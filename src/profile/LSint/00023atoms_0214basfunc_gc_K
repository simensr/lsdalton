#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00023atoms_0214basfunc_gc_K.info <<'%EOF%'
   00023atoms_0214basfunc_gc_K
   -------------
   Molecule:         alanine2/cc-pVDZ
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         ~1 min
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00023atoms_0214basfunc_gc_K.mol <<'%EOF%'
BASIS
cc-pVDZ
alanine2
-------------------
Atomtypes=4 Nosymmetry Angstrom
Charge=1.0 Atoms=12
H     0.05280000   -1.53439999    0.88129997
H    -1.23450005    0.27579999   -0.89679998
H    -0.19740000    2.36640000   -0.29170001
H    -1.35080004    1.76890004    0.92479998
H     0.39960000    1.62769997    1.21329999
H    -0.67930001   -0.36179999    1.70280004
H     1.93879998   -0.20299999    0.86240000
H     3.77309990    0.31909999   -1.13689995
H     5.30579996   -1.03629994    0.13810000
H     4.34819984    0.08210000    1.13769996
H     3.94610000   -1.65120006    1.10769999
H     4.32289982   -2.89739990   -2.76340008
Charge=6.0 Atoms=6
C    -0.38240001    0.22050001   -0.21940000
C     0.93320000   -0.08650000   -0.95240003
C    -0.38280001    1.59440005    0.45510000
C     3.35980010   -0.57099998   -0.66259998
C     3.28279996   -1.76810002   -1.62360001
C     4.30800009   -0.81140000    0.51470000
Charge=7.0 Atoms=2
N    -0.63120002   -0.79659998    0.79250002
N     2.03369999   -0.27239999   -0.14070000
Charge=8.0 Atoms=3
O     0.97780001   -0.15680000   -2.18120003
O     2.21239996   -2.33019996   -1.85829997
O     4.46810007   -2.14759994   -2.18169999
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00023atoms_0214basfunc_gc_K.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00023atoms_0214basfunc_gc_K.check
cat >> 00023atoms_0214basfunc_gc_K.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-91\.246391" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
