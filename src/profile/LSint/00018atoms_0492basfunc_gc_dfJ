#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00018atoms_0492basfunc_gc_dfJ.info <<'%EOF%'
   00018atoms_0492basfunc_gc_dfJ
   -------------
   Molecule:         naphthalene/cc-pVTZ
   Wave Function:    HF
   Profile:          Density-fitted Coulomb Matrix
   CPU Time:         6 min
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00018atoms_0492basfunc_gc_dfJ.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
naphthalene optimized with b3lyp/6-31G(d,p)
-------------------
Atomtypes=2 Nosymmetry Angstrom
Charge=6.0 Atoms=10
C         0.000000      2.433095      0.708350
C         0.000000      1.244661      1.402482
C         0.000000      0.000000      0.716836
C         0.000000      0.000000     -0.716836
C         0.000000      1.244661     -1.402482
C         0.000000      2.433095     -0.708350
C         0.000000     -1.244661      1.402482
C         0.000000     -1.244661     -1.402482
C         0.000000     -2.433095     -0.708350
C         0.000000     -2.433095      0.708350
Charge=1.0 Atoms=8
H         0.000000     -1.242090      2.489510
H         0.000000      3.377295      1.245045
H         0.000000      1.242090      2.489510
H         0.000000      1.242090     -2.489510
H         0.000000      3.377295     -1.245045
H         0.000000     -1.242090     -2.489510
H         0.000000     -3.377295     -1.245045
H         0.000000     -3.377295      1.245045
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00018atoms_0492basfunc_gc_dfJ.dal <<'%EOF%'
**PROFILE
.COULOMB
**INTEGRALS
.DENSFIT
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DIAG
.GCBASIS
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00018atoms_0492basfunc_gc_dfJ.check
cat >> 00018atoms_0492basfunc_gc_dfJ.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Coulomb energy, mat_dotproduct\(D,J\)\= * 882\.0730627[1-2]" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
