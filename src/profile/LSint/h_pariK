#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > h_pariK.info <<'%EOF%'
   h_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > h_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
h
EXCITATION DIAGNOSTIC
Atomtypes=1 Nosymmetry 
Charge=1.0 Atoms=1
H       0.0000000000            0.0000000000            0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > h_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >h_pariK.check
cat >> h_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
