#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00012atoms_0341basfunc_gc_K.info <<'%EOF%'
   00012atoms_0341basfunc_gc_K
   -------------
   Molecule:         monofluorobenzene/cc-pVTZ
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         1 min 40 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00012atoms_0341basfunc_gc_K.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=Ahlrichs-Coulomb-Fit
CME monofluorobenzene
B3LYP/cc-pVTZ optimized -331.423475
Atomtypes=3 Nosymmetry
Charge=6.0 Atoms=6
C         0.0000000000           -3.6019849275            0.0000000000
C         2.2746863692           -2.2844271478            0.0000000000
C        -2.2746863692           -2.2844271478            0.0000000000
C         2.2916299551            0.3437123383            0.0000000000
C        -2.2916299551            0.3437123383            0.0000000000
C         0.0000000000            1.6046504042            0.0000000000
Charge=9.0 Atoms=1
F         0.0000000000            4.1568833610            0.0000000000
Charge=1.0 Atoms=5
H         0.0000000000           -5.6456667396            0.0000000000
H         4.0333157589            1.4110328255            0.0000000000
H        -4.0333157589            1.4110328255            0.0000000000
H         4.0486115823           -3.3009875717            0.0000000000
H        -4.0486115823           -3.3009875717            0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00012atoms_0341basfunc_gc_K.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**WAVE FUNCTIONS
.HF
'DENSOPT
.START
H1DIAG
.GCBASIS
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00012atoms_0341basfunc_gc_K.check
cat >> 00012atoms_0341basfunc_gc_K.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-43\.1190662" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
