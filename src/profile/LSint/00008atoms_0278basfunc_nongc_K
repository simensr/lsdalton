#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00008atoms_0278basfunc_nongc_K.info <<'%EOF%'
   00008atoms_0278basfunc_nongc_K
   -------------
   Molecule:         2 NH3 molecule/Turbomole-TZVPPP
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         ~1 min
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00008atoms_0278basfunc_nongc_K.mol <<'%EOF%'
BASIS
Turbomole-TZVPPP Aux=Turbomole-TZVPPP
2 NH3 molecule
ccsd/cc-pVQZ optimized
AtomTypes=2 Nosymmetry Angstrom
Charge=7 Atoms=2
N        0.000000    0.000000    0.115054
N        8.000000    0.000000    0.115054
Charge=1 Atoms=6
H        0.000000    0.933817   -0.268460
H        0.808710   -0.466909   -0.268460
H       -0.808710   -0.466909   -0.268460
H        8.000000    0.933817   -0.268460
H        8.808710   -0.466909   -0.268460
H        7.191290   -0.466909   -0.268460
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00008atoms_0278basfunc_nongc_K.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DAIG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00008atoms_0278basfunc_nongc_K.check
cat >> 00008atoms_0278basfunc_nongc_K.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-23\.5450302" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
