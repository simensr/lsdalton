g2-2 cation test set 

b3lyp/6-31G* geometries

no. of atomic species:3
He+, Ne+, Ar+

no. of molecules:47
BF3+, BCl3+, B2F4+, CO2+, CF2+, COS+, CS2+, CH2+, CH3+,
 C2H5+, C3H4+ (cyclopropene+), CH2=C=CH2+, sec-C3H7+,
C6H6+, C6H5CH3+, CN+,
 CHO+, CH2OH+, CH3O+, CH3OH+, CH3F+, CH2S+, HSCH2+, CH3SH+, CH3Cl+, 
C2H5OH+, CH3CHO+, CH3OF+, C2H4S+ (thiirane+), NCCN+, C4H4O+ (furan+), 
C4H5N+ (pyrrole+), C6H5OH+, C6H5NH2+,
B2H4+, NH+, NH2+, N2H2+, N2H3+, HOF+, SiH2+, SiH3+,
 Si2H2+, Si2H4+, Si2H5+, Si2H6+, H3+

notes:
BF3+: alter B 15,16
C6H6+: alter B 20,21
NCCN+: alter B 11, 13
C2H5OH+: new geometry 9-2-98



% chk=cation
%mem=4000000
#p B3LYP/6-31G* 

cation: he

1 2
he

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* 

cation: ne

1 2
ne

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* 

cation: ar

1 2
ar

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct guess=alter

cation: BF3+ C2v 2-B2 B3LYP/6-31G* optimized geometry
B3LYP/6-31G*=-324.0104863a.u.


1,2 
B
F,1,rf1
F,1,rf2,2,af2
F,1,rf2,2,af2,3,180.,0
Variables:
rf1=1.25836406
rf2=1.36395972
af2=131.74623765

 
15 16

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: BCl3+ C2v C2v B3LYP/6-31G* optimized geometry

1,2
B,0.,0.,-0.0004255336
Cl,0.,0.,1.7346903469
Cl,0.,-1.5034198103,-0.867282595
Cl,0.,1.5034198103,-0.867282595

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: B2F4+ D2d B3LYP/6-31G* optimized geometry

1 2
b1
b2 b1 rb2
f1 b2 rf1 b1 af1
f2 b2 rf1 b1 af1 f1 180.0
f3 b1 rf1 b2 af1 f1 -90.0
f4 b1 rf1 b2 af1 f3 180.0

rb2=2.02803703
rf1=1.27504891
af1=110.91450948

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CO2+ DH B3LYP optimized geometry

1,2
C,0.,0.,0.
O,0.,0.,1.1810539009
O,0.,0.,-1.1810539009

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CF2+ B3LYP optimized geometry

1,2
C,0.,0.,0.4296707219
F,0.,1.0872306078,-0.143223574
F,0.,-1.0872306078,-0.143223574

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: ocs+ B3LYP/6-31G* optimized geometry 

1,2
O,0.,0.,-1.7126395897
C,0.,0.,-0.5722747987
S,0.,0.,1.0709228443

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CS2+ Dh B3LYP/6-31G* optimized geometry

1,2
S,0.,0.,1.5631282817
C,0.,0.,0.
S,0.,0.,-1.5631282817

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH2+ B3LYP/6-31G* optimized geometry

1,2
C,0.,0.,0.0938739388
H,0.,1.0328873969,-0.2816218163
H,0.,-1.0328873969,-0.2816218163

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3+ D3h B3LYP/6-31G* optimized geometry

1,1
C,0.,0.,0.
H,0.,1.0948833489,0.
H,0.9481967943,-0.5474416745,0.
H,-0.9481967943,-0.5474416744,0.


--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: C2H5+ C2v (nonclassical bridged structure) B3LYP/6-31G* optimized geometry

1,1
C,0.,0.6914314984,-0.0634306503
C,0.,-0.6914314984,-0.0634306503
H,0.,0.,1.0609231958
H,0.9365962956,1.2494981808,-0.0749388481
H,-0.9365962956,1.2494981808,-0.0749388481
H,-0.9365962956,-1.2494981808,-0.0749388481
H,0.9365962956,-1.2494981808,-0.0749388481

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: C3H4+ (cyclopropenyl cation) C2v B3LYP/6-31G* optimized geometry
B3LYP/6-31G*=-116.276475028a.u.

1,2
 C 0.0291359312 0.0082986074 0.8255483301
 C -0.0577403672 -0.671705625 -0.4836722465
 C 0.0232723121 0.6619527445 -0.4992975166
 H 0.9321565871 0.2201459659 1.4299634476
 H -0.8292860988 -0.1915518758 1.4959781722
 H -0.317455691 -1.6091358622 -0.964552418
 H 0.2465779459 1.5892674104 -1.016860604

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: C3H4+ (twisted structure) B3LYP/6-31G* optimized geometry 

1,2
C
X,1,1.
C,1,rc2,2,ac2
C,1,rc2,2,ac2,3,180.,0
H,3,rh1,1,ah1,2,th1,0
H,3,rh2,1,ah2,5,th2,0
H,4,rh1,1,ah1,2,th1,0
H,4,rh2,1,ah2,7,th2,0

rc2=1.3195613
ac2=89.9996989
rh1=1.09453614
ah1=238.8578197
th1=-24.89525328
rh2=1.09454782
ah2=121.14353437
th2=180.00079005

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: sec-C3H7+ B3LYP/6-31G*g optimized geometry 

1 1
C,0.,0.,0.4554892687
H,0.,0.,1.5499126404
C,-0.0131862067,-1.2859202407,-0.1981785184
C,0.0131862067,1.2859202407,-0.1981785184
H,-1.0432744302,-1.6701485898,0.0026983558
H,1.0432744302,1.6701485898,0.0026983558
H,0.1458859795,-1.2624326197,-1.2774112169
H,-0.1458859795,1.2624326197,-1.2774112169
H,0.6179865571,-2.0216501285,0.3223598454
H,-0.6179865571,2.0216501285,0.3223598454

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* guess=alter opt scf=direct

cation: C6H6+ 2B2g (compressed structure) B3LYP/6-31G* optimized geometry
B3LYP/6=31G*=-231.9231298a.u.

1 2 
X
 X          rx2
 X          1.        2     90. 
 X          1.        1     90.       3     0.        
 C          rc1       3     90.       2     180.      
 C          rc1       4     90.       1     180.      
 C          rc3       3     90.       2     -90.      
 C          rc3       3     90.       2     90.       
 C          rc3       4     90.       1     90.       
 C          rc3       4     90.       1     -90.      
 X          1.        1     90.       3     0.        
 X          1.        2     90.       4     0.        
 H          rh1       11    90.       1     180.      
 H          rh1       12    90.       2     180.      
 H          rh3       5     ah3       8     180.      
 H          rh3       5     ah3       7     180.      
 H          rh3       6     ah3       10    180.      
 H         rh3       6     ah3       9     180.      

  rx2                   1.37239                  
  rc1                   0.6986                   
  rc3                   1.24999                  
  rh1                   1.08707                  
  rh3                   1.08511                  
  ah3                 119.4218


20 21

--link1--
%chk=cation
#p B3LYP/6-31G* opt scf=direct

cation: C6H5-CH3(+). Toluene cation (C1 symm). 
B3LYP/6-31G* optimized geometry 
B3LYP/6-31G*=-271.2577367a.u.

1,2
C  -0.029710452 -1.8850474086 -0.0028373723
 C  0.0087571963 0.9169904165 0.0129517024
 C  1.2417205937 0.1659194885 0.0229023876
 C  -1.2423572041 0.2081056528 -0.0077290586
 C  -1.2609874123 -1.1633596067 -0.0144922899
 C  1.2208209813 -1.2053024444 0.0164053164
 H  2.1820314255 0.7089436355 0.0377650696
 H  -2.1662835475 0.778120109 -0.0149028602
 H  -2.1993633946 -1.7082480472 -0.0275277728
 H  2.1427334237 -1.7774607784 0.0264239449
 H  -0.0486570044 -2.9714285272 -0.0100223849
 C  0.0425743323 2.3958720696 -0.0051233336
 H  -0.9027484966 2.8456217883 0.3076342026
 H  0.8721466296 2.7940158577 0.5906607455
 H  0.2352327536 2.7313669561 -1.0424950567
 
--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CN+ B3LYP/6-31G* optimized geometry

1,1
C,0,0.,0.,-0.6366526492
N,0,0.,0.,0.5457022708

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: HCO+ B3LYP/6-31G* optimized geometry

1,1
C,0.,0.,-0.5203814497
O,0.,0.,0.5927435273
H,0.,0.,-1.6196595202

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH2OH+ B3LYP/6-31G* optimized geometry

1,1
C,0.0433768701,0.6271449849,0.
O,0.0544316111,-0.6249818175,0.
H,-0.8897002104,1.1987554236,0.
H,1.0240401689,1.1063243092,0.
H,-0.8300540676,-1.0680951021,0.

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3O+ C3v 3A1 B3LYP/6-31G* optimized geometry

1,3
C,0.,0.,-0.5483551737
O,0.,0.,0.7584825964
H,0.,-1.0638244575,-0.9259099096
H,-0.9212990054,0.5319122288,-0.9259099096
H,0.9212990054,0.5319122287,-0.9259099096

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3OH+ B3LYP/6-31G* optimized geometry

1,2
C,-0.6438076986,0.0590653003,-0.0188828377
O,0.7117716969,-0.1243770203,0.0368827892
H,-1.0424649222,-0.7721214117,-0.6663177135
H,1.2436056292,0.6858192328,-0.1808038423
H,-1.0490348087,-0.3303533738,0.9575172184
H,-0.9834332819,1.0572799129,-0.2921609504

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3F+ Cs B3LYP/6-31G* optimized geometry
B3LYP/6-31G*=-139.283103758 a.u.

1,2
C -0.0085066575 0.5827757196 -0.0302453382
 F 0.0489444151 -0.7180095016 -0.0251635336
 H -1.0933168779 0.9149336927 0.2291782378
 H 0.2128678122 0.9753553055 1.0436126155
 H 0.490989275 1.0751421989 -0.8648470221

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH2S+ B3LYP/6-31G* optimized geometry

1,2
C,0.,0.,-1.0213121988
S,0.,0.,0.5819522396
H,0.,-0.9389311572,-1.5916813208
H,0.,0.9389311572,-1.5916813208

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: HSCH2+ Cs 1-A' B3LYP/6-31G* optimized geometry

1,1
C,0.0449199106,1.0651823544,0.
S,0.0543292021,-0.5533391828,0.
H,-1.2762987794,-0.7642799111,0.
H,-0.8692119901,1.6536326075,0.
H,1.006724072,1.5729801024,0.

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3SH+ B3LYP/6-31G* optimized geometry

1,2
C,-0.0481440233,1.1420481788,0.
S,-0.0559365334,-0.6549944371,0.
H,-1.0843021431,1.4847858822,0.
H,1.2887828691,-0.861022511,0.
H,0.489683974,1.5019292749,0.8908688222
H,0.489683974,1.5019292749,-0.8908688222

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3Cl+ B3LYP/6-31G* optimized geometry

1 2
c1
cl1 c1 rcl1
h1 c1 rh1 cl1 ah1
h2 c1 rh2 cl1 ah2 h1 th2
h3 c1 rh2 cl1 ah2 h1 -th2

rcl1=1.76013081
rh1=1.12244817
ah1=103.91387827
rh2=1.09502774
ah2=109.48390323
th2=114.80226837

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct
B3LYP/6-31G*=-154.6694482a.u.

cation: CH3CH2OH+ C1 B3LYP/6-31G* optimized geometry

1,2
C  -1.2653496665 -0.3508752825 0.1259760964
 C  0.1558017539 0.6149599926 -0.0262653224
 O  1.1711776199 -0.2497196845 -0.2016083692
 H  -2.0248835549 0.433402434 0.18164744
 H  1.6365753118 -0.48671618 0.634259597
 H  -1.3193170116 -0.9612477113 -0.7738043363
 H  -1.1468161292 -0.9209895783 1.0468666967
 H  -0.047133719 1.1391546755 -0.96477833
 H  0.1894416198 1.2096455756 0.8904112419
 
--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: ch3cho+ (acetaldehyde cation)  optimized geometry

1,2
O,1.214946466,0.3325862723,0.
C,0.0139932795,0.4599991684,0.
H,-0.3680469034,1.5089696416,0.
C,-0.9660533108,-0.6933039823,0.
H,-0.44601316,-1.651571028,0.
H,-1.5965757387,-0.5591299542,0.8917496121
H,-1.5965757387,-0.5591299542,-0.8917496121

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: CH3OF+ B3LYP/6-31G* optimized geometry

1,2
C,0.2912734113,-1.0625625679,0.30910917
O,0.3056907365,0.3542944346,0.3244093219
F,-0.5591892919,0.8526868596,-0.5934305405
H,1.0184775119,-1.3330219661,1.0808426934
H,-0.743276908,-1.4000699197,0.5211330845
H,0.5643366632,-1.4000699197,-0.7110305085

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: c2h4s+ B3LYP/6-31G* optimized geometry 
(Thiooxirane+. cyclic CH2-S-CH2+ ring). C2v symm.

1,2
C,0.,-0.7350507161,-0.825826312
S,0.,0.,0.8779138682
C,0.,0.7350507161,-0.825826312
H,-0.9207910584,-1.2811836135,-1.0341765367
H,0.9207910584,-1.2811836135,-1.0341765367
H,0.9207910584,1.2811836135,-1.0341765367
H,-0.9207910584,1.2811836135,-1.0341765367

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct guess=alter

cation: NCCN+ Dh B3LYP/6-31G* optimized geometry
B3LYP/6-31G*=-185.1814338a.u.

1 2
c1
c2 c1 rc2
x1 c1 1.0 c2 90.0
x2 c2 1.0 c1 90.0 x1 0.0
n1 c1 rn1 x1 90.0 c2 180.0
n2 c2 rn1 x2 90.0 c1 180.0

rc2                   1.34905                  
 rn1                   1.19696                  


11 13


--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: C4H4O+ (furan cation) B3LYP/6-31G* optimized geometry

1 2
o1
c1 o1 rc1
c2 o1 rc1 c1 ac2
c3 c1 rc3 o1 ac3 c2 0.0
c4 c2 rc3 o1 ac3 c1 0.0
h1 c1 rh1 o1 ah1 c2 180.0
h2 c2 rh1 o1 ah1 c1 180.0
h3 c3 rh3 c1 ah3 o1 180.0
h4 c4 rh3 c2 ah3 o1 180.0

rc1=1.35026228
ac2=106.6293859
rc3=1.41650178
ac3=110.66602431
rh1=1.08350499
ah1=116.39294549
rh3=1.08243358
ah3=125.44577137

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: C4H4NH+ (Pyrrole, planar cyclic cation) B3LYP/6-31G* optimized geometry. 
Doublet A2. C2v symm. 

1 2
H,0.,0.,-2.1597902166
N,0.,0.,-1.1446467545
C,1.1098794601,0.,-0.352908336
C,-1.1098794601,0.,-0.352908336
C,0.6874582983,0.,1.0178404664
C,-0.6874582983,0.,1.0178404664
H,2.1083064212,0.,-0.7729946912
H,-2.1083064212,0.,-0.7729946912
H,1.3540606682,0.,1.8695606583
H,-1.3540606682,0.,1.8695606583

--link1--
%chk=cation
#p B3LYP/6-31G* opt scf=direct

cation: C6H5-OH+ (Phenol cation. Planar Cs) B3LYP/6-31G*
Doublet A

1,2
C,-0.0198839397,0.9411657754,0.
C,1.2453092929,0.2694103488,0.
C,1.2578496525,-1.1021702233,0.
C,0.0317478463,-1.8293586948,0.
C,-1.218719997,-1.161863737,0.
C,-1.2587485597,0.2106769294,0.
O,-0.1320186356,2.249012329,0.
H,0.7292990448,2.7139135525,0.
H,2.1660650263,0.8468101446,0.
H,2.1993996649,-1.6414991403,0.
H,0.0604715344,-2.9149226003,0.
H,-2.1382711406,-1.7374574539,0.
H,-2.1861408164,0.7738944748,0.

--link1--
%chk=cation
#p B3LYP/6-31G* opt scf=direct

cation: C6H5-NH2+ (Aniline cation) B3LYP/6-31G* optimized geometry 
Cs -> Planar C2v. 

1,2
C,0.,0.,0.936163194
C,0.,1.2455485606,0.219764687
C,0.,-1.2455485606,0.219764687
C,0.,1.2314168176,-1.1538560838
C,0.,-1.2314168176,-1.1538560838
C,0.,0.,-1.8529792587
N,0.,0.,2.2720030564
H,0.,-0.864037862,2.8059867627
H,0.,0.864037862,2.8059867627
H,0.,2.1640962552,-1.7081629973
H,0.,-2.1640962552,-1.7081629973
H,0.,0.,-2.9383624873
H,0.,2.1796214513,0.7743433555
H,0.,-2.1796214513,0.7743433555

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: B2H4+ B3LYP/6-31G* optimized geometry
(nonplanar doubly bridged structure C2v symm)

1,2
B,0.,0.7740436926,-0.0490215833
B,0.,-0.7740436926,-0.0490215833
H,-1.0157161755,0.,0.3090277989
H,1.0157161755,0.,0.3090277989
H,0.,1.9465399137,-0.0639198823
H,0.,-1.9465399137,-0.0639198823

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: NH+ B3LYP/6-31G* optimized geometry

1,2
N,0.,0.,0.1346790598
H,0.,0.,-0.9427534184

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: NH2+, 3-B2 C2v, B3LYP/6-31G* optimized geometry

1,3
N,-0.0514269708,0.,-0.0132168172
H,-0.0715637767,0.,1.0250780929
H,0.4315525725,0.,-0.9325603723

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: N2H2+ trans B3LYP/6-31G* optimized geometry

1,2
N,0.0025121362,0.,-0.5839803045
N,-0.0025121362,0.,0.5839803045
H,0.8694982293,0.,-1.1695107279
H,-0.8694982293,0.,1.1695107279

--Link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: N2H3+ B3LYP/6-31G* optimized geometry

1,1
N,0.0475487939,-0.5375395319,0.
N,0.0592468747,0.6912898883,0.
H,-0.8028985108,-1.129336869,0.
H,0.9608618344,-1.0180982027,0.
H,-0.9055330041,1.0711825764,0.

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: HOF+ B3LYP/6-31G* optimized geometry

1,2
O,0.0649026608,0.643183823,0.
F,0.0451404,-0.671662377,0.
H,-0.9254848867,0.8994908088,0.

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: SIH2+ B3LYP/6-31G* optimized geometry

1,2
Si,0,0.,0.,0.0943911136
H,0,0.,1.288741062,-0.6607377949
H,0,0.,-1.288741062,-0.6607377949

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: SiH3+ c3v B3LYP/6-31G* optimized geometry

1 1
Si,0.,0.,-0.0000090328
H,1.4733991215,0.,0.0000421529
H,-0.7366995608,-1.2760010692,0.0000421529
H,-0.7366995608,1.2760010692,0.0000421529

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: Si2H2+ B3LYP/6-31G* optimized geometry 

1,2
Si,0.,-1.1284348054,-0.0528746687
Si,0.,1.1284348054,-0.0528746687
H,0.9918451812,0.,0.7402453619
H,-0.9918451812,0.,0.7402453619

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: Si2H4+ D2h 2B3u B3LYP/6-31G* optimized geometry

1,2
Si,0.,0.,1.1258985746
Si,0.,0.,-1.1258985746
H,0.,1.2772136369,1.8614026872
H,0.,-1.2772136369,1.8614026872
H,0.,1.2772136369,-1.8614026872
H,0.,-1.2772136369,-1.8614026872

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: Si2H5+ B3LYP/6-31G* optimized geometry
B3LYP/6-31G*=-581.6556302a.u.

1,1
Si
Si,1,rsi2
H,1,rh1,2,ah1
H,1,rh2,2,ah2,3,th2,0
H,1,rh3,2,ah3,3,th3,0
H,2,rh4,1,ah4,3,th4,0
H,2,rh5,1,ah5,6,th5,0

rsi2=2.38110707
rh1=1.48319093
ah1=100.39518414
rh2=1.47619736
ah2=105.03218262
th2=118.58790064
rh3=1.4761624
ah3=105.05590408
th3=-118.60955485
rh4=1.48141743
ah4=124.15962253
rh5=1.48141759
ah5=124.13231503
th5=-177.19533973
th4=88.77162451 

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: Si2H6+ D3d, 2Ag B3LYP/6-31G* optimized geometry

1,2
Si,0.,0.,1.3489341069
Si,0.,0.,-1.3489341069
H,0.,1.4604697293,1.5606099926
H,0.,-1.4604697293,-1.5606099926
H,-1.264803887,-0.7302348646,1.5606099926
H,1.264803887,-0.7302348647,1.5606099926
H,-1.264803887,0.7302348647,-1.5606099926
H,1.264803887,0.7302348646,-1.5606099926

--link1--
%chk=cation
%mem=4000000
#p B3LYP/6-31G* opt scf=direct

cation: H3+ d3h B3LYP/6-31G* optimized geometry

1 1
H,-0.2497997665,0.,-0.4326658872
H,-0.2497997665,0.,0.4326658872
H,0.4995995329,0.,0.




