anion: ------------------------------------------------

G2-2 anion set B3LYP/6-31G* geometries

no. of atomic species:4
Li-, B- (3), Na-, Al- (3)

no. of molecules:29
CC- (2), C2O- (2) CCO, CF2- (2) 2B1, NCO- (1), NO2- (1) ONO, O3-, 
OF- (2), SO2-, S2O-, C2H-, C2H3-, H2C=C=C-, H2C=C=CH-, CH2CHCH2-, 
HCO-, HCF- (2) 2A", CH3O-, CH3S-, CH2S-, CH2CN-, CH2NC-, CHCO-, 
CH2CHO-, CH3CO-, CH3CH2O-, CH3CH2S-, LiH-, HNO-, HO2- (1) HOO- 1A'


%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: Li-

-1 1
Li

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: B-

-1 3
B

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: Na-

-1 1
Na

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: Al-

-1 3
Al

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CC- B3LYP/6-31G* optimized geometry

-1 2
C,0,0.,0.,-0.633408887
C,0,0.,0.,0.633408887

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CCO- B3LYP/6-31G*  optimized geometry

-1 2
C,0,0.100367889,0.,0.
C,0,1.4107625971,0.,0.
O,0,-1.1333478646,0.,0.

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CF2- 2B1 B3LYP/6-31G* optimized geometry

-1 2
C,0,-0.5445434356,0.,-0.456926196
F,0,-0.5391538518,0.,1.0111678037
F,0,0.9021828089,0.,-0.7065503397

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: NCO- B3LYP/6-31G*  optimized geometry

-1 1
C,0,0.071714051,0.,0.
N,0,1.2679149655,0.,0.
O,0,-1.163211133,0.,0.

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: NO2- B3LYP/6-31G* optimized geometry

-1 1
N,0,-0.4328307435,0.,-0.1848979821
O,0,-0.2335581974,0.,1.0709171446
O,0,0.6122850979,0.,-0.9091314103

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: O3- C2v B3LYP/6-31G* optimized geometry

-1,2
O
O,1,ro2
O,1,ro2,2,ao3

ro2=1.35997555
ao3=115.64875315

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: OF- B3LYP/6-31G*  optimized geometry

-1 1
O,0,0.,0.,-0.8047272056
F,0,0.,0.,0.7153130716

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: SO2- B3LYP/6-31G*  optimized geometry

-1,2
S,0,0.1827254423,0.3428711544,0.1150837789
O,0,-1.1239519131,0.3577064419,-0.7078851848
O,0,0.7585010285,-1.0434487506,0.4777176269

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: S2O- Doublet A" Cs B3LYP/6-31G*  optimized geometry
 
-1,2
S
S,1,SS
O,1,OS,2,OSS

SS=2.04774502
OS=1.54805543
OSS=116.06027032

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt scf=direct

anion: C2H- B3LYP/6-31G*  optimized geometry

-1,1
C,0,0.,0.,-0.4940629679
C,0,0.,0.,0.7557093151
H,0,0.,0.,-1.5698780837

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: c2h3- (H2CCH-) B3LYP/6-31G* optimized geometry 

-1,1
C,0,0.0665641089,0.,-0.5689890884
C,0,0.0673200643,0.,0.7881182579
H,0,1.0181382615,0.,-1.1428028002
H,0,-0.7985659801,0.,-1.279940155
H,0,-1.0228773209,0.,1.1079679385

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: C3H2- (H2CCC-) B3LYP/6-31G* optimized geometry

-1,2
C,0,0.,0.,0.2248015089
C,0,0.,0.,-1.1475587497
C,0,0.,0.,1.5024763129
H,0,0.,0.9218481806,-1.7391572164
H,0,0.,-0.9218481806,-1.7391572164

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: C3H3- (H2CCCH-) B3LYP/6-31G* optimized geometry

-1,1
C,0,-0.114133844,0.,-0.0082459408
C,0,1.2368236226,0.,0.0294619066
C,0,-1.3948790061,0.,0.1022042353
H,0,1.8214524211,-0.9240149639,0.0360476321
H,0,1.8214524211,0.9240149639,0.0360476321
H,0,-2.0097694768,0.,-0.8126164707

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: C3H5- (CH2CHCH2-, PROP-2-ENYL) C2v B3LYP/6-31G* optimized geometry
B3LYP/6-31G*= -117.2418936a.u.

-1,1
C  0. 0. 0.37420029
 H  0. 0. 1.4787052971
 C  -0.0918789344 1.2783766214 -0.1760914825
 C  0.0918789344 -1.2783766214 -0.1760914825
 H  -0.336326052 2.137526527 0.4498039025
 H  0.336326052 -2.137526527 0.4498039025
 H  -0.1774294012 1.4289138062 -1.2552085262
 H  0.1774294012 -1.4289138062 -1.2552085262
 
--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: HCO- B3LYP/6-31G*  optimized geometry

-1,1
C,0,0.0906147194,0.6309997556,0.
O,0,0.0783048034,-0.6062312399,0.
H,0,-1.1701267437,1.0638513855,0.

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: HCF- 2A" B3LYP/6-31G* optimized geometry

-1 2
C,0,-0.0807808114,0.,-0.8323710477
F,0,-0.0651167181,0.,0.6665885057
H,0,1.0707353319,0.,-1.0050702655

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: ch3o- B3LYP/6-13G* optimized geometry

-1,1
C,0,-0.0001954955,0.5180518563,0.
O,0,0.0002998773,-0.7912787438,0.
H,0,-1.0302736909,1.0582782666,0.
H,0,0.5145303931,1.058870502,0.891885966
H,0,0.5145303931,1.058870502,-0.891885966

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH3S- B3LYP/6-31G* optimized geometry
B3LYP/6-31G*=-438.1096266a.u.

-1,1
C  -0.0002143196 -1.1278271258 0.
 S  0.0001353171 0.7142509901 0.
 H  1.0187258839 -1.5538958783 0.
 H  -0.5098025201 -1.5535786041 -0.8824993612
 H  -0.5098025201 -1.5535786041 0.8824993612

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH2S- B3LYP/6-31G* optimized geometry

-1,2
C,0,-1.1079530049,0.,0.0010286849
S,0,0.6254253611,0.,-0.0166509692
H,0,-1.6795438738,-0.9243977137,0.130121699
H,0,-1.6795438738,0.9243977137,0.130121699

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH2CN- B3LYP/6-31G*  optimized geometry

-1,1
C,0,0.1007643946,-1.2080467728,0.
C,0,0.0016766582,0.180040395,0.
N,0,-0.0196385467,1.3691579579,0.
H,0,-0.2385882451,-1.7080337189,0.9118739793
H,0,-0.2385882451,-1.7080337189,-0.9118739793

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH2NC- B3LYP/6-31G* optimized geometry

-1,1
C,0,0.1145794641,-1.1661311357,0.
N,0,-0.0004227714,0.2296639331,0.
C,0,0.0199048963,1.4191760028,0.
H,0,-0.4019733813,-1.5629583671,0.8920011181
H,0,-0.4019733813,-1.5629583671,-0.8920011181

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CHCO- B3LYP/6-31G* optimized geometry
B3LYP/6-31G*= -151.9770292a.u.

-1,1
 C  -0.0074222206 -0.0279256546 -0.0092246557
 C  0.0236667093 0.0880474434 -1.268034845
 O  0.0076173402 0.0245753444 1.2216193675
 H  -0.1584056535 -0.5573334887 -2.1093979353

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH2CHO- B3LYP/6-31G* optimized geometry

-1,1
C,0,0.1766109221,-1.1609247184,0.2265400003
C,0,0.175109914,0.2215509957,0.2246146474
O,0,-0.4121520737,1.0551437162,-0.5286701967
H,0,0.8263902307,0.6425214279,1.0600162263
H,0,-0.3946142976,-1.7342122052,-0.5061743752
H,0,0.75511564,-1.7132166159,0.9685918363

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH3CO- Cs (H cis to Oxygen) B3LYP/6-31G* optimized geometry

-1,1
C,0,-0.324182029,1.1075054622,-0.2388383985
C,0,-0.3269846049,-0.5576438654,-0.2409031729
O,0,0.6194939065,-0.9783674335,0.4564069544
H,0,0.4925512812,1.5604597545,0.362883037
H,0,-0.2480988107,1.4836550664,-1.2772555312
H,0,-1.2934039192,1.4836550664,0.1415662876

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: ch3ch2o- B3LYP/6-31G* optimized geometry

-1,1
C,0,0.2673168764,-1.1490313586,0.214765906
C,0,0.2538332276,0.4230019429,0.2039329647
O,0,-0.683132078,0.9500270661,-0.5488373261
H,0,0.2347740548,0.6916240074,1.3314978203
H,0,1.350809132,0.6916240074,-0.0576193646
H,0,1.0607976196,-1.5876435861,0.8522588059
H,0,-0.7083244215,-1.5198222317,0.5660059624
H,0,0.4000996147,-1.5198222317,-0.8136378394

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: CH3CH2S- B3LYP/6-31G*  optimized geometry

-1 1
C,0,0.0741586786,-1.4583570599,0.7844008762
C,0,0.0736206084,0.0780898194,0.7787095294
S,0,-0.0851004221,0.8129616474,-0.9001353168
H,0,0.1704946853,-1.8817139933,1.8033786893
H,0,-0.8539883341,-1.8387776863,0.3393359227
H,0,0.9024518659,-1.8387776863,0.1732788871
H,0,-0.7465522944,0.4167432251,1.43643504
H,0,1.0025251087,0.4167432251,1.2710740966

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: LIH- 2Sigma B3LYP/6-31G*  optimized geometry

-1 2
Li,0,0.,0.,-0.4350620178
H,0,0.,0.,1.3051860533

--link1--
%chk=anion
anion: ------------------------------------------------

G2-2 anion set 

no. of atomic species:4
Li-, B- (3), Na-, Al- (3)

no. of molecules:29
CC- (2), C2O- (2) CCO, CF2- (2) 2B1, NCO- (1), NO2- (1) ONO, O3-, 
OF- (2), SO2-, S2O-, C2H-, C2H3-, H2C=C=C-, H2C=C=CH-, CH2CHCH2-, 
HCO-, HCF- (2) 2A", CH3O-, CH3S-, CH2S-, CH2CN-, CH2NC-, CHCO-, 
CH2CHO-, CH3CO-, CH3CH2O-, CH3CH2S-, LiH-, HNO-, HO2- (1) HOO- 1A'



%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: HNO- B3LYP/6-31G*  optimized geometry

-1,2
N,0,0.0660867543,0.6457075772,0.
O,0,0.0643294381,-0.6844024946,0.
H,0,-0.9772427853,0.9552669167,0.

--link1--
%chk=anion
%mem=4000000
# B3LYP/6-31G* Opt  scf=direct

anion: HOO- 1A' B3LYP/6-31G*  optimized geometry

-1 1
O,0,-0.0599002204,0.,-0.7098012097
O,0,-0.0527030856,0.,0.8159863062
H,0,0.9008264479,0.,-0.8494807726







