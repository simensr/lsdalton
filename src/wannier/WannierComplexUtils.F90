!> @file 
!> Contains wrappers for complex Lapack functions 
!> Should ideally be replaced by extending the matrix type to complex
!arrays.

module wannier_complex_utils
  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use matrix_module, only: &
       matrix
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage
  !use memory_handling, only: mem_dealloc
  use wannier_utils, only: &
       wu_indxtocoor, &
       wu_indxtolatvec, &
       wtranslate_molecule, &
       wcopy_atompositions
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_dotproduct, &
       mat_abs_max_elm, &
       mat_trans, &
       mat_dsyev, &
       mat_add, &
       mat_print, &
       mat_copy
  use memory_handling, only: mem_alloc, mem_dealloc
  use TYPEDEFTYPE
  use configurationType
  use av_utilities
  !use wannier_startguess, only: bloch_arrays
  
  implicit none
  private
  complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)
  public :: wannier_zheev, &
       wannier_dggev, &
       wannier_invert, &
       wannier_invert_svd,&
       bloch_transform, &
       bloch_transform_mos, &
       wannier_transform, &
       bloch_arrays, &
       wannier_complex_print, &
       wannier_sqrt, &
       !wannier_sqrt_zgeev, &
       wannier_transform_mos, &
       ba_print, sort_virt, sort_eigenvalue
 
  type :: bloch_arrays
    complex(complexk), pointer, dimension(:,:) :: bloch_array
  end type bloch_arrays 
contains

  subroutine ba_print(mat,dim,iounit)
    complex(complexk), intent(in) :: mat(:,:)
    integer, intent(in)           :: dim,iounit

    integer :: i

    write(iounit,*) ''
    do i = 1,dim
       write(iounit,*) mat(i,:)
    enddo
    write(iounit,*) ''
    
  end subroutine ba_print
  
  !> @brief Find the inverse of a complex array
  subroutine wannier_invert(u_array, u_array_inverted,  nbast, wconf)

    complex(complexk), intent(in)    :: u_array(nbast,nbast)
    complex(complexk), intent(inout) :: u_array_inverted(nbast,nbast)
    integer, intent(in)              :: nbast
    type(wannier_config), intent(in) :: wconf
    
    complex(complexk), pointer, dimension(:,:) :: a,tmp
    complex(complexk), pointer, dimension(:)   :: work
    integer, pointer, dimension(:) :: ipiv
    integer :: i, j, info, error
    complex(complexk) :: trace
    
    call mem_alloc(a,nbast,nbast)
    call mem_alloc(work,nbast)
    call mem_alloc(ipiv,nbast)
    
    a(:,:) = u_array(:,:)
    
    call zgetrf(nbast, nbast, a, nbast, ipiv, info)
    if (info /= 0) then
       write (*,*) 'in wannier_invert zgetrf exited with info =',info
       call lsquit('wannier_invert',-1)
    endif
 
    call zgetri(nbast, a, nbast, ipiv, work, nbast, info)
    if (info /= 0) then
       write (*,*) 'in wannier_invert zgetri exited with info =',info
       call lsquit('wannier_invert',-1)
    endif
    
    u_array_inverted = a
   
    call mem_dealloc(a)
    call mem_dealloc(work)
    call mem_dealloc(ipiv)
        
    if (wconf%debug_level >= 0) then
       call mem_alloc(tmp,nbast,nbast)
       tmp(:,:) = 0.0_complexk
       call zgemm('n','n',nbast,nbast,nbast, c1, u_array, nbast, u_array_inverted, nbast, &
            & c0, tmp, nbast)

       trace = c0
       do i = 1, nbast
          trace = trace + tmp(i,i)
       enddo
       if (abs(real(trace) - nbast) > 1d-12) then
          call lsquit('S * S^-1 /= Id', -1)
       endif
       call mem_dealloc(tmp)
       
    endif
    
  end subroutine wannier_invert

  !> @brief Find the inverse of a rank-deficient complex array
  !> @author Elisa Rebolini
  !> @author Sept 2016
  !> @param u_array complexk array of size nbast x nbast to be inverted U
  !> @paramu_array_inverted inverted matrix U^{-1} as a 2d complexk array
  !> @param nbast size of the matrices
  !> @param wconf Wannier configuration settings
  !> @param lupri
  !> @pqrqm luerr
  subroutine wannier_invert_svd(u_array, u_array_inverted,  nbast, wconf, lupri, luerr)

    complex(complexk), intent(in)    :: u_array(nbast,nbast)
    complex(complexk), intent(inout) :: u_array_inverted(nbast,nbast)
    integer, intent(in)              :: nbast, lupri, luerr
    type(wannier_config), intent(in) :: wconf

    real(realk),pointer                        :: s(:),rwork(:)
    complex(complexk), pointer, dimension(:,:) :: a,tmp,u,vt,s2d
    complex(complexk), pointer, dimension(:)   :: work
    integer                                    :: i, j, info
    real(realk),parameter :: thr = 1e-6_realk
    
    call mem_alloc(a,nbast,nbast)
    call mem_alloc(s,nbast)
    call mem_alloc(u,nbast,nbast)
    call mem_alloc(vt,nbast,nbast)
    call mem_alloc(work,3*nbast)
    call mem_alloc(rwork,5*nbast)
    call mem_alloc(s2d,nbast,nbast)
    call mem_alloc(tmp,nbast,nbast)
    
    a(:,:) = u_array(:,:)
    u(:,:) = c0
    vt(:,:) = c0
    s(:) = 0.0_realk
    s2d(:,:) = c0

    call zgesvd('A','A',nbast,nbast,a,nbast,s,u,nbast,vt,nbast,work,3*nbast,rwork,info)

    if (wconf%debug_level >= 4) then
       write (luerr,*) 'SVD S(k)',s
    endif
    do i = 1,nbast
       if (s(i) < thr) then
          write (*,*) 'Overlap critically small',s(i),'Not considered for the inverse'
       else
          s2d(i,i) = 1/s(i)
       endif
    enddo

    tmp(:,:) = 0.0_complexk
    call zgemm('c','n',nbast,nbast,nbast,c1,vt,nbast,s2d,nbast,c0,tmp,nbast)
    call zgemm('n','c',nbast,nbast,nbast,c1,tmp,nbast,u,nbast,c0,u_array_inverted,nbast)

    !call zgemm('n','n',nbast,nbast,nbast,c1,u_array,nbast,u_array_inverted,nbast,c0,tmp,nbast)
    !write (*,*) tmp
    
    call mem_dealloc(a)
    call mem_dealloc(s)
    call mem_dealloc(u)
    call mem_dealloc(vt)
    call mem_dealloc(work)
    call mem_dealloc(rwork)
    call mem_dealloc(s2d)
    call mem_dealloc(tmp)
            
  end subroutine wannier_invert_svd


!> @brief Wrappper for Lapack hermitean eigenvalue solver (zheev) for complex arrays
!> @author Audun Skau Hansen
!> @date 2015
  subroutine wannier_zheev(complex_array, nbast, eigenvalues)
    integer, intent(in) :: nbast 
    complex(complexk), intent(inout)  :: complex_array(nbast,nbast)
    real(realk),  intent(inout)  :: eigenvalues(nbast)
    
    complex(complexk), dimension(:), pointer   :: work
    real(realk), dimension(:), pointer      :: rwork, w
    integer :: info, lwork, i

    lwork = max(1, 2*nbast - 1) 
   
    call mem_alloc(w, nbast)
    call mem_alloc(rwork, max(1, 3*nbast-2))
    call mem_alloc(work, max(1, lwork))
   
    !calculate eigenvalues
    call zheev('n', 'l', nbast, complex_array, nbast, &
               & eigenvalues, work, lwork, rwork, info)  

    call mem_dealloc(w)
    call mem_dealloc(rwork)
    call mem_dealloc(work)

  end subroutine wannier_zheev


!> @brief Transformation from k-space to wannier space
!> @author Audun Skau Hansen
!> @date   January, 2016
!> @param  wannier_mat  lattice of coefficients in wannier space 
!> @param  bloch_mats a set of coefficients in k-space 
!> @param  kpoints a 3 x npoints array containing the k-points
!> @param  nkpoints the number of k-points
!> @param  nbast size of the basis
!> @param  wconf wannier configuration
  subroutine wannier_transform(wannier_mat,bloch_mats,kpoints,nkpoints,nbast,wconf,norm)

    integer,intent(in)                  :: nbast, nkpoints
    type(lmatrixp), intent(inout)       :: wannier_mat
    real(realk), intent(in)             :: kpoints(3, nkpoints)
    real(realk)                         :: kpoint(3)
    type(wannier_config), intent(inout) :: wconf
    type(bloch_arrays), intent(in)      :: bloch_mats(nkpoints)
    complex(complexk), optional         :: norm
    
    type(lattice_domains_type) :: dom
    integer                    :: i,indx_l, j, k, l, kl
    type(matrix)               :: mat_L,wannier_i,wannier_r !reusing names
    type(matrix), pointer      :: ptr
    logical                    :: lstat
    real(realk)                :: latvec(3)
    real(realk)                :: k_dot_r, expon_kr_real, expon_kr_imag, norm_const
    complex(complexk)          :: expon_kr!, k_mat(nbast, nbast) 
    complex(complexk), pointer, dimension(:,:) :: wannier_mat_k
    complex(complexk), pointer, dimension(:,:) :: k_mat,tmp
    
    allocate(k_mat(nbast, nbast))
    call mem_alloc(wannier_mat_k, nbast, nbast)
    wannier_mat_k = 0.0_complexk
    if (present(norm)) then
       norm_const = norm
    else
       norm_const = 1._realk / sqrt(real(nkpoints, realk))
    endif
    
    !Initialization
    call mat_init(wannier_r,nbast,nbast)
    call mat_init(wannier_i,nbast,nbast)
    call mat_init(mat_L,nbast,nbast)
    call mat_zero(wannier_r)
    call mat_zero(wannier_i)
    
    call ltdom_init(dom, wannier_mat%periodicdims, maxval(wannier_mat%cutoffs))
    call ltdom_getdomain(dom, wannier_mat%cutoffs, incl_redundant=.true.)

    !Loop over the cells in the domain dom
    do i = 1, dom%nelms 
       indx_l = dom%indcs(i)
              
       
       call mat_zero(mat_L)
       
       latvec = wu_indxtolatvec(wconf%blat, indx_l)

       !sum over all k-points (or rather, only positive ones?)
       do j = 1, nkpoints
         kpoint = kpoints(:, j)

         !MO coeffs for the given k-point
         k_mat = bloch_mats(j)%bloch_array
          
         k_dot_r = kpoint(1)*latvec(1) + kpoint(2)*latvec(2) + &
                 & kpoint(3)*latvec(3)

         expon_kr = norm_const * exp(-cmplx(0,1)*k_dot_r) 

         
         call mem_alloc(tmp,nbast,nbast)
         tmp(:,:) = 0.0_complexk
         call zaxpy(nbast*nbast, expon_kr, k_mat, 1, tmp, 1)
         do k=1,nbast        
            do l=1,nbast
               kl = (l-1) * nbast + k
               mat_L%elms(kl) = mat_L%elms(kl) + real(tmp(k,l))
            enddo
         enddo
         call mem_dealloc(tmp)
         !note: mat_L%elms is a real array, but are we sure no imaginary parts
         !remains? --> well obviously it is not the case !!!!
         !> @todo Sum k-points by pair k, -k in order to get a real 


         !Compute and accumulate the real and imaginary part of the Bloch matrix
         !call mat_daxpy(expon_kr_imag, k_mat, wannier_i)
         !call mat_daxpy(expon_kr_real, k_mat, wannier_r)

       enddo

      
       !MOs in the wannier basis now contained in mat_L%elms
       !set the coefficients and assert that wannier_i == 0
       !check the following with Karl:

       
       ptr => lts_get(wannier_mat, indx_l)
       if (.not.associated(ptr)) then
          call lts_mat_init(wannier_mat, indx_l)
          ptr => lts_get(wannier_mat, indx_l)
       endif
       !write(*,*) "transformed elms:", mat_L%elms, indx_l
       !write(*,*) "-----------------------------"
       call mat_copy(1.0_realk,mat_L,ptr)
       !write(*,*) "elements copied"
              
    enddo

    
    !combine the real and imaginary parts
    !bloch_mat = reshape(cmplx(1,0)*bloch_r%elms + &
    !     & cmplx(0,1)*bloch_i%elms, (/nbast, nbast/))

    
    call ltdom_free(dom)
    call mat_free(wannier_r)
    call mat_free(wannier_i)
    call mat_free(mat_L)

    call mem_dealloc(wannier_mat_k)
    deallocate(k_mat)
  end subroutine wannier_transform

  !> @brief Transformation from k-space to wannier space for the MOS
  !> @author Elisa Rebolini
  !> @date   January, 2016
  !> @param  mos_w  lattice of coefficients in wannier space 
  !> @param  mos_k_array Array of the MO coefficients in all k-points 
  !> @param  kpoints a 3 x npoints array containing the k-points
  !> @param  nkpoints the number of k-points
  !> @param  nbast size of the basis
  !> @param  wconf wannier configuration
  subroutine wannier_transform_mos(mos_w,mos_k_array,kpoints,nkpoints,nbast,wconf)

    integer,intent(in)                  :: nbast, nkpoints
    type(lmatrixp), intent(inout)       :: mos_w
    real(realk), intent(in)             :: kpoints(3, nkpoints)
    type(wannier_config), intent(inout) :: wconf
    type(bloch_arrays), intent(in)      :: mos_k_array(nkpoints)

    type(lattice_domains_type) :: dom
    real(realk)                :: vecM(3), vecK(3)
    integer                    :: m, indx_m, k, i, j, ij, norb
    real(realk)                :: k_dot_M
    complex(complexk)          :: exp_ikM
    complex(complexk), pointer, dimension(:,:)  :: mos_k, tmp
    type(matrix)               :: tmp_real
    type(matrix), pointer      :: ptr

    complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)

    norb = mos_w%ncol
    
    !Allocation of the intermediate matrices
    call mem_alloc(mos_k, nbast, norb)
    call mem_alloc(tmp, nbast, norb)
    call mat_init(tmp_real,nbast, norb)
    
    !Initialisation of the domain d_0
    call ltdom_init(dom, mos_w%periodicdims, maxval(mos_w%cutoffs))
    call ltdom_getdomain(dom, mos_w%cutoffs, incl_redundant=.true.)

    !Loop over the lattice cells
    loop_m: do m=1,dom%nelms 
       indx_m = dom%indcs(m)

       !Coordinates of the lattice vector vec(M)
       vecM = wu_indxtolatvec(wconf%blat, indx_M)

       !Initialization of the sum over the k-points
       tmp(:,:) = 0.0_complexk
       
       !Loop over the k-points
       loop_k: do k=1,nkpoints
          !Coordinates of the k-point k
          vecK = kpoints(:, k)
          !Scalar product vec(K).vec(M)
          k_dot_M = vecK(1)*vecM(1) + vecK(2)*vecM(2) + vecK(3)*vecM(3)
          !Exponential prefactor exp(ikM)/N_BvK
          exp_ikM = exp(cmplx(0,1)*k_dot_M) / real(nkpoints,realk)
          !MO coefficient at k-point k
          mos_k(:,:) = c0
          mos_k = mos_k_array(k)%bloch_array
          !Nultiplication of the MO by the exponential prefactor for k-point k
          !add it to the tmp matrix
          call zaxpy(nbast*norb, exp_ikM, mos_k, 1, tmp, 1)
       enddo loop_k
       !copy the real part of tmp into tmp_real
       do i=1,nbast
          do j=1,norb
             ij = (j-1) * nbast + i
             tmp_real%elms(ij) = real(tmp(i,j),realk)
          enddo
       enddo
       !Assign a pointer to the transformed mos to the corresponding block os mos_w
       ptr => lts_get(mos_w, -indx_M)
       call mat_copy(1.0_realk,tmp_real,ptr)
       
!       write(*,*) 'MOS W-space',indx_m
!       do i=1,nbast
!          write(*,*) tmp(i,:)
!       enddo
!       write(*,*) ''
    enddo loop_m

    !Deallocation of the intermediate matrices
    call mem_dealloc(mos_k)
    call mem_dealloc(tmp)
    call mat_free(tmp_real)
    
    !Free domain
    call ltdom_free(dom)
    
  end subroutine wannier_transform_mos

  !> @brief transform the intial Wannier functions to Bloch functions
  !> |kp> = 1/sqrt(N_BvK) sum_L exp(ik.L) |Lp> with |Lp> = sum_mu |L mu> C_0mu,0p
  subroutine bloch_transform_mos(wannier_mat,bloch_mat,kpoint,nkpoint,nbast,wconf,lupri,luerr)

    integer,intent(in)                      :: nbast,lupri,luerr
    type(lmatrixp), intent(in)              :: wannier_mat
    complex(complexk), pointer, intent(out) :: bloch_mat(:,:)!(nbast,nbast)
    real(realk), intent(in)                 :: kpoint(3)
    type(wannier_config), intent(inout)     :: wconf

    type(lattice_domains_type) :: dom
    integer                    :: i,indx_l,nkpoint,norb
    type(matrix)               :: mat_L,bloch_i,bloch_r
    logical                    :: lstat
    real(realk)                :: latvec(3)
    real(realk)                :: k_dot_r, expon_kr_real, expon_kr_imag, norm_const
    complex(complexk)          :: expon_kr
	 
    norm_const = 1._realk / sqrt(real(nkpoint, realk))
    norb = wannier_mat%ncol
    
    !Initialization
    call mat_init(bloch_r,nbast,norb)
    call mat_init(bloch_i,nbast,norb)
    call mat_init(mat_L,nbast,norb)
    call mat_zero(bloch_r)
    call mat_zero(bloch_i)
    
    call ltdom_init(dom, wannier_mat%periodicdims, maxval(wannier_mat%cutoffs))
    call ltdom_getdomain(dom, wannier_mat%cutoffs, incl_redundant=.true.)
    !Loop over the cells in the domain dom
    do i = 1, dom%nelms 
       indx_l = dom%indcs(i)

       call mat_zero(mat_L)
       call lts_copy(wannier_mat, -indx_L, lstat, mat_L)
       if (.not. lstat)  cycle

       latvec = wu_indxtolatvec(wconf%blat, indx_l)

       k_dot_r = kpoint(1)*latvec(1) + kpoint(2)*latvec(2) + &
            & kpoint(3)*latvec(3)

       expon_kr = norm_const * exp(cmplx(0,-1)*k_dot_r)
       expon_kr_real = real(expon_kr)
       expon_kr_imag = imag(expon_kr)
       
       !Compute and accumulate the real and imaginary part of the Bloch matrix
       call mat_daxpy(expon_kr_imag, mat_L, bloch_i)
       call mat_daxpy(expon_kr_real, mat_L, bloch_r)
             
    enddo

    !combine the real and imaginary parts
    bloch_mat = reshape(cmplx(1,0)*bloch_r%elms + &
         & cmplx(0,1)*bloch_i%elms, (/nbast, norb/))

    call ltdom_free(dom)
    call mat_free(bloch_r)
    call mat_free(bloch_i)
    call mat_free(mat_L)

  end subroutine bloch_transform_mos


  !> @brief transform from Wannier to Bloch space
  !> S^k_mu nu = sum_L exp(ik.L) S^L_mu nu
  subroutine bloch_transform(wannier_mat,bloch_mat,kpoint,nkpoint,nbast,wconf,lupri,luerr)

    integer,intent(in)                  :: nbast,lupri,luerr
    type(lmatrixp), intent(in)          :: wannier_mat
    complex(complexk), intent(out)      :: bloch_mat(nbast,nbast)
    real(realk), intent(in)             :: kpoint(3)
    type(wannier_config), intent(inout) :: wconf

    type(lattice_domains_type) :: dom
    integer                    :: i,indx_l,nkpoint
    type(matrix)               :: mat_L,bloch_i,bloch_r
    logical                    :: lstat
    real(realk)                :: latvec(3)
    real(realk)                :: k_dot_r, expon_kr_real, expon_kr_imag, norm_const
    complex(complexk)          :: expon_kr
	 
    norm_const = 1._realk 

    !Initialization
    call mat_init(bloch_r,nbast,nbast)
    call mat_init(bloch_i,nbast,nbast)
    call mat_init(mat_L,nbast,nbast)
    call mat_zero(bloch_r)
    call mat_zero(bloch_i)
    
    call ltdom_init(dom, wannier_mat%periodicdims, maxval(wannier_mat%cutoffs))
    call ltdom_getdomain(dom, wannier_mat%cutoffs, incl_redundant=.true.)
    !Loop over the cells in the domain dom
    do i = 1, dom%nelms 
       indx_l = dom%indcs(i)

       call mat_zero(mat_L)
       call lts_copy(wannier_mat, indx_L, lstat, mat_L)
       if (.not. lstat)  cycle

       latvec = wu_indxtolatvec(wconf%blat, indx_l)

       k_dot_r = kpoint(1)*latvec(1) + kpoint(2)*latvec(2) + &
            & kpoint(3)*latvec(3)

       expon_kr = norm_const * exp(cmplx(0,1)*k_dot_r)
       expon_kr_real = real(expon_kr)
       expon_kr_imag = imag(expon_kr)
       
       !Compute and accumulate the real and imaginary part of the Bloch matrix
       call mat_daxpy(expon_kr_imag, mat_L, bloch_i)
       call mat_daxpy(expon_kr_real, mat_L, bloch_r)
       
    enddo

    !combine the real and imaginary parts
    bloch_mat = reshape(cmplx(1,0)*bloch_r%elms + &
         & cmplx(0,1)*bloch_i%elms, (/nbast, nbast/))

    call ltdom_free(dom)
    call mat_free(bloch_r)
    call mat_free(bloch_i)
    call mat_free(mat_L)

  end subroutine bloch_transform

  
!> @brief Print complex array properly
  subroutine wannier_complex_print(zarray, nbast)
    integer, intent(in) :: nbast
    
    complex(complexk), intent(in) :: zarray(nbast, nbast)
    integer :: i
    do i = 1, nbast
      write(*,*) zarray(:, i)
    enddo

  end subroutine wannier_complex_print


  !> @brief Computes the square root of a square complex matrix
  !> N.B. the square root of a matrix is not uniquely defined!
  !> @author E. Rebolini
  !> @date Jan 2016
  !> @param mat Square complex matrix
  !> @param sqrt_mat Square root of mat
  !> @param nbast Number of basis functions
  !> @param wconf Wannier configuration settigs
  subroutine wannier_sqrt(mat,sqrt_mat,nbast,wconf,lupri,luerr)
    integer, intent(in)                 :: nbast,lupri,luerr
    complex(complexk), intent(inout)    :: mat(nbast,nbast),sqrt_mat(nbast,nbast)
    type(wannier_config), intent(inout) :: wconf

    complex(complexk), pointer, dimension(:,:)  :: cholesky, matU, matVT
    complex(complexk), pointer, dimension(:,:)  :: tmp, tmp2, tmp_debug, mat_copy, mat_diff, mat_copy2
    complex(complexk), pointer, dimension(:)    :: work
    real(realk), pointer, dimension(:)          :: matSigma, rwork
    complex(complexk), pointer, dimension(:,:)  :: matSigma2D
    integer                                     :: i, j, info, lwork
    real(realk)                                 :: dzasum,sum, rms

    lwork = 5*nbast
    
    call mem_alloc(cholesky,nbast,nbast)
    cholesky(:,:) = 0.0_complexk

    call mem_alloc(tmp,nbast,nbast)
    call mem_alloc(matSigma,nbast)
    call mem_alloc(matU,nbast,nbast)
    call mem_alloc(matVT,nbast,nbast)
    call mem_alloc(work,lwork)
    call mem_alloc(rwork,lwork)
    call mem_alloc(matSigma2D,nbast,nbast)
    matSigma2D(:,:) = 0.0_complexk
    matSigma(:) = 0.0_realk
    matU(:,:) = 0.0_complexk
    matVT(:,:) = 0.0_complexk
    
    !Copy invS to test the Cholesky deco;position
    if (wconf%debug_level >= 0) then
       call mem_alloc(mat_copy,nbast,nbast)
       call mem_alloc(mat_copy2,nbast,nbast)
       call mem_alloc(tmp_debug,nbast,nbast)
       call mem_alloc(tmp2,nbast,nbast)
       call mem_alloc(mat_diff,nbast,nbast)
       mat_copy(:,:) = mat(:,:)
    endif
       
    
    !Cholesky decomposition
    call zpotrf('u', nbast, mat, nbast, info)
    if (.not. (info == 0)) then
       write (*,*) 'ZPOTRF failed with INFO =', INFO
    endif
    do i = 1, nbast
       do j=i, nbast
          cholesky(i,j) = mat(i,j)
       enddo
    enddo
        
    !Test Cholesky
    !Test if copy of S^-1 - G^T * G = 0
    if (wconf%debug_level >= 0) then
       
       tmp_debug(:,:) = 0.0_complexk
       sum = c0
       
       call zgemm('c','n',nbast,nbast,nbast,c1,cholesky,nbast,cholesky,nbast, &
            & c0,tmp_debug,nbast)

       mat_diff(:,:) = mat_copy(:,:) - tmp_debug(:,:)
       do i =1,nbast
          do j = 1,nbast
             sum = sum + abs(mat_diff(i,j))**2 
          enddo
       enddo

       rms = sqrt(real(sum,realk))/(nbast*nbast)
       
       if (rms > 1d-12)  then
          write (*,*) 'RMS of S^-1 - G^T * G =',rms
          !write (*,*) 'This is most likely due to linear dependency -- try another basis set'
          !call lsquit('Cholesky S^-1', -1)
       endif
    endif
    
    !SVD decomposition
    !copy for test of SVD
    if (wconf%debug_level >= 0) then
       mat_copy2(:,:) = cholesky(:,:)
    endif
       
    call zgesvd('A','A',nbast,nbast,cholesky,nbast,matSigma,matU,nbast,matVT,nbast, &
         & WORK,lwork,rwork,INFO)
    if (.not. (info == 0)) then
       write (*,*) 'ZGESVD failed with INFO =', INFO
    endif

    if (wconf%debug_level >= 4) then
       write(luerr,*) 'SVD for S^{-1/2}'
       write(luerr,*) matSigma
    endif
    do i=1,nbast
       matSigma2D(i,i) = matSigma(i)
    enddo
    
    !Test SVD
    if (wconf%debug_level >= 0) then
       tmp_debug(:,:) = 0.0_complexk
       tmp2(:,:) = 0.0_complexk

       call zgemm('n','n',nbast,nbast,nbast,c1,matU,nbast,matSigma2D,nbast, &
            & c0,tmp_debug,nbast)
       call zgemm('n','n',nbast,nbast,nbast,c1,tmp_debug,nbast,matVT,nbast, &
            & c0,tmp2,nbast)

       !Test if U * Sigma * V^T gives G back
       mat_diff(:,:) = mat_copy2(:,:) - tmp2(:,:)
       sum = c0
       do i = 1,nbast
          do j= 1, nbast
             sum = sum + abs(mat_diff(i,j))**2
          enddo
       enddo
       if (sqrt(real(sum))/(nbast*nbast) > 1d-12) then
          write (*,*) 'RMS  U * Sigma * V^T - G =',sqrt(real(sum))/(nbast*nbast)
          call lsquit('SVD decomposition', -1)
       endif
    endif
        
    !Construction of the square root V * Sigma * V^T
    !For some reason, the convention for the Cholesky decomp. is different for complex matrices
    !Thus one has to take V * Sigma * V^T instead of U * Sigma * U^T
    
    tmp(:,:) = 0.0_complexk
    call zgemm('c','n',nbast,nbast,nbast,c1,matVT,nbast,matSigma2D,nbast,c0,tmp,nbast)
    call zgemm('n','n',nbast,nbast,nbast,c1,tmp,nbast,matVT,nbast,c0,sqrt_mat,nbast)

    !Test square root
    if (wconf%debug_level >= 0) then
       
       tmp2(:,:) = 0.0_complexk
       call zgemm('n','n',nbast,nbast,nbast,c1,sqrt_mat,nbast, &
            & sqrt_mat,nbast,c0,tmp2,nbast)
       
       mat_diff(:,:) = mat_copy(:,:) - tmp2(:,:)
       !Test if S^{-1/2} * S^{-1/2} = S^-1
       do i = 1,nbast
          do j= 1, nbast
             sum = sum + abs(mat_diff(i,j))**2
          enddo
       enddo
       sum = sqrt(sum) / (nbast*nbast)
       if (sum > 1d-12) then
          write (*,*) 'RMS S^{-1/2} * S^{-1/2} - S^-1 =', sum
          call lsquit('S^-1/2',-1)
       endif
    endif

    call mem_dealloc(cholesky)
    call mem_dealloc(matSigma2D)
    call mem_dealloc(matVT)
    call mem_dealloc(matSigma)
    call mem_dealloc(matU)
    call mem_dealloc(work)
    call mem_dealloc(rwork)
    call mem_dealloc(tmp)
    
    if (wconf%debug_level >= 0) then
       call mem_dealloc(tmp_debug)
       call mem_dealloc(tmp2)
       call mem_dealloc(mat_diff)
       call mem_dealloc(mat_copy)
       call mem_dealloc(mat_copy2)
    endif
    
  end subroutine wannier_sqrt

  
!!$  TO BE TESTED !!!
!!$  subroutine wannier_sqrt_zgeev(mat,sqrt_mat,nbast,wconf,lupri,luerr)
!!$    integer, intent(in)                 :: nbast,lupri,luerr
!!$    complex(complexk), intent(inout)    :: mat(nbast,nbast),sqrt_mat(nbast,nbast)
!!$    type(wannier_config), intent(inout) :: wconf
!!$
!!$    complex(complexk),pointer :: w(:),vl(:,:),vr(:,:),work(:),lambda(:,:),tmp(:,:)
!!$    real(realk),pointer       :: rwork(:)
!!$    integer                   :: info, i
!!$
!!$    call mem_alloc(w,nbast)
!!$    call mem_alloc(vl,nbast,nbast)
!!$    call mem_alloc(vr,nbast,nbast)
!!$    call mem_alloc(work,3*nbast)
!!$    call mem_alloc(rwork,2*nbast)
!!$    call mem_alloc(lambda,nbast,nbast)
!!$    call mem_alloc(tmp,nbast,nbast)
!!$    
!!$    
!!$    call zgeev('V','V',nbast,mat,nbast,w,vl,nbast,vr,nbast,work,3*nbast,rwork,info)
!!$    !write(*,*) w
!!$    if (info /= 0) call lsquit('wannier_sqrt_zgeev',-1)
!!$
!!$    lambda(:,:) = 0.0_complexk
!!$    do i=1,nbast
!!$       lambda(i,i) = sqrt(real(w(i),realk))
!!$    enddo
!!$
!!$    call zgemm('N','N',nbast,nbast,nbast,c1,vl,nbast,lambda,nbast,c0,tmp,nbast)
!!$    call zgemm('N','C',nbast,nbast,nbast,c1,tmp,nbast,vr,nbast,c0,sqrt_mat,nbast)
!!$    
!!$    call mem_dealloc(w)
!!$    call mem_dealloc(vl)
!!$    call mem_dealloc(vr)
!!$    call mem_dealloc(work)
!!$    call mem_dealloc(rwork)
!!$    call mem_dealloc(lambda)
!!$    call mem_dealloc(tmp)
!!$    
!!$  end subroutine wannier_sqrt_zgeev

  subroutine wannier_dggev(F,S,eival,C,lupri,luerr)

    type(matrix), intent(in)          :: F,S
    real(realk), pointer, intent(out) :: eival(:)
    type(matrix), intent(out)         :: C
    integer, intent(in)               :: lupri, luerr

    integer             :: ndim, nbas, i, j, ij, info
    real(realk),pointer :: alphar(:),alphai(:),beta(:),vl(:,:),vr(:,:),work(:),stmp(:,:),ftmp(:,:),tmp(:,:),tmp2(:,:)
    real(realk)         :: norm
    
    ndim = F%nrow
    
    call mem_alloc(alphar,ndim)
    call mem_alloc(alphai,ndim)
    call mem_alloc(beta,ndim)
    call mem_alloc(vl,1,ndim)
    call mem_alloc(vr,ndim,ndim)
    call mem_alloc(work,8*ndim)
    call mem_alloc(stmp,ndim,ndim)
    call mem_alloc(ftmp,ndim,ndim)

    do i=1,ndim
       do j=1,ndim
          ij = (j-1)*ndim +i
          stmp(i,j) = S%elms(ij)
          ftmp(i,j) = F%elms(ij)
       enddo
    enddo
    
    call dggev('N','V',ndim,ftmp,ndim,stmp,ndim,alphar,alphai,beta,vl,1,vr,ndim,work,8*ndim,info)
    if (info /= 0) then
       write (*,*) 'DGGEV failed in WannierComplexUtils with info=',info
       call lsquit('wannier_dggev',-1)
    endif

    do i=1,ndim
       if (alphai(i) /= 0 ) then
          write (*,*) 'Complex Eigenvalue'
          call lsquit('complex orbital energies',-1)
       endif
       eival(i)=alphar(i)/beta(i)
    enddo
    
    call sort_eigenvalue(vr,eival,ndim,lupri,luerr)

    !Normalisation C^T S C = 1
    call mem_alloc(tmp,ndim,ndim)
    call mem_alloc(tmp2,ndim,ndim)

    call dgemm('N','N',ndim,ndim,ndim,1.0_realk,stmp,ndim,vr,ndim,0.0_realk,tmp,ndim)
    call dgemm('T','N',ndim,ndim,ndim,1.0_realk,vr,ndim,tmp,ndim,0.0_realk,tmp2,ndim)

!!$    write(*,*) stmp
!!$    write(*,*) tmp2
!!$    
!!$    do j=1,ndim
!!$       norm = dsqrt(tmp2(j,j))
!!$       write(*,*) norm
!!$       do i=1,ndim
!!$          !ij = (j-1)*ndim +i
!!$          !C%elms(ij) = vr(i,j) / norm
!!$          vr(i,j) = vr(i,j) /norm
!!$       enddo
!!$    enddo
!!$
!!$    call dgemm('N','N',ndim,ndim,ndim,1.0_realk,stmp,ndim,vr,ndim,0.0_realk,tmp,ndim)
!!$    call dgemm('T','N',ndim,ndim,ndim,1.0_realk,vr,ndim,tmp,ndim,0.0_realk,tmp2,ndim)
!!$
!!$    
    
    do j=1,ndim
       norm = dsqrt(tmp2(j,j))
       do i=1,ndim
          ij = (j-1)*ndim +i
          C%elms(ij) = vr(i,j) / norm
          !vr(i,j) = vr(i,j) /norm
       enddo
    enddo
    
    call mem_dealloc(tmp2)
    call mem_dealloc(tmp) 
    
    call mem_dealloc(alphar)
    call mem_dealloc(alphai)
    call mem_dealloc(beta)
    call mem_dealloc(vl)
    call mem_dealloc(vr)
    call mem_dealloc(work)
    call mem_dealloc(stmp)
    call mem_dealloc(ftmp)

 
  end subroutine wannier_dggev

    !> @brief Sort the eigenvectors by decreasing overlap with the previous set
  !> @author Elisa Rebolini
  !> @date Sept 2016
  !>
  !> @param U unitary transform between old and new set
  !> @param sigma 
  !> @param nbas
  !> @param lupri
  !> @param luerr
  subroutine sort_virt(U,sigma,nbas,lupri,luerr)

    integer, intent(in)                :: nbas,lupri,luerr
    real(realk),pointer, intent(inout) :: U(:,:)
    real(realk),pointer, intent(inout) :: sigma(:)

    integer :: i, Location 

    do i=1,nbas
       Location = FindMax(sigma, i, nbas)
       call dswap(sigma,i,location,nbas)
       call d2swap(U,i,location,nbas)
    enddo

  end subroutine sort_virt

  
  !> @brief returns the location of the maximum in the section between StartIndx and EndIndx.
  !> @author Elisa Rebolini
  !> @date Sept 2016
  integer function FindMax(x, StartIndx, EndIndx)
    implicit  none
    real(realk), dimension(1:), intent(IN) :: x
    integer, intent(IN)                    :: StartIndx, EndIndx
    real(realk)                            :: Mx
    integer                                :: Location
    integer                                :: i

    Mx = x(StartIndx)           ! assume the first is the max
    Location = StartIndx        ! record its position
    do i = StartIndx+1, EndIndx ! start with next elements
       if (x(i) > Mx) then      !   if x(i) greater than the max?
          Mx = x(i)             !      Yes, a new max found
          Location = i          !      record its position
       end if
    end do
    FindMax = Location          ! return the position
  end function  FindMax

  !> @brief Sort the eigenvectors by increasing eigenvalue
  subroutine sort_eigenvalue(U,sigma,nbas,lupri,luerr)

    integer, intent(in)                :: nbas,lupri,luerr
    real(realk),pointer, intent(inout) :: U(:,:)
    real(realk),pointer, intent(inout) :: sigma(:)

    integer :: i, Location 

    do i=1,nbas
       Location = FindMin(sigma, i, nbas)
       call dswap(sigma,i,location,nbas)
       call d2swap(U,i,location,nbas)
    enddo

  end subroutine sort_eigenvalue

  
  !> @brief returns the location of the maximum in the section between StartIndx and EndIndx.
  integer function  FindMin(x, StartIndx, EndIndx)
    implicit  none
    real(realk), dimension(1:), intent(IN) :: x
    integer, intent(IN)                    :: StartIndx, EndIndx
    real(realk)                            :: Mx
    integer                                :: Location
    integer                                :: i

    Mx = x(StartIndx)           ! assume the first is the min
    Location = StartIndx        ! record its position
    do i = StartIndx+1, EndIndx ! start with next elements
       if (x(i) < Mx) then      !   if x(i) greater than the max?
          Mx = x(i)             !      Yes, a new max found
          Location = i          !      record its position
       end if
    end do
    FindMin = Location          ! return the position
  end function  FindMin

  
  !> @ brief swaps the values of its two formal arguments of type real(realk)
  subroutine  dswap(x, a, b, n)
    implicit  none
    integer, intent(in)        :: n
    integer, intent(IN)        :: a, b
    real(realk), intent(inout) :: x(n)

    real(realk)                :: tmp

    tmp = x(a)
    x(a) = x(b)
    x(b) = tmp

  end subroutine  dswap

  !> @brief swaps the column a and b of a 2d array of type real(realk)
  subroutine  d2swap(x, a, b, n)
    implicit  none
    integer, intent(in)               :: n
    integer, intent(IN)               :: a, b
    real(realk), intent(inout)        :: x(n,n)

    real(realk),pointer        :: tmp(:)

    call mem_alloc(tmp,n)
    tmp = x(:,a)
    x(:,a) = x(:,b)
    x(:,b) = tmp
    call mem_dealloc(tmp)
  end subroutine  d2swap
  
end module wannier_complex_utils
