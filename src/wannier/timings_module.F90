!> @file timings_module.F90
!> Timing module.
!> @ brief Generic module to manage timings of diffenent tasks. Add times to 
!> the diffenent taskt f.eks. in subsequent iterations.
!>
!> @example 
!> type(timings_type) :: timings
!> timings_init(timings, num_jobs)
!> 
!> timings_start(timings, 'jobname1')
!> !execute code to be timed ...
!> timings_stop(timings, 'jobname1') 
!> ...
!> !print:
!> timings_print(timings)
!> timings_free(timings)
!> 
!> @author Karl R. Leikanger 
!> date 2014

module timings_module

use precision
implicit none

private

public :: timings_init, timings_free, timings_print, &
	& timings_start, timings_stop, timings_type

type timings_type
	integer :: num_jobs, num_init
	type(job), pointer :: jobs(:)
	integer :: printlevel, lupri
end type

type job
	character (len=30) :: jobname
	real(realk) :: cputime_begin, walltime_begin, cputime, walltime
	integer :: ncalls
end type

contains
	
!> @brief Initialize and allocate mem for timings_type.
!> @param timings timings_type.
!> @param num_jobs_inp Max. number of different jobs to time.
!> @param lupri Logical print unit
!> @param printlevel 0: (nothing), 1: (to lupri), 2: (to lupri & screen)
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_init(timings, num_jobs, lupri, printlevel)
	! input
	integer, intent(in) :: num_jobs, lupri
	type(timings_type), intent(inout):: timings
	integer, intent(in), optional :: printlevel
	! local
	integer :: i

	allocate(timings%jobs(num_jobs))
	
	timings%num_jobs = num_jobs
	timings%num_init = 0
	timings%lupri = lupri
	do i = 1, num_jobs
		timings%jobs(i)%cputime = 0.0
		timings%jobs(i)%walltime = 0.0
		timings%jobs(i)%ncalls = 0
	end do	
	
	if ( present(printlevel) ) then
		timings%printlevel = printlevel
	else
		timings%printlevel = 1
	endif
	if ((printlevel < 0) .or. (printlevel > 2)) then
		call lsquit('mod:timings_module, sr:timings_init. &
			& Not valid value for printlevel; must be set to 0, 1 or 2.', -1)
	endif
end subroutine

!> @brief Deallocate mem.
!> @param timings timings_type.
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_free(timings)
		! input
		type(timings_type), intent(inout):: timings
		
		deallocate(timings%jobs)
end subroutine

!> @brief Initialize names of the different jobs.
!> @param timings timings_type.
!> @param jname Jobname.
!> @param job The number of the job to name.
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_name_timer(timings, jname, job)
		! input
		type(timings_type), intent(inout):: timings
		integer, intent(in) :: job
		character (len=30), intent(in) :: jname

		timings%jobs(job)%jobname = jname
		timings%num_init = timings%num_init + 1
end subroutine 

!> @brief Add time to one of the jobs. Start timing. If job with name jobname
!> exists, add time. Otherwise, initiate timing with name jobname and start.
!> @param timings timings_type.
!> @param jobname Name of the job.
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_start(timings, jname)
	! input 
   character(len=*), intent(in) :: jname
	type(timings_type), intent(inout) :: timings
	! local
	integer :: i
	character(len=30) :: jobname

	if (len(jname) > 30) then
		call lsquit('mod:timings_module, sr:timings_start: len(jname) > 30.', -1)
	endif
	write(jobname, "(A30)") jname
	! check if initiated; and if it is start timing and return
	do i = 1, timings%num_init
		if (timings%jobs(i)%jobname == jobname) then
			call timings_start_jobnr(timings, i)
			return
		endif
	enddo
	! otherwise; initiate new job and start timing
	call timings_name_timer(timings, jobname, i)
	call timings_start_jobnr(timings, i)
end subroutine

!> @brief Add time to one of the job. Start timing.
!> @param timings timings_type.
!> @param job Number of the job.
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_start_jobnr(timings, job)
	! input 
	type(timings_type), intent(inout) :: timings
	integer, intent(in) :: job
	! local
	real(realk) :: cputime, walltime

   call ls_gettim(cputime, walltime)
	timings%jobs(job)%walltime_begin = walltime
	timings%jobs(job)%cputime_begin = cputime
	timings%jobs(job)%ncalls = timings%jobs(job)%ncalls + 1 
end subroutine 

!> @brief Stop timing.
!> @param timings timings_type.
!> @param jname jobname
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_stop(timings, jname)
	! input
	character(len=*), intent(in) :: jname
	type(timings_type), intent(inout) :: timings
	! local
	integer :: i
	character(len=90) :: errmsg
	character(len=30) :: jobname

	write(jobname, "(A30)") jname
	! check if initiated; and if it is stop timing and return
	do i = 1, timings%num_init 
		if (timings%jobs(i)%jobname == jobname) then
			call timings_stop_jobnr(timings, i)
			return
		endif
	enddo
	! otherwise;
	write(errmsg, *) &
		& 'mod:timings_module, s.r.:timings_stop, job is not initiated: ', &
		& trim(adjustl(jobname))
	call lsquit(errmsg, -1)
end subroutine


!> @brief Add time to one of the job. Stop timing.
!> @param timings timings_type.
!> @param job Number of the job.
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_stop_jobnr(timings, job)
	! input
   integer, intent(in) :: job
	type(timings_type), intent(inout) :: timings
	! local
   real(realk) :: cputime_end, walltime_end, deltawall, deltacpu
	character (len=9) :: dw_s, dc_s

   call ls_gettim(cputime_end,walltime_end)
	deltawall = walltime_end - timings%jobs(job)%walltime_begin
	deltacpu = cputime_end - timings%jobs(job)%cputime_begin
	
	timings%jobs(job)%cputime = timings%jobs(job)%cputime + deltacpu
	timings%jobs(job)%walltime = timings%jobs(job)%walltime + deltawall

	if ( timings%printlevel == 1 ) then
		write (dw_s,'(2F9.2)') deltawall
		write (dc_s,'(2F9.2)') deltacpu
		write (timings%lupri, *) '>>>> Time consumption: ', & 
			& adjustl(timings%jobs(job)%jobname), &
			& ',  Walltime:', dw_s, ',  CPUtime:', dc_s, '.'
	endif
	if (timings%printlevel > 1 ) then 
		write (dw_s,'(2F9.2)') deltawall
		write (dc_s,'(2F9.2)') deltacpu
		write (timings%lupri, *) '>>>> Time consumption: ', &
			& adjustl(timings%jobs(job)%jobname), &
			& ',  Walltime:', dw_s, ',  CPUtime:', dc_s, '.'
	endif
end subroutine

!> @brief Print all the collected information.
!> @param timings timings_type.
!> @param header Print header. 
!> @author Karl R. Leikanger
!> date 2014
subroutine timings_print(timings, header)
	! input
	type(timings_type), intent(in) :: timings
	character(len=*), intent(in) :: header

	if (timings%printlevel > 1) call printall(timings, header, timings%lupri)
	call printall(timings, header, timings%lupri)
end subroutine

!> @brief Print all the collected information.
!> @param timings timings_type.
!> @param header Print header. 
!> @param lu Logical print unit.
!> @author Karl R. Leikanger
!> date 2014
subroutine printall(timings, header, lu)
	! input
	type(timings_type), intent(in) :: timings
	character(len=*), intent(in) :: header
	integer, intent(in) :: lu
	!local
	integer :: i
	write (lu, *)
	write (lu, *) '========================================================&
		&======================================='
	write (lu, "(A20,A30)") 'TIMINGS:', adjustl(header)
	write (lu, *) '========================================================&
		&======================================='
	write (lu,'(A10,A25,A18,A18,A20)') 'Task', '', 'Walltime', 'Cputime', &
		& 'Times called'
	write (lu, *) '--------------------------------------------------------&
		&---------------------------------------'
	do i = 1, timings%num_init
		write (lu,"(A5,A30,F18.2,F18.2,I12)") ' >>> ', &
			& adjustl(timings%jobs(i)%jobname), &
			& timings%jobs(i)%walltime, &
			& timings%jobs(i)%cputime, &
			& timings%jobs(i)%ncalls
	enddo
	write (lu, *) '========================================================&
		&======================================='
	write (lu, *) '========================================================&
		&======================================='
	write (lu, *)
end subroutine

end module
