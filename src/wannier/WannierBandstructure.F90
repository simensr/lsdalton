module wannier_bandstructure_tools
  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use typedeftype, only: daltoninput, &
       lssetting
  use wlattice_domains, only: ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage!, only: lmatrixp, lts_init, lts_free, lts_get
  use matrix_module, only: &
       matrix
  use timings_module
  use memory_handling, only: mem_dealloc
  use wannier_fmm, only: &
       wcalc_nucmom, &
       wfmm_multipole_expansion, &
       wfmm_form_tlattice, &
       pbc_mat_redefine_q, &
       pbc_multipl_moment_matorder
  use wannier_utils, only: &
       wu_indxtocoor, &
       wu_indxtolatvec, &
       wtranslate_molecule, &
       wcopy_atompositions
  use wannier_test_module
  use wannier_fockao
  use wannier_orthpot
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_dotproduct, &
       mat_abs_max_elm, &
       mat_trans, &
       mat_dsyev, &
       mat_add, &
       mat_print
  use integralinterfacemod, only: &
       ii_get_coulomb_mat, &
       ii_get_exchange_mat 
  use memory_handling, only: mem_alloc
  use scfloop_module, only: & 
       scf_init, scf_shutdown
  use configurationType
  use queue_module
  use queue_ops
  use av_utilities
  use wannier_complex_utils
  
  implicit none
  private
  public :: wannier_bandstructure_sample_kspace
contains

  !> @brief Trace a linear path between two k-points 
  !> @author Audun Skau Hansen
  !> @date 2015
  !> @param kpoint_begin Start point
  !> @param kpoint_end End point (not included in path)
  !> @param npoints Number of sample points in path
  subroutine wannier_bandstructure_get_k_path(kpoint_begin, kpoint_end,npoints,&
  & kpoint_path)

    real(realk), intent(in)       :: kpoint_begin(3), kpoint_end(3)
    integer, intent(in)           :: npoints
    real(realk), intent(inout), dimension(3, npoints) :: kpoint_path
    real(realk)                   :: delta_kpoint(3)
    integer                       :: i 
    do i = 1, 3
      delta_kpoint(i) = (kpoint_end(i)-kpoint_begin(i))/npoints
    enddo

    do i = 0, npoints -1 
      kpoint_path(:, i+1) = kpoint_begin + delta_kpoint*i
    enddo
  end subroutine wannier_bandstructure_get_k_path
  
  !> @brief Extract bandstructure from Wannier SCF orbitals.
  !> returns the bandstructure along linear paths between k-points
  !> @author Audun Skau Hansen
  !> @date 2015
  !> @param mos MO coefficients
  !> @param nbast Number of basis functions
  !> @param aoint AO integrals
  !> @param fmmt FMM tensor
  !> @param refcell Description of the reference cell
  !> @param lsset Information on the integral setttings
  !> @param wconf Wannier configutation
  !> @param timings Timings for the calculation
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  !> @param dens Density matrix in lattic
  !> @param kpoints A 3xN array containing N points in kspace
  !> @param kpath_resolution The # of intermediate samplings between kpoints
  subroutine wannier_bandstructure_sample_kspace(mos, nbast, aoint, fmmt, refcell, &
       & lsset, wconf, timings, lupri, luerr, dens, zmat, kpoints, kpath_resolution)
       
    integer, intent(in) :: nbast, lupri, luerr
    type(moleculeinfo), intent(in)      :: refcell
    type(lssetting), intent(inout)      :: lsset
    type(ao_integrals), intent(inout)   :: aoint
    type(fmm_tensors), intent(inout)    :: fmmt
    type(wannier_config), intent(inout) :: wconf
    type(timings_type), intent(inout)   :: timings
    type(lmatrixp), intent(inout)       :: mos, dens,zmat
    real(realk), intent(in) :: kpoints(:,:)
    
    type(lmatrixp)             :: fockmat
    integer                    :: i,j,indx, matsize
    type(lattice_domains_type) :: dom_ao
    type(matrix), pointer      :: ptr 
    integer     :: npoints, nkpoint, n_bvk(3)
    real(realk) :: k_begin(3)
    real(realk) :: k_end(3)
    integer, intent(in)      :: kpath_resolution !number of samples between kpoint
    real(realk), allocatable :: kpath(:,:) !path through kspace

    complex(complexk) :: fockmat_k(nbast, nbast) !, mos_k(nbast,nbast),&
                         !& tmp(nbast,nbast), epsilon_k(nbast, nbast)
    real(realk) :: bands_k(nbast)
   

    !wconf%blat !lattice vectors
    fockmat_k(:,:) = 0.0_complexk
    !mos_k(:,:) = 0.0_complexk
    !tmp(:,:) = 0.0_complexk
    bands_k(:) = 0.0_realk
    !epsilon_k(:,:) = 0.0_complexk
    npoints = size(kpoints, 2)

    nkpoint = 1
    n_bvk = wconf%n_bvk

    do i = 1, wconf%blat%dims
       nkpoint = nkpoint * n_bvk(i)
    enddo


    write(*,*) "#################################"
    write(*,*) "### Bandstructure Calculation ###"
    write(*,*) "#################################"
 

    allocate(kpath(3, kpath_resolution))
  

    call lts_init(fockmat, wconf%blat%dims, mos%cutoffs, nbast, nbast, &
         & .false., .false.)  ! todo set incl_redundant to false !! ??

    call w_calc_fockmat( refcell, dens, lsset, aoint, fmmt, wconf, &
         & timings, fockmat, zmat, lupri, luerr)

    call lts_print(fockmat, 6)

    !todo main loop over lattice to avoid
    !excessive memory lookups.
    do i = 1, npoints-1
      k_begin = kpoints(:, i)
      k_end = kpoints(:,i+1)
      !obtain the path between k_begin and k_end in k space
      call wannier_bandstructure_get_k_path(k_begin, k_end, kpath_resolution, kpath)
      do j = 1, kpath_resolution
        !calculate eigevalues for each point in k-space
        !epsilon_k(:,:) = 0.0_complexk
        write(*,*) (j-1) + (i-1)*kpath_resolution, kpath(:, j)
        call bloch_transform(fockmat,fockmat_k,kpath(:,j),nkpoint, nbast,wconf, lupri,luerr)


        call wannier_zheev(fockmat_k, nbast, bands_k)

        write(*,*) "bands:"
        write(*,*) bands_k
        write(*,*) " "
      enddo

      write(*,*) " "
    enddo
       

    deallocate(kpath) 

  end subroutine wannier_bandstructure_sample_kspace

  !> @brief A matrix transform for complex arrays
  subroutine wannier_unitary_transform(u_array, m_array, result_array, nbast)
    integer, intent(in) :: nbast
    complex(complexk), intent(in) :: u_array(nbast, nbast)
    complex(complexk), intent(inout) :: m_array(nbast, nbast),&
                                      & result_array(nbast, nbast)

    complex(complexk) :: tmp(nbast, nbast)
    tmp(:,:) = 0.0_complexk

    call wannier_complex_print(tmp, nbast)

    call wannier_complex_print(u_array, nbast)
    call wannier_complex_print(m_array, nbast)
    call zgemm('c','n',nbast,nbast,nbast,1.0_realk,u_array,nbast,m_array,nbast,&
                   & 0.0_realk,tmp,nbast)

    call zgemm('n','n',nbast,nbast,nbast,1.0_realk,tmp,nbast,u_array,nbast,&
                   & 0.0_realk,result_array,nbast)

  end subroutine wannier_unitary_transform


end module wannier_bandstructure_tools
