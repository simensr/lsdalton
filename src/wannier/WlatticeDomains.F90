!> @file WlatticeDomains.F90
!> Module for handeling the lattice domains. 
!>
!> Usage example:	
!> ...
!> type(bravais_lattice), intent(in) :: lat
!>
!>	type(lattice_domains_type) :: dom
!> integer :: max_layer
!>
!> max_layer = 10 !def how large domains can be calculated
!>
!>	call ltdom_init(dom, lat, max_layer)
!>
!> ! calculate lattice domain with 1 layer centered in (0,0,1)
!>	call ltdom_getdomain(dom, 1, (/0,0,1/))
!>
!> ! calculate intersection between lattice domains with 2 layers centered in 
!> ! (-1,0,0) and with 3 layers centered in (4,4,0)
!>	call ltdom_getintersect(dom, 2, (/-1,0,0/), 3, (/4,0,0/))
!>
!> ! Indices of lattice cells in domain
!> write (*, *) dom%indcs(1:dom%nelms)
!>
!>	call ltdom_delete(dom)

!TODO allocate -> mem_alloc
!TODO deallocate -> mem_dealloc

module wlattice_domains

!use memory_handling, only: mem_alloc, mem_dealloc
use wannier_utils, only: wu_coortoindx
use precision

implicit none
private
public :: &
	& ltdom_getlayer, lattice_domains_type, ltdom_getdomain, &
	& ltdom_getintersect, ltdom_init, ltdom_free, ltdom_getnext, ltdom_contains

!> @brief Type to store information about a lattice domain.
type lattice_domains_type
	!> Indices of the cells in a given domain.
	integer, pointer :: indcs(:)
	!> Number of elms - size(indcs).
	integer :: nelms
	!> Largest index of the stored cells.
	integer :: max_indx
	!> Largest layer of stored cells
	integer :: max_layer
	!> Definition of the bravais lattice. Needed to calculate the domains.
	integer :: dims
	!> To use with iteratior getnext. Should be a private variable.
	integer :: iter
end type

contains

! Usage: 
! call ltdom_getdomain(dom, ...)
! !The integer i is the value of the cell)
! do while(ltdom_getnext(dom, i)
! 	! ... do stuff with i
! enddo
logical function ltdom_getnext(this, i)
	type(lattice_domains_type), intent(inout) :: this
	integer, intent(out) :: i
	if (this%iter == this%nelms + 1) then
		ltdom_getnext = .false.
		return
	else
		i = this%indcs(this%iter)
		this%iter = this%iter + 1
		ltdom_getnext = .true.
		return
	endif
end function

!> @brief Init lattice_domains_type, allocate memory ect.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine ltdom_init(dom, dims, max_layer)
	type(lattice_domains_type), intent(inout) :: dom
	integer, intent(in) :: dims, max_layer

	integer :: ierr
	!should be possible to set this
	dom%max_layer = max_layer
	dom%max_indx = (2*max_layer + 1)**dims
	!todo if symm etc max_indx/2 + 1
	!fixme call mem_alloc(dom%indcs, dom%max_indx)
	nullify(dom%indcs)
	allocate(dom%indcs(dom%max_indx), stat=ierr)
	if (ierr /= 0) then
		call lsquit('Mem. alloc in sr: ltdom_init, mod: wlattice_domain')
	endif
	dom%dims = dims           
end subroutine

!> @brief Destruct type. Dealloc mem etc.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine ltdom_free(dom)
	type(lattice_domains_type), intent(inout) :: dom

	!fixme call mem_dealloc(dom%indcs)
	deallocate(dom%indcs)
end subroutine

!> @brief Check if the (lattice cell) indx is in a domain (this)
!> @param this domains type
!> @param indx integer index
!> @date 2015
!> @author Karl R. Leikanger
logical pure function ltdom_contains(this, indx)
	type(lattice_domains_type), intent(in) :: this
	integer, intent(in) :: indx

	ltdom_contains = any(this%indcs == indx)
end function


!> @brief Get the indices of all cells in a layer. NB: refcell is layer 0.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine ltdom_getlayer(dom, layer, incl_redundant)
	integer, intent(in) :: layer
	type(lattice_domains_type), intent(inout) :: dom
	logical, intent(in), optional :: incl_redundant

	logical :: incl_red

	if (present(incl_redundant)) then
		incl_red = incl_redundant
	else
		incl_red = .true.
	endif

	if (layer > dom%max_layer) then
		call lsquit('sr: ltdom_getlayer, mod:wlattice_domains, &
			& layer > dom%max_layer', -1)
	endif

	select case (dom%dims)
		case (3)
			call getlayer3d(dom, incl_red, layer)
		case (2)
			call getlayer2d(dom, incl_red, layer)
		case (1)
			call getlayer1d(dom, incl_red, layer)
		case default
			call lsquit('mod: wlattice_domains sr: ltdom_getlayer wannier_types%dims > 3', -1)
	end select

	dom%iter = 1
end subroutine

!> @brief Get the indices of all cells in a layer.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine getlayer1d(dom, incl_redundant_elms, layer)
  integer, intent(in) :: layer
  logical, intent(in) :: incl_redundant_elms
  type(lattice_domains_type), intent(inout) :: dom
  
  integer :: nelem, indx, i, coor(3)
  
  if (layer == 0) then

     coor = [0,0,0]
     dom%indcs(1) = wu_coortoindx(coor)
     dom%nelms = 1
     return
  endif
  
  nelem = 1
  indx = 2*layer - 1
  do i = -layer, layer, 2*layer
     coor = [i,0,0]
     indx = wu_coortoindx(coor)
     
     if (.not. incl_redundant_elms) then
        if (indx < 0) then 
           cycle
        endif
     endif
     
     dom%indcs(nelem) = indx
     nelem = nelem + 1
  enddo

  dom%nelms = nelem - 1
  
end subroutine getlayer1d

!> @brief Get the indices of all cells in a layer.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine getlayer2d(dom, incl_redundant_elms, layer)
  integer, intent(in) :: layer
  logical, intent(in) :: incl_redundant_elms
  type(lattice_domains_type), intent(inout) :: dom
  
  integer :: i, j, nelem, indx, isteps, coor(3)
  
  if (layer == 0) then
     coor = [0,0,0]
     dom%indcs(1) = wu_coortoindx(coor)
     dom%nelms = 1
     return
  endif
  
  nelem = 1
  do j = -layer, layer
     if (abs(j)==layer) then 
        isteps = 1
     else 
        isteps = 2*layer
     endif
     do i = -layer, layer, isteps
        coor = [i,j,0]
        indx = wu_coortoindx(coor)
        
        if (.not. incl_redundant_elms) then
           if (indx < 0) then 
              cycle
           endif
        endif
        
        dom%indcs(nelem) = indx
        nelem = nelem + 1
     enddo
  enddo
  
  dom%nelms = nelem - 1
end subroutine

!> @brief Get the indices of all cells in a layer.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine getlayer3d(dom, incl_redundant_elms, layer)
  integer, intent(in) :: layer
  logical, intent(in) :: incl_redundant_elms
  type(lattice_domains_type), intent(inout) :: dom
  
  integer :: i, j, k, nelem, indx, isteps,coor(3)

 if (layer == 0) then
    coor = [0,0,0]
    dom%indcs(1) = wu_coortoindx(coor)
    dom%nelms = 1
    return
 endif

 ! first_elem in layer
 nelem = 1
 do k = -layer, layer
    do j = -layer, layer
       if (abs(k)==layer .or. abs(j)==layer) then
          isteps = 1
       else
          isteps = 2*layer
       endif
       do i = -layer, layer, isteps
          coor = [i,j,k]
          indx = wu_coortoindx(coor)
          
          if (.not. incl_redundant_elms) then
             if (indx < 0) then 
                cycle
             endif
          endif
          
          dom%indcs(nelem) = indx	
          nelem = nelem + 1
          
       end do
    end do
 end do
 dom%nelms = nelem - 1
end subroutine

!> @brief
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine ltdom_getdomain(dom, cutoffs, translation, incl_redundant)
  integer, intent(in) :: cutoffs(3)
  type(lattice_domains_type), intent(inout) :: dom
  integer, optional :: translation(3)
  logical, intent(in), optional :: incl_redundant
  
  integer :: i, j, k, nelem, transl(3), indx, coor(3)
  logical :: incl_red

  if (present(incl_redundant)) then
     incl_red = incl_redundant
  else
     incl_red = .true.
  endif
  
  if (present(translation)) then 
     transl = translation
     if (any(transl(1+dom%dims:3) /= 0)) then
        call lsquit('sr: get_domain, mod: wlattice_domains,&
             & translation vector nonzero along nonperiodic direction', -1)
     endif
  else 
     transl = [0, 0, 0]
  endif
 
 !todo necc?
  if (any(cutoffs(1+dom%dims:3) /= 0)) then
     call lsquit('sr: get_domain, mod: wlattice_domains,&
          & cutoff nonzero along nonperiodic direction', -1)
  endif
  
  nelem = 1
  do k = - cutoffs(3), cutoffs(3)
     do j = - cutoffs(2), cutoffs(2)
        do i = - cutoffs(1), cutoffs(1)
           coor = [i,j,k] + transl
           indx = wu_coortoindx(coor) 
          if ((.not. incl_red) .and. (indx < 0)) cycle
          dom%indcs(nelem) = indx
          nelem = nelem + 1
       enddo
    enddo
 enddo
 dom%nelms = nelem - 1
 
 dom%iter = 1
end subroutine

!!> @brief
!!> @param
!!> @param
!!> @param
!!> @param
!!> @date
!!> @author
!subroutine ltdom_getdomain(dom, max_layer, transl, incl_redundant_elms_inp)
!	integer, intent(in) :: max_layer, transl(3)
!	logical, intent(in), optional :: incl_redundant_elms_inp
!	type(lattice_domains_type), intent(inout) :: dom
!
!	logical :: incl_redundant_elms
!
!	if (present(incl_redundant_elms_inp)) then
!		incl_redundant_elms = incl_redundant_elms_inp
!		if (.not. incl_redundant_elms_inp) then
!			call lsquit('sr: ltdom_getdomain, mod: wlattice_domains, &
!				& req. func. not yet implemented', -1)
!		endif
!	else
!		incl_redundant_elms = .true.
!	endif
!	
!	select case (dom%dims)
!		case (3)
!			call getdom3d(dom, max_layer, transl, incl_redundant_elms)
!		case (2)
!			call getdom2d(dom, max_layer, transl, incl_redundant_elms)
!		case (1)
!			call getdom1d(dom, max_layer, transl, incl_redundant_elms)
!		case default
!			call lsquit('sr: ltdom_getdomain, mod: wlattice_domains, \
!				wannier_types%dims > 3', -1)
!	end select
!
!end subroutine
!
!!> @brief
!!> @param
!!> @param
!!> @param
!!> @param
!!> @date
!!> @author
!subroutine	getdom1d(dom, max_layer, transl, incl_redundant_elms)
!	integer, intent(in) :: max_layer, transl(3)
!	logical, intent(in) :: incl_redundant_elms
!	type(lattice_domains_type), intent(inout) :: dom
!
!	integer :: coor(3), layer, i, j, nelem
!
!	! a translation along a non-periodic direction does not make sense.
!	if ((transl(2) /= 0) .or. (transl(3) /= 0 )) then
!		call lsquit('sr: getdom1d, mod: wlattice_domains, &
!			& translation vector nonzero along nonperiodic direction', -1)
!	endif
!
!	coor(:) = transl(:)
!	dom%indcs(1) = wu_coortoindx(coor) 
!
!	nelem = 2
!	do layer = 1, max_layer
!		do i = -layer, layer, 2*layer
!			coor(:) = transl + [i, 0, 0]
!			dom%indcs(nelem) = wu_coortoindx(coor) 
!			nelem = nelem + 1
!		enddo
!	enddo
!	dom%nelms = nelem - 1
!
!end subroutine
!
!!> @brief
!!> @param
!!> @param
!!> @param
!!> @param
!!> @date
!!> @author
!subroutine	getdom2d(dom, max_layer, transl, incl_redundant_elms)
!	integer, intent(in) :: max_layer, transl(3)
!	logical, intent(in) :: incl_redundant_elms
!	type(lattice_domains_type), intent(inout) :: dom
!
!	integer :: i, j, k, nelem, isteps, layer, coor(3)
!
!	if ( transl(3) /= 0) then
!		call lsquit('sr: getdom2d, mod: wlattice_domains, &
!			& translation vector nonzero along nonperiodic direction', -1)
!	endif
!
!	coor(:) = transl(:)
!	dom%indcs(1) = wu_coortoindx(coor) 
!
!	nelem = 2
!	do layer = 1, max_layer
!		do j = -layer, layer
!			if (abs(j)==layer) then 
!				isteps = 1
!			else 
!				isteps = 2*layer
!			endif
!			do i = -layer, layer, isteps
!				coor(:) = [i, j, 0] + transl(:)
!				dom%indcs(nelem) = wu_coortoindx(coor) 
!				nelem = nelem + 1
!			end do
!		end do
!	end do
!
!	dom%nelms = nelem - 1
!end subroutine
!
!!> @brief
!!> @param
!!> @param
!!> @param
!!> @param
!!> @date
!!> @author
!subroutine	getdom3d(dom, max_layer, transl, incl_redundant_elms)
!	integer, intent(in) :: max_layer, transl(3)
!	logical, intent(in) :: incl_redundant_elms
!	type(lattice_domains_type), intent(inout) :: dom
!
!	integer :: i, j, k, nelem, isteps, layer, coor(3)
!
!	coor(:) = transl(:)
!	dom%indcs(1) = wu_coortoindx(coor) 
!
!	nelem = 2
!	do layer = 1, max_layer
!		do k = -layer, layer
!			do j = -layer, layer
!				if (abs(k)==layer .or. abs(j)==layer) then
!					isteps = 1
!				else
!					isteps = 2*layer
!				endif
!				do i = -layer, layer, isteps
!					coor(:) = [i, j, k] + transl(:)
!					dom%indcs(nelem) = wu_coortoindx(coor) 
!					nelem = nelem + 1
!				end do
!			end do
!		end do
!	end do
!
!	dom%nelms = nelem - 1
!end subroutine

!> @brief Get the intersection between two domains with centers in 
!> transl1 and transl2, and cutoff1/cutoff2 layers in the directions
!> of the primitive vectors.
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine ltdom_getintersect(dom, cutoff1, transl1, cutoff2, transl2, &
		& incl_redundant)
	integer, intent(in) :: cutoff1(3), cutoff2(3), transl1(3), transl2(3)
	logical, intent(in), optional :: incl_redundant
	type(lattice_domains_type), intent(inout) :: dom

	logical :: incl_redundant_elms

	if (present(incl_redundant)) then
		incl_redundant_elms = incl_redundant
		if (.not. incl_redundant_elms) then
			call lsquit('sr: ltdom_getintersect, mod: wlattice_domains, &
				& req. func. not yet implemented', -1)
		endif
	else
		incl_redundant_elms = .true.
	endif

	! translations/cutoffs along non-periodic directions does not make sense.
	select case (dom%dims)
	case (2)
		if ((transl1(3) /= 0) .or. (transl2(3) /= 0)) then
			call lsquit('sr: ltdom_getintersect, mod: wlattice_domains, &
				& translation vector nonzero along nonperiodic direction', -1)
		endif
		if ((cutoff1(3) /= 0) .or. (cutoff2(3) /= 0)) then
			call lsquit('sr: ltdom_getintersect, mod: wlattice_domains, &
				& cutoff vector nonzero along nonperiodic direction', -1)
		endif
	case (1)
		if ( (transl1(2) /= 0) .or. (transl1(3) /= 0 ) .or. &
			& (transl2(2) /= 0) .or. (transl2(3) /= 0 ) ) then
			call lsquit('sr: ltdom_getintersect, mod: wlattice_domains, &
				& translation vector nonzero along nonperiodic direction', -1)
		endif
		if ( (cutoff1(2) /= 0) .or. (cutoff1(3) /= 0 ) .or. &
			& (cutoff2(2) /= 0) .or. (cutoff2(3) /= 0 ) ) then
			call lsquit('sr: ltdom_getintersect, mod: wlattice_domains, &
				& translation vector nonzero along nonperiodic direction', -1)
		endif
	end select

	call getintrsct(dom, cutoff1, transl1, cutoff2, transl2, &
		& incl_redundant_elms)

	dom%iter = 1
	! check if max elem is larger than dom%max_indx
	! in this case the cell is not stored.
end subroutine

!> @brief Get intersection. Note that for 2D or 3D grids the second and/or 
!> third element in the cutoff and translation - vectors are 0. 
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine getintrsct(dom, cutoff1, transl1, cutoff2, transl2, &
		& incl_redundant_elms_inp)
	integer, intent(in) :: cutoff1(3), cutoff2(3), transl1(3), transl2(3)
	logical, intent(in) :: incl_redundant_elms_inp
	type(lattice_domains_type), intent(inout) :: dom

	integer :: &
		& max_x1, max_x2, min_x1, min_x2, &
		& max_y1, max_y2, min_y1, min_y2, &
		& max_z1, max_z2, min_z1, min_z2, &
		& loopmin_x, loopmax_x, &
		& loopmin_y, loopmax_y, &
		& loopmin_z, loopmax_z, &
		& nelem, i, j, k, coor(3)

	max_x1 = cutoff1(1) + transl1(1)
	min_x1 = - cutoff1(1) + transl1(1)
	max_x2 = cutoff2(1) + transl2(1)
	min_x2 = - cutoff2(1) + transl2(1)
	
	max_y1 = cutoff1(2) + transl1(2)
	min_y1 = - cutoff1(2) + transl1(2)
	max_y2 = cutoff2(2) + transl2(2)
	min_y2 = - cutoff2(2) + transl2(2)

	max_z1 = cutoff1(3) + transl1(3)
	min_z1 = - cutoff1(3) + transl1(3)
	max_z2 = cutoff2(3) + transl2(3)
	min_z2 = - cutoff2(3) + transl2(3)

	loopmin_x = max(min_x1, min_x2)
	loopmax_x = min(max_x1, max_x2)
	loopmin_y = max(min_y1, min_y2)
	loopmax_y = min(max_y1, max_y2)
	loopmin_z = max(min_z1, min_z2)
	loopmax_z = min(max_z1, max_z2)

 nelem = 1
 do i = loopmin_x, loopmax_x
    do j = loopmin_y, loopmax_y
       do k = loopmin_z, loopmax_z
          coor = [i,j,k]
          dom%indcs(nelem) = wu_coortoindx(coor) 
          nelem = nelem + 1
       enddo
    enddo
 enddo
 dom%nelms = nelem - 1
 
end subroutine getintrsct


!--------------------------------------------------
!
! DELETE stuff below
!
!--------------------------------------------------
!> @brief
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine getintrsct1d(dom, max_layer1, transl1, max_layer2, transl2, &
		& incl_redundant_elms_inp)
	integer, intent(in) :: max_layer1, max_layer2, transl1(3), transl2(3)
	type(lattice_domains_type), intent(inout) :: dom
	logical, intent(in) :: incl_redundant_elms_inp

	integer :: max_1, max_2, min_1, min_2, loopmin, loopmax, nelem, coor(3), i

	! a translation along a non-periodic direction does not make sense.
	if ( (transl1(2) /= 0) .or. (transl1(3) /= 0 ) .or. &
		& (transl2(2) /= 0) .or. (transl2(3) /= 0 ) ) then
		call lsquit('sr: getintrsct1d, mod: wlattice_domains, &
			& translation vector nonzero along nonperiodic direction', -1)
	endif

	max_1 = max_layer1 + transl1(1)
	min_1 = - max_layer1 + transl1(1)
	max_2 = max_layer2 + transl2(1)
	min_2 = - max_layer2 + transl2(1)

	loopmin = max(min_1, min_2)
	loopmax = min(max_1, max_2)

	nelem = 1
	coor(:) = 0
	do i = loopmin, loopmax
		coor(1) =  i
		dom%indcs(nelem) = wu_coortoindx(coor) 
		nelem = nelem + 1
	enddo
	dom%nelms = nelem - 1

end subroutine

!> @brief
!> @param
!> @param
!> @param
!> @param
!> @date
!> @author
subroutine getintrsct2d(dom, max_layer1, transl1, max_layer2, transl2, &
     & incl_redundant_elms_inp)
  integer, intent(in) :: max_layer1, max_layer2, transl1(3), transl2(3)
  logical, intent(in) :: incl_redundant_elms_inp
  type(lattice_domains_type), intent(inout) :: dom

  integer :: &
       & max_x1, max_x2, min_x1, min_x2, &
       & max_y1, max_y2, min_y1, min_y2, &
       & loopmin_x, loopmax_x, &
       & loopmin_y, loopmax_y, &
       & nelem, i, j, k, coor(3)
  
  k = abs(transl1(3)) + abs(transl2(3))
  if (k /= 0) then
     call lsquit('sr: getintrsct2d, mod: wlattice_domains, &
          & translation vector nonzero along nonperiodic direction', -1)
  endif
  
  max_x1 = max_layer1 + transl1(1)
  min_x1 = - max_layer1 + transl1(1)
  max_x2 = max_layer2 + transl2(1)
  min_x2 = - max_layer2 + transl2(1)
  
  max_y1 = max_layer1 + transl1(2)
  min_y1 = - max_layer1 + transl1(2)
  max_y2 = max_layer2 + transl2(2)
  min_y2 = - max_layer2 + transl2(2)
  
  loopmin_x = max(min_x1, min_x2)
  loopmax_x = min(max_x1, max_x2)
  loopmin_y = max(min_y1, min_y2)
  loopmax_y = min(max_y1, max_y2)
  
  nelem = 1
  do i = loopmin_x, loopmax_x
     do j = loopmin_y, loopmax_y
        coor = [i,j,0]
        dom%indcs(nelem) = wu_coortoindx(coor) 
        nelem = nelem + 1
     enddo
  enddo
  dom%nelms = nelem - 1

end subroutine

!> @brief Sorts an integer array arr of length N in ascending order 
!> by the Heapsort method. Nlog2N method.          
!> @param ra Array to be sorted.
!> @param n Num. of elements in ra.
!> @date 2015
!> @author KRL
subroutine heapsort(arr, n)
	integer, pointer, intent(inout) :: arr(:)
	integer, intent(in) :: n

	integer :: l, ir, i, j, rarr

	l = n/2 +1
	ir = n
	do
		if (l > 1) then
			l = l - 1
			rarr = arr(l)
		else
			rarr = arr(ir)
			arr(ir) = arr(1)
			ir = ir - 1
			if (ir == 1) then
				arr(1) = rarr
				return
			endif
		endif
		i = l
		j = l + l

		do while (j <= ir)
			if (j < ir) then
				if (arr(j) < arr(j+1))  j=j+1
			endif
			if (rarr < arr(j)) then
				arr(i) = arr(j)
				i=j
				j=j+j
			else
				j = ir + 1
			endif
		enddo
		arr(i) = rarr
	enddo
end subroutine


end module
