!> @file
!> Module containing soubroutines for settting up input for the Thermite driver
MODULE Integralinfo
use precision
use integral_type
!use typedef
use typedeftype

private

public :: init_integral_INPUT

CONTAINS
!> \brief initiate the INTEGRALINPUT structure 
!> \author T. Kjaergaard and S. Reine
!> \date 2009 
!>
!> This routine initiate the INTEGRALINPUT structure and should always be 
!> call before the MAIN_INTEGRAL_DRIVER is called. In most cases the 
!> values are set equal to values contained in the setting%scheme
!> but other values are set to the default values and can then be changed
!> after this call
!>
SUBROUTINE init_integral_INPUT(INTINPUT,SETTING)
implicit none
!> the INTEGRALINPUT to initiate
TYPE(INTEGRALINPUT) :: INTINPUT
!> contains info about thresholds and integral evaluation schemes 
TYPE(LSSETTING)   :: SETTING
!
REAL(REALK) :: TMP1
INTEGER :: TMP2
REAL(REALK) :: TEN=1E1_realk
real(realk),parameter :: D0=0.0E0

!Nullify(INTINPUT%DMAT_LHS)
!Nullify(INTINPUT%DMAT_RHS)
Nullify(INTINPUT%AO(1)%p)
Nullify(INTINPUT%AO(2)%p)
Nullify(INTINPUT%AO(3)%p)
Nullify(INTINPUT%AO(4)%p)
INTINPUT%noOMP = SETTING%SCHEME%noOMP
INTINPUT%exchangeFactor = SETTING%SCHEME%exchangeFactor
INTINPUT%HIGH_RJ000_ACCURACY = SETTING%SCHEME%HIGH_RJ000_ACCURACY
INTINPUT%currentFragment=1
INTINPUT%NDMAT_LHS=0
INTINPUT%NDMAT_RHS=0
INTINPUT%DRHS_SYM=.FALSE.
INTINPUT%DLHS_SYM=.FALSE.
INTINPUT%sameODs=.FALSE.
INTINPUT%sameRHSaos=.FALSE.
INTINPUT%sameLHSaos=.FALSE.
!INTINPUT%RHS_DMAT=.FALSE.
!INTINPUT%LHS_DMAT=.FALSE.
INTINPUT%CENTERS=0
INTINPUT%ndim=0
INTINPUT%DO_PASSES=SETTING%SCHEME%DOPASS
INTINPUT%ContAng=SETTING%SCHEME%ContAng
INTINPUT%sphericalEcoeff=.NOT.SETTING%SCHEME%nonSphericalETUV
INTINPUT%maxpasses=SETTING%SCHEME%maxpasses
INTINPUT%DO_FOCK=.FALSE.  !Only used in DistributeIntegrals for branching out to the proper distribution routine
INTINPUT%DO_COULOMB=.FALSE.
INTINPUT%DO_EXCHANGE=.FALSE.
INTINPUT%DO_ENERGY=.FALSE.
INTINPUT%DO_JENGINE=.FALSE.
INTINPUT%DO_DAJENGINE=.FALSE.
INTINPUT%DO_DACOULOMB=.FALSE.
INTINPUT%DO_LINK=.FALSE.
INTINPUT%DO_DALINK=.FALSE.
INTINPUT%DASCREEN_THRLOG=SETTING%SCHEME%DASCREEN_THRLOG
INTINPUT%DO_FMM=.FALSE.
INTINPUT%DO_MULMOM=.FALSE.
INTINPUT%LU_MMDATA=-1
INTINPUT%LU_MMDATR=-1
INTINPUT%MMUnique_iD1 = 0
INTINPUT%MMUnique_iD2 = 0
INTINPUT%MMstartA = 0
INTINPUT%MMstartB = 0
INTINPUT%MMindex = 0
! Screen integrals based on non-classical extent
INTINPUT%NonClassical_SCREEN=.FALSE.
!!!!!!!!INTINPUT%FMM_lmaxLocal=0
!!!!!!!!INTINPUT%FMM_lmaxTranslationFarField=0
!!!!!!!!INTINPUT%FMM_screenThreshold=0.0E0_realk
INTINPUT%MM_SCREENTHR = SETTING%SCHEME%MM_SCREEN*SETTING%SCHEME%intTHRESHOLD
INTINPUT%MM_NOONE = SETTING%SCHEME%MM_NO_ONE
INTINPUT%MM_TLMAX = SETTING%SCHEME%MM_TLMAX
INTINPUT%MM_LMAX  = SETTING%SCHEME%MM_LMAX
INTINPUT%MM_NOSCREEN = SETTING%SCHEME%MM_NOSCREEN
INTINPUT%DO_MMGRD    = SETTING%SCHEME%DO_MMGRD
!Specifies calculation of CS-integrals
INTINPUT%CS_int=SETTING%SCHEME%CS_INT
!Specifies calculation of PS-integrals
INTINPUT%PS_int=SETTING%SCHEME%PS_INT
!Specifies usage of CS-screening
INTINPUT%CS_SCREEN=.FALSE.
INTINPUT%CS_THRESHOLD=SETTING%SCHEME%CS_THRESHOLD*SETTING%SCHEME%intTHRESHOLD
!Beware when converting from double precision to short integer 
!If double precision is less than 10^-33 then you can run into
!problems with short integer overflow
IF (INTINPUT%CS_THRESHOLD.LT.shortIntCrit) THEN
   print*,'WARNING MINIMUM THRESHOLD IN INTEGRALCODE IS ',TEN**shortzero
   INTINPUT%CS_THRESHOLD=shortintcrit
ENDIF
TMP1 = log10(INTINPUT%CS_THRESHOLD)
TMP2 = NINT(TMP1)
IF (ABS(TMP1-TMP2).LT. 1E-5_realk) THEN
   !this means that the threshold is 1EX_realk X=-1,..,-19,..
   INTINPUT%CS_THRLOG=TMP2
ELSE
   INTINPUT%CS_THRLOG=FLOOR(log10(INTINPUT%CS_THRESHOLD))
ENDIF
! PRIMITIVE SCREENING
INTINPUT%PS_SCREEN=.FALSE.
INTINPUT%PS_THRESHOLD=SETTING%SCHEME%PS_THRESHOLD*SETTING%SCHEME%intTHRESHOLD
!Beware when converting from double precision to short integer 
!If double precision is less than 10^-33 then you can run into
!problems with short integer overflow
IF (INTINPUT%PS_THRESHOLD.LT.shortintCrit) THEN
   print*,'WARNING MINIMUM THRESHOLD IN INTEGRALCODE IS ',TEN**shortzero
   INTINPUT%PS_THRESHOLD=shortintcrit
ENDIF
TMP1 = log10(INTINPUT%PS_THRESHOLD)
TMP2 = NINT(TMP1)
IF (ABS(TMP1-TMP2).LT.1.0E-15) THEN
   !this means that the threshold is 1EX_realk X=-1,..,-19,..
   INTINPUT%PS_THRLOG=TMP2
ELSE
   INTINPUT%PS_THRLOG=FLOOR(log10(INTINPUT%PS_THRESHOLD))
ENDIF
! Screen OD-batches based on AO-batch extent
INTINPUT%OD_SCREEN=.FALSE.
INTINPUT%OD_THRESHOLD=SETTING%SCHEME%OD_THRESHOLD*SETTING%SCHEME%intTHRESHOLD
!Multipole Based Integral Estimate Screening
INTINPUT%MBIE_SCREEN=.FALSE.
INTINPUT%MBIE_INT=.FALSE.
!Distance-dependent Schwarz-based integral estimate screening 
INTINPUT%QQR_SCREEN=.FALSE.
!Specifies overlap integral screening based on OD-batch extents
INTINPUT%OE_SCREEN=.FALSE.
INTINPUT%OE_THRESHOLD=SETTING%SCHEME%OE_THRESHOLD*SETTING%SCHEME%intTHRESHOLD
INTINPUT%AC_center = .FALSE.
!Derivative info
! First the number of components 
INTINPUT%NGeoDerivComp  = 1
INTINPUT%NGeoDerivCompP = 1
INTINPUT%NGeoDerivCompQ = 1
INTINPUT%NmagDerivCompP = 1
INTINPUT%NmagDerivCompQ = 1
INTINPUT%nMultipoleMomentComp = 1
INTINPUT%nCartesianMomentComp = 1
! Second the order of derivative 
INTINPUT%GeoderOrderP = 0
INTINPUT%GeoderOrderQ = 0
INTINPUT%MagderOrderP = 0
INTINPUT%MagderOrderQ = 0
INTINPUT%doMagScreen = .FALSE.
INTINPUT%MMorder  = 0
INTINPUT%CMstart  = 0
INTINPUT%CMorder  = 0
INTINPUT%CMimat   = 0
INTINPUT%GEODERIVORDER = 0
INTINPUT%MAGDERIVORDER = 0

INTINPUT%AddToIntegral = .FALSE.
INTINPUT%OD_MOM = SETTING%SCHEME%OD_MOM
INTINPUT%MOM_CENTER = SETTING%SCHEME%MOM_CENTER
INTINPUT%DO_GRADIENT=.FALSE.
!Default factor for closed shells
INTINPUT%CoulombFactor = 2.0E0_realk
INTINPUT%ATTomega = SETTING%SCHEME%CAMmu
INTINPUT%ATTalpha = SETTING%SCHEME%CAMalpha 
INTINPUT%ATTbeta = SETTING%SCHEME%CAMbeta
INTINPUT%ATTFACTOR = .FALSE.
INTINPUT%FTUVmaxprim = SETTING%SCHEME%FTUVmaxprim
INTINPUT%uselst_DRHS = .FALSE.
INTINPUT%uselst_DLHS = .FALSE.
INTINPUT%GGem = SETTING%GGem
IF (setting%lstensor_attached) THEN
  INTINPUT%lst_dLHS => setting%lst_dLHS
  INTINPUT%lst_dRHS => setting%lst_dRHS
ELSE
  NULLIFY(INTINPUT%LST_DLHS)
  NULLIFY(INTINPUT%LST_DRHS)
ENDIF
!Gab
NULLIFY(INTINPUT%LST_GAB_LHS)
NULLIFY(INTINPUT%LST_GAB_RHS)
INTINPUT%PS_MAXELM_LHS = shortzero
INTINPUT%PS_MAXELM_RHS = shortzero
INTINPUT%CS_MAXELM_LHS = shortzero
INTINPUT%CS_MAXELM_RHS = shortzero
INTINPUT%GAB_LHSusePointer=.FALSE.
INTINPUT%GAB_RHSusePointer=.FALSE.
INTINPUT%iPQXYZ=0 
INTINPUT%HermiteEcoeff = SETTING%SCHEME%HermiteEcoeff
INTINPUT%LinComCarmomType = 0
! Property Integral input
INTINPUT%DO_PROP=SETTING%SCHEME%DO_PROP
INTINPUT%PROPTYPE=-173
INTINPUT%PROP_ORIGIN(1) = D0
INTINPUT%PROP_ORIGIN(2) = D0
INTINPUT%PROP_ORIGIN(3) = D0
INTINPUT%PropDerivEcoeff = .TRUE.
INTINPUT%PropKineticEcoeff = .FALSE.
INTINPUT%PropMomentEcoeff =.TRUE.
INTINPUT%PropMaxD = 0
INTINPUT%PropMaxM = 0
INTINPUT%PropRequireBoys = -1
INTINPUT%fullcontraction = .FALSE.
INTINPUT%node = setting%node
INTINPUT%numnodes = setting%numnodes
INTINPUT%LHSSameAsRHSDmat = setting%LHSSameAsRHSDmat
INTINPUT%RealGabMatrix = setting%Output%RealGabMatrix
END SUBROUTINE init_integral_INPUT

END MODULE INTEGRALINFO
