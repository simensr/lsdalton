!> @file 
!> contains many structure and associated subroutine
MODULE GCtransMod
 use files
 use precision
 use matrix_module
 use typedeftype
 use matrix_operations
 use matrix_operations_scalapack
 use memory_handling
 use basis_typetype
 use infpar_module
private
!STILL NEED TO GET UNRES WORKING AND TEST THE SCALAPACK THING (test without scalapack)
public :: init_AO2GCAO_GCAO2AO, free_AO2GCAO_GCAO2AO, &
     & AO2GCAO_transform_matrixF, AO2GCAO_half_transform_matrix, &
     & GCAO2AO_half_transform_matrix, &!GCAO2AO_transform_matrixD, &
     & GCAO2AO_transform_matrixD2, AO2GCAO_transform_matrixD, &
     & AO2GCAO_transform_fullF, GCAO2AO_transform_fullD, &
!     & GCAO2AO_transform_fullF, AO2GCAO_transform_fullD, &
     & write_GCtransformationmatrix, GCAO2AO_half_transform_matrixfull,&
     & RemoveLinearDependenciesOfBasisGCAO,&
     & AO2GCAO_transform_fullF2
type(matrix),save :: GCAOtrans
type(matrix),save :: GCAOtrans_inv
logical :: GCbuild

Contains

!===============================================================================

! Subroutines that init/free/build/saves/recovers the transform matrices

!===============================================================================

subroutine init_AO2GCAO_GCAO2AO()
GCbuild = .FALSE.
call mat_nullify(GCAOtrans)
call mat_nullify(GCAOtrans_inv)
end subroutine INIT_AO2GCAO_GCAO2AO

subroutine free_AO2GCAO_GCAO2AO()
  IF(GCbuild)THEN
     call mat_free(GCAOtrans)
     call mat_free(GCAOtrans_inv)
  ENDIF
end subroutine free_AO2GCAO_GCAO2AO

!> \brief Build AO,GCAO transformation matrix CC(AO,GCAO)
!> \author T. Kjaergaard
!> \date 2010
!> \param CCfull the transformation matrix to be generated
!> \param nbast the dimension, the number of basis functions
!> \param setting the setting structure
!> \param lupri the logical unit number for output
SUBROUTINE build_GCtransformationmatrix(CC,nbast,setting,lupri)
implicit none
integer,intent(in)   :: lupri
integer,intent(in)   :: nbast
TYPE(LSSETTING) :: setting
TYPE(MATRIX)    :: CC
!
integer :: nbast1,R1,I,ICHARGE,type,iang,norb,II,JJ,isp
integer :: nOrbComp,cc1,cc2,ccJJ,ccII
real(realk),parameter :: D0=0E0_realk
real(realk),pointer :: TMP(:,:),TMPss(:,:)
real(realk) :: TMPs
logical :: matrix_exsist

!this could be built directly into a coordinate form and then transformed into CSR !
!so introduced special case for CSR 
call mat_zero(CC)
nbast1 = 0
R1 = setting%BASIS(1)%p%BINFO(GCTBasParam)%Labelindex
DO I=1,setting%MOLECULE(1)%p%natoms   
   IF(setting%MOLECULE(1)%p%ATOM(I)%pointcharge)cycle
   IF(R1.EQ.0)THEN
      ICHARGE = INT(setting%MOLECULE(1)%p%ATOM(I)%CHARGE)      
      type = setting%BASIS(1)%p%BINFO(GCTBasParam)%CHARGEINDEX(ICHARGE)
   ELSE
      type = setting%MOLECULE(1)%p%ATOM(I)%IDtype(R1)
   ENDIF
   do iang = 1,setting%BASIS(1)%p%BINFO(GCTBasParam)%ATOMTYPE(type)%nAngmom
      norb = setting%BASIS(1)%p%BINFO(GCTBasParam)%ATOMTYPE(type)%SHELL(iang)%segment(1)%ncol
      nOrbComp = (2*(iang-1))+1   !not true if cartesian
      call mem_alloc(TMP,norb,norb)
      DO JJ=1,norb
         DO II=1,norb
            TMP(II,JJ)=&
                 &setting%BASIS(1)%p%BINFO(GCTBasParam)%ATOMTYPE(type)%SHELL(iang)%segment(1)%elms(II+(JJ-1)*norb)
         ENDDO
      ENDDO
      !Build Block 
      call mem_alloc(TMPss,norb*nOrbComp,norb*nOrbComp)
      CALL LS_DZERO(TMPss,norb*nOrbComp*norb*nOrbComp)
      IF(Setting%BASIS(1)%p%BINFO(RegBasParam)%SPHERICAL)THEN
         nOrbComp = (2*(iang-1))+1   
      ELSE
         !not true if cartesian
         call lsquit('CARTESIAN REQUIRE NOGCBASIS',-1)
      ENDIF
      DO JJ=1,norb
         ccJJ = (JJ-1)*nOrbComp
         DO II=1,norb
            ccII = (II-1)*nOrbComp
            TMPs = TMP(II,JJ)
            do isp = 1,nOrbComp
               cc1 = isp+ccII
               cc2 = isp+ccJJ
               TMPss(cc1,cc2)=TMPs
            ENDDO
         ENDDO !II
      ENDDO !JJ
      !Add to Full 

      call mat_add_block(CC,TMPss,norb*nOrbComp,norb*nOrbComp,nbast1+1,nbast1+1)
      call mem_dealloc(TMP)
      call mem_dealloc(TMPss)
      nbast1 = nbast1 + norb*nOrbComp
   ENDDO
enddo
IF(nbast1.NE.nbast)&
     & call lsquit('dim mismatch in build_GCtransformationmatrix',-1)
end SUBROUTINE build_GCtransformationmatrix

!> \brief Build AO,GCAO transformation matrix CC(AO,GCAO)
!> \author T. Kjaergaard
!> \date 2010
!> \param CCfull the transformation matrix to be generated
!> \param nbast the dimension, the number of basis functions
!> \param setting the setting structure
!> \param lupri the logical unit number for output
SUBROUTINE write_GCtransformationmatrix(nbast,setting,lupri)
implicit none
integer,intent(in)   :: lupri
integer   :: nbast
TYPE(LSSETTING) :: setting
real(realk),pointer :: CCfull(:,:)
logical :: matrix_exsist,DoMPIIO
character(len=30) :: filename 
integer :: GCAOtrans_lun
IF(GCbuild)THEN
   !matrix already built - overwrite
   call mat_free(GCAOtrans)
   call mat_free(GCAOtrans_inv)
   GCbuild = .FALSE.
ENDIF
call mat_init(GCAOtrans,nbast,nbast)
call build_GCtransformationmatrix(GCAOtrans,nbast,setting,lupri)
call WritematGCAOtrans()
call mat_init(GCAOtrans_inv,nbast,nbast)
call mat_inv(GCAOtrans, GCAOtrans_inv) 
GCbuild = .TRUE.
end SUBROUTINE write_GCtransformationmatrix

subroutine WritematGCAOtrans()
logical :: matrix_exsist,DoMPIIO
character(len=30) :: filename 
integer :: GCAOtrans_lun
#ifdef VAR_MPI
DoMPIIO = infpar%ScalapackMPIIO
#else
DoMPIIO = .FALSE.
#endif
IF(matrix_type .EQ. mtype_scalapack.AND.DoMPIIO)THEN
   filename = ' '
   filename = 'GCAOtrans'
   call mat_scalapack_write_single_mat_to_disk(GCAOtrans,filename)
ELSE
   INQUIRE(file='GCAOtrans',EXIST=matrix_exsist) 
   IF(matrix_exsist)then
      GCAOtrans_lun=-1
      call lsopen(GCAOtrans_lun,'GCAOtrans','OLD','UNFORMATTED')
      call lsclose(GCAOtrans_lun,'DELETE')
   ENDIF
   GCAOtrans_lun = -1  !initialization
   call lsopen(GCAOtrans_lun,'GCAOtrans','UNKNOWN','UNFORMATTED')
   rewind GCAOtrans_lun
   call mat_write_to_disk(GCAOtrans_lun,GCAOtrans,.TRUE.)
   call lsclose(GCAOtrans_lun,'KEEP')
ENDIF
end subroutine WritematGCAOtrans

SUBROUTINE read_GCtransformationmatrix(setting,lupri)
implicit none
integer,intent(in)   :: lupri
!integer,intent(in)   :: nbast
!integer   :: nbast2
TYPE(LSSETTING) :: setting
!type(matrix),intent(inout) :: CCmatrix
logical :: matrix_exsist
integer :: GCAOtrans_lun
character(len=30) :: filename 
IF(GCbuild)THEN
   call lsquit('read_GCtransformationmatrix should not be called',-1)
ELSE
   INQUIRE(file='GCAOtrans',EXIST=matrix_exsist) 
   IF(matrix_exsist)then
      Call ReadmatGCAOtrans()
   ELSE
      call lsquit('read_GCtransformationmatrix should not be called',-1)
!      call build_GCtransformationmatrix(CCmatrix,nbast,setting,lupri)      
   ENDIF   
ENDIF
end SUBROUTINE read_GCtransformationmatrix

subroutine ReadmatGCAOtrans()
  implicit none
  logical :: matrix_exsist,gcbasis
  integer :: GCAOtrans_lun
  character(len=30) :: filename 

  IF(matrix_type .EQ. mtype_scalapack)THEN
     filename = ' '
     filename = 'GCAOtrans'
     gcbasis = .FALSE. !do not read logical
     call mat_scalapack_read_single_mat_from_disk(GCAOtrans,filename,gcbasis)
  ELSE
     GCAOtrans_lun = -1  !initialization
     call lsopen(GCAOtrans_lun,'GCAOtrans','OLD','UNFORMATTED')
     rewind GCAOtrans_lun
     call mat_read_from_disk(GCAOtrans_lun,GCAOtrans,.TRUE.)
     call lsclose(GCAOtrans_lun,'KEEP')
  ENDIF
end subroutine ReadmatGCAOtrans

!===============================================================================

! Subroutines that transform back and forth 

!===============================================================================

!> \brief Transform AO Fock matrix to GCAO matrix
!> \author T. Kjaergaard
!> \date 2010
!> \param MAT Matrix to be transformed
!> \param setting the setting structure
!> \param lupri the logical unit number for output
subroutine AO2GCAO_transform_matrixF(F,setting,lupri,FGCAO)
implicit none
integer,intent(in) :: lupri
type(matrix) :: F
TYPE(LSSETTING) :: setting
type(matrix),optional :: FGCAO
!
type(Matrix) :: wrk
integer :: nao,ngcao
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF(nao.EQ.ngcao)THEN
      !no linear dependencies
      call MAT_INIT(wrk,ngcao,nao)
      call mat_mul(GCAOtrans,F,'t','n',1E0_realk,0E0_realk,wrk)
      IF(present(FGCAO))THEN
         call mat_mul(wrk,GCAOtrans,'n','n',1E0_realk,0E0_realk,FGCAO)
      ELSE
         call mat_mul(wrk,GCAOtrans,'n','n',1E0_realk,0E0_realk,F)
      ENDIF
      call MAT_free(wrk)
   ELSE
      !linear dependencies 
      IF(F%nrow.EQ.nao)THEN
         call MAT_INIT(wrk,ngcao,nao)
         IF(present(FGCAO))THEN
            IF(FGCAO%nrow.NE.ngcao)call lsquit('dim mismatch AO2GCAO.',-1)
            !F is in the AO basis and will be changed into GCAO
            call mat_mul(GCAOtrans,F,'t','n',1E0_realk,0E0_realk,wrk)
            call mat_mul(wrk,GCAOtrans,'n','n',1E0_realk,0E0_realk,FGCAO)
         ELSE
            !F is in the AO basis and will be changed into GCAO
            call mat_mul(GCAOtrans,F,'t','n',1E0_realk,0E0_realk,wrk)
            call MAT_free(F)
            call MAT_init(F,ngcao,ngcao)
            call mat_mul(wrk,GCAOtrans,'n','n',1E0_realk,0E0_realk,F)
         ENDIF
         call MAT_free(wrk)
      ELSE
         !F is in the GCAO basis
         call lsquit('dim mismatch AO2GCAO. F in GCAO',-1)
      ENDIF
   ENDIF
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF
end subroutine AO2GCAO_transform_matrixF

!> \brief Half-transform AO matrix to GCAO matrix
!> \author S. Reine
!> \date Dec 6th 2012
!> \param MAT Matrix to be transformed
!> \param setting the setting structure
!> \param lupri the logical unit number for output
!> \param side index indicating if first or second AO should be transformed (1 or 2)
subroutine AO2GCAO_half_transform_matrix(F,setting,lupri,side,FGCAO)
implicit none
integer,intent(in) :: lupri
type(matrix)       :: F     
TYPE(LSSETTING)    :: setting
Integer            :: side
type(matrix),optional :: FGCAO
!
integer :: nrow,ncol,ngcao,nao
type(Matrix) :: wrk,CC
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF (side.EQ.1) THEN
      call MAT_INIT(wrk,ngcao,F%ncol)
      !newF(M,N) = GCAO(N,M)^T*F(N,N)
      IF(F%nrow.NE.GCAOtrans%ncol) call lsquit('dim mismatch1 in AO2GCAO_half_transform_matrix',-1)
      call mat_mul(GCAOtrans,F,'t','n',1E0_realk,0E0_realk,wrk)
   ELSE
      call MAT_INIT(wrk,F%nrow,ngcao)
      !newF(N,M) = F(N,N)*GCAO(N,M) 
      IF(F%ncol.NE.GCAOtrans%nrow) call lsquit('dim mismatch2 in AO2GCAO_half_transform_matrix',-1)
      call mat_mul(F,GCAOtrans,'n','n',1E0_realk,0E0_realk,wrk)
   ENDIF
   IF(nao.EQ.ngcao)THEN
      !no linear dependencies
      IF(present(FGCAO))THEN
         call mat_copy(1E0_realk,wrk,FGCAO)
      ELSE
         call mat_copy(1E0_realk,wrk,F)
      ENDIF
   ELSE
      IF(present(FGCAO))THEN
         call mat_copy(1E0_realk,wrk,FGCAO)
      ELSE
         call lsquit('dim mismatch3 in AO2GCAO_half_transform_matrix',-1)
      ENDIF
   ENDIF
   call MAT_free(wrk)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine AO2GCAO_half_transform_matrix

!> \brief Half-transform AO matrix to GCAO matrix
!> \author S. Reine
!> \date Dec 6th 2012
!> \param MAT Matrix to be transformed
!> \param setting the setting structure
!> \param lupri the logical unit number for output
!> \param side index indicating if first or second AO should be transformed (1 or 2)
subroutine GCAO2AO_half_transform_matrixFull(fullMat,n1,n2,setting,lupri,side,&
     & fullmatAO,n3,n4)
implicit none
integer,intent(in) :: lupri,n1,n2
real(realk)       :: fullMat(n1,n2)
TYPE(LSSETTING)    :: setting
Integer            :: side
integer     :: n3,n4
real(realk) :: fullMatAO(n3,n4)
!
integer :: ngcao,nao
real(realk),pointer :: wrk(:,:),CCfull(:,:)
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   call mem_alloc(CCfull,nao,ngcao)
   call mat_to_full(GCAOtrans,1.0E0_realk,CCfull)

   IF (side.EQ.1) THEN
      call mem_alloc(wrk,nao,n2)
      IF(n1.NE.ngcao)call lsquit('dim mismatch GCAO2AO_half_transform_matrixFull1',-1)
      !FAO(nao,n2) = CCfull(nao,ngcao)*Ffull(ngcao,n2)  nao = ngcao
      call DGEMM('n','n',nao,n2,ngcao,1E0_realk,&
           &CCfull,nao,fullMAT,ngcao,0E0_realk,WRK,ngcao)
   ELSE
      call mem_alloc(wrk,n1,ngcao)
      IF(n2.NE.ngcao)call lsquit('dim mismatch GCAO2AO_half_transform_matrixFull1',-1)
      !FAO(n1,nao) = Ffull(n1,ngcao)*CCfull(nao,ngcao)
      call DGEMM('n','t',n1,nao,ngcao,1E0_realk,&
           &fullMat,n1,CCfull,nao,0E0_realk,WRK,n1)
   ENDIF
   call mem_dealloc(CCfull)
   CALL DCOPY(n3*n4,wrk,1,fullMatAO,1)
   call mem_dealloc(wrk)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine GCAO2AO_half_transform_matrixFull

!> \brief Half-transform GCAO matrix to AO matrix
!> \author S. Reine
!> \date May 22nd 2013
!> \param F Matrix to be transformed
!> \param setting the setting structure
!> \param lupri the logical unit number for output
!> \param side index indicating if first or second AO should be transformed (1 or 2)
subroutine GCAO2AO_half_transform_matrix(F,setting,lupri,side,FAO)
implicit none
integer,intent(in) :: lupri
type(matrix)       :: F
TYPE(LSSETTING)    :: setting
Integer            :: side
type(matrix),optional :: FAO
!
integer :: nrow,ncol,ngcao,nao
type(Matrix) :: wrk,CC

IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF (side.EQ.1) THEN
      call MAT_INIT(wrk,GCAOtrans%nrow,F%ncol)
      call mat_mul(GCAOtrans,F,'n','n',1E0_realk,0E0_realk,wrk)
   ELSE
      call MAT_INIT(wrk,F%nrow,GCAOtrans%nrow)
      call mat_mul(F,GCAOtrans,'n','t',1E0_realk,0E0_realk,wrk)
   ENDIF
   IF(nao.EQ.ngcao)THEN
      !no linear dependencies
      IF(present(FAO))THEN
         call mat_copy(1E0_realk,wrk,FAO)
      ELSE
         call mat_copy(1E0_realk,wrk,F)
      ENDIF
   ELSE
      IF(present(FAO))THEN
         call mat_copy(1E0_realk,wrk,FAO)
      ELSE
         call mat_free(F)
         call mat_init(F,wrk%nrow,wrk%ncol)
         call mat_assign(F,wrk)
!         call lsquit('dim mismatch3 in GCAO2AO_half_transform_matrixFull',-1)
      ENDIF
   ENDIF
   call MAT_free(wrk)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine GCAO2AO_half_transform_matrix

!> \brief Transform GCAO Density matrix to AO Density matrix
!> \author T. Kjaergaard
!> \date 2010
!> \param MAT Matrix to be transformed
!> \param setting the setting structure
!> \param lupri the logical unit number for output
subroutine GCAO2AO_transform_matrixD2(DMATGCAO,DMATAO,setting,lupri)
implicit none
integer,intent(in) :: lupri
type(matrix) :: DMATGCAO,DMATAO
TYPE(LSSETTING) :: setting
!
type(matrix) :: wrk,CC
integer :: ndmat,nbast,ngcao,nao
nbast=DMATAO%nrow
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF(DMATGCAO%nrow.NE.ngcao)call lsquit('dim mismatch1 in GCAO2AO_transform_matrixD2',-1)
   IF(DMATAO%nrow.NE.nao)call lsquit('dim mismatch2 in GCAO2AO_transform_matrixD2',-1)
   call MAT_INIT(wrk,GCAOtrans%nrow,DMATGCAO%ncol)
   call mat_mul(GCAOtrans,DMATGCAO,'n','n',1E0_realk,0E0_realk,wrk)
   call mat_mul(wrk,GCAOtrans,'n','t',1E0_realk,0E0_realk,DMATAO)
   call MAT_free(wrk)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine GCAO2AO_transform_matrixD2

!> \brief Transform AO Density matrix to GCAO Density matrix
!> \author T. Kjaergaard
!> \date 2010
!> \param MAT Matrix to be transformed
!> \param setting the setting structure
!> \param lupri the logical unit number for output
subroutine AO2GCAO_transform_matrixD(DMAT,setting,lupri)
implicit none
integer,intent(in) :: lupri
type(matrix) :: DMAT
TYPE(LSSETTING) :: setting
!
integer :: ndmat,nbast,nao,ngcao
type(matrix) :: wrk
nbast=DMAT%nrow

IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF(DMAT%nrow.NE.nao)call lsquit('dim mismatch2 in GCAO2AO_transform_matrixD',-1)
   call MAT_INIT(wrk,GCAOtrans_inv%nrow,DMAT%ncol)
   call mat_mul(GCAOtrans_inv,DMAT,'n','n',1E0_realk,0E0_realk,wrk)
   IF(nao.NE.ngcao)THEN
      call mat_free(Dmat)
      call mat_init(Dmat,ngcao,ngcao)
   ENDIF
   call mat_mul(wrk,GCAOtrans_inv,'n','t',1E0_realk,0E0_realk,DMAT)
   call MAT_free(wrk)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine AO2GCAO_transform_matrixD

!> \brief Transform 2 dim fortran array from AO to GCAO
!> \author T. Kjaergaard
!> \date 2010
!> \param fullMAT array to be transformed
!> \param nbast number of basis functions
!> \param setting the setting structure
!> \param lupri the logical unit number for output
!> This one works for S,F,H1 and other matrices that transform in the same way
!> For D se next routine
subroutine AO2GCAO_transform_fullF(fullMAT,fullgcao,nbast,M,setting,lupri)
implicit none
integer,intent(in) :: lupri,nbast,M
real(realk) :: fullMAT(nbast,nbast),fullgcao(M,M)
TYPE(LSSETTING) :: setting
!
integer :: ngcao,nao
real(realk),pointer :: CCfull(:,:)
real(realk),pointer :: WRK(:,:)
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF(M.NE.ngcao)call lsquit('dim mismatch1 in AO2GCAO_transform_fullF',-1)
   IF(nbast.NE.nao)call lsquit('dim mismatch2 in AO2GCAO_transform_fullF',-1)
   call mem_alloc(CCfull,nao,ngcao)
   call mat_to_full(GCAOtrans, 1.0E0_realk, CCfull)
   call mem_alloc(wrk,ngcao,nbast)
   call DGEMM('t','n',ngcao,nbast,nbast,1E0_realk,&
        &CCfull,nao,fullMAT,nbast,0E0_realk,WRK,ngcao)
   call DGEMM('n','n',ngcao,ngcao,nbast,1E0_realk,&
        &WRK,ngcao,CCfull,nao,0E0_realk,fullgcao,ngcao)
   call mem_dealloc(wrk)
   call mem_dealloc(CCfull)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine AO2GCAO_transform_fullF

!> \brief Transform 2 dim fortran array from AO to GCAO
!> \author T. Kjaergaard
!> \date 2010
!> \param fullMAT array to be transformed
!> \param nbast number of basis functions
!> \param setting the setting structure
!> \param lupri the logical unit number for output
!> This one works for S,F,H1 and other matrices that transform in the same way
!> For D se next routine
subroutine AO2GCAO_transform_fullF2(fullMAT,nbast,setting,lupri)
implicit none
integer,intent(in) :: lupri,nbast
real(realk) :: fullMAT(nbast,nbast)
TYPE(LSSETTING) :: setting
!
integer :: ngcao,nao
real(realk),pointer :: CCfull(:,:)
real(realk),pointer :: WRK(:,:)
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF(nao.NE.ngcao)call lsquit('dim mismatch1 in AO2GCAO_transform_fullF2',-1)
   call mem_alloc(CCfull,nao,ngcao)
   call mat_to_full(GCAOtrans, 1.0E0_realk, CCfull)
   call mem_alloc(wrk,ngcao,nbast)
   call DGEMM('t','n',ngcao,nbast,nbast,1E0_realk,&
        &CCfull,nao,fullMAT,nbast,0E0_realk,WRK,ngcao)
   call DGEMM('n','n',ngcao,ngcao,nbast,1E0_realk,&
        &WRK,ngcao,CCfull,nao,0E0_realk,fullMat,ngcao)
   call mem_dealloc(wrk)
   call mem_dealloc(CCfull)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF

end subroutine AO2GCAO_transform_fullF2

!> \brief Transform GCAO 3dim fortran array to AO
!> \author T. Kjaergaard
!> \date 2010
!>
!> This one works for D and other matrices that transform in the same way
!> For F,S,H1 se previous routine
!>
!> \param MATGCAO GCAO array to be transformed
!> \param MATAO AO the output array
!> \param nbast number of basis functions
!> \param setting the setting structure
!> \param lupri the logical unit number for output
subroutine GCAO2AO_transform_fullD(MATGCAO,MATAO,nbast,M,ndmat,setting,lupri)
implicit none
integer :: lupri,ndmat,nbast,M
real(realk) :: MATGCAO(M,M,ndmat)
real(realk) :: MATAO(nbast,nbast,ndmat)
TYPE(LSSETTING) :: setting
!
integer :: I,ngcao,nao
integer,pointer :: IPVT(:)
real(realk) :: dummy(2),RCOND
real(realk),pointer :: CCfull(:,:),WRK(:,:),WORK1(:)
type(matrix) :: CC
IF(GCbuild)THEN
   ngcao = GCAOtrans%ncol
   nao = GCAOtrans%nrow
   IF(M.NE.ngcao)call lsquit('dim mismatch1 in GCAO2AO_transform_fullD',-1)
   IF(nbast.NE.nao)call lsquit('dim mismatch2 in GCAO2AO_transform_fullD',-1)

   call mem_alloc(CCfull,nao,ngcao)
   call mat_to_full(GCAOtrans, 1.0E0_realk, CCfull)
   call mem_alloc(WRK,nao,nbast)
   DO I=1,ndmat
      call DGEMM('n','n',nao,nbast,nbast,1E0_realk,&
           &CCfull,nao,matgcao(:,:,I),nbast,0E0_realk,WRK,nao)
      call DGEMM('n','t',nao,nao,nbast,1E0_realk,&
           &WRK,nao,CCfull,nao,0E0_realk,matao(:,:,I),nao)
   ENDDO
   call mem_dealloc(WRK)
   call mem_dealloc(CCfull)
ELSE
   call lsquit('GCtrans not build',-1)
ENDIF
end subroutine GCAO2AO_transform_fullD

!G_new(N,M) = G_old(N,N)*U(N,M)
subroutine RemoveLinearDependenciesOfBasisGCAO(U,U_inv,N,M)
  implicit none
  !> N is the number of AO basis functions
  integer,intent(in) :: N
  !> M is the number of projected AO basis functions
  integer,intent(in) :: M
  type(matrix),intent(in) :: U,U_inv !U(N,M), U_inv(M,N)
  !
  type(matrix) :: GCAOtransU,GCAOtrans_invU
  IF(GCbuild)THEN
     call mat_init(GCAOtransU,N,M)
     call mat_mul(GCAOtrans,U,'n','n',1E0_realk,0E0_realk,GCAOtransU)
     call mat_free(GCAOtrans)
     call mat_init(GCAOtrans,N,M)
     call mat_assign(GCAOtrans,GCAOtransU)
     call mat_free(GCAOtransU)

     call mat_init(GCAOtrans_invU,M,N)
     call mat_mul(U_inv,GCAOtrans_inv,'n','n',1E0_realk,0E0_realk,GCAOtrans_invU)
     call mat_free(GCAOtrans_inv)
     call mat_init(GCAOtrans_inv,M,N)
     call mat_assign(GCAOtrans_inv,GCAOtrans_invU)
  ELSE
     call lsquit('RemoveLinearDependenciesOfBasisGCAO no GCAOtrans',-1)
  ENDIF
  
  call WritematGCAOtrans()  

end subroutine RemoveLinearDependenciesOfBasisGCAO

END MODULE GCTRANSMod



