!> @file 
!> contains many structure and associated subroutine
MODULE purificationtype
  use precision
  
  !*****************************************
  !*
  !* OBJECT CONTAINING INFORMATION ABOUT PURIFICATION ALGORITHM
  !*
  !*****************************************
  TYPE purificationInfo
     integer :: purifyStart
     integer :: purifyAlgo
     real(realk) :: CONVTHR
     logical :: OAO
     logical :: printinfo
!     logical :: preopt
  END TYPE PURIFICATIONINFO

  integer,parameter :: IdemPuInputDensity=1
  integer,parameter :: IdemPuPMmethodStart=2
  integer,parameter :: IdemPuGershgorin=3
  
  integer,parameter :: IdemPuBasicMcWeeny=1
  integer,parameter :: IdemPuTraceConserving=2
  integer,parameter :: IdemPuTraceResetting=3
  integer,parameter :: IdemPuGenMcWeeny=4
  
  private
  public :: PURIFICATIONINFO,set_default_purificationinfo,&
       & IdemPuPMmethodStart,IdemPuInputDensity,&
       & IdemPuGershgorin,IdemPuBasicMcWeeny,&
       & IdemPuTraceConserving,IdemPuTraceResetting,&
       & IdemPuGenMcWeeny

contains
  !> \brief set default values for Purification Info Item
  !> \date 2017
  !> \author T. Kjaergaard
  subroutine set_default_purificationinfo(purifyItem)
    implicit none
    type(purificationInfo) :: purifyItem
    !Logicals to determine Which Start Density should be used.
    purifyItem%purifyStart = IdemPuGershgorin
    purifyItem%purifyAlgo = IdemPuTraceResetting
    purifyItem%CONVTHR = 1.0E-10_realk
    purifyItem%OAO = .TRUE.
    purifyItem%printinfo = .FALSE.
!    purifyItem%preopt= .FALSE.
  end subroutine set_default_purificationinfo

end MODULE PURIFICATIONTYPE

