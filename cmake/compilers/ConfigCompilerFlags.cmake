#This is a workaround for the Fujitsu compiler where CMake does not pick up the correct compier ID for fortran, USE WITH CARE
if(CMAKE_C_COMPILER_ID MATCHES Fujitsu)
   set(CMAKE_Fortran_COMPILER_ID ${CMAKE_C_COMPILER_ID})
endif()

include(SaveCompilerFlags)

if(CMAKE_C_COMPILER_WORKS)
    include(CFlags)
endif()

if(CMAKE_CXX_COMPILER_WORKS)
    include(CXXFlags)
endif()

if(CMAKE_Fortran_COMPILER_WORKS)
    include(FortranFlags)
endif()
