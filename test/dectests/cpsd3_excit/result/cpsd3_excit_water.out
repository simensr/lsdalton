     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | pablo
     Host                     | pablo-AU
     System                   | Linux-4.6.5-040605-generic
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4
     BLAS                     | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_gf_lp64.so;/opt/intel/composer_xe_2015.2.16
                              | 4/mkl/lib/intel64/libmkl_sequential.so;/opt/intel/
                              | composer_xe_2015.2.164/mkl/lib/intel64/libmkl_core
                              | .so;/usr/lib/x86_64-linux-gnu/libpthread.so;/usr/l
                              | ib/x86_64-linux-gnu/libm.so
     LAPACK                   | /opt/intel/composer_xe_2015.2.164/mkl/lib/intel64/
                              | libmkl_lapack95_lp64.a;/opt/intel/composer_xe_2015
                              | .2.164/mkl/lib/intel64/libmkl_gf_lp64.so
     Static linking           | OFF
     Last Git revision        | 519f240046caed3224b4c6cae342fb6149fa623b
     Git branch               | pablo/cpt
     Configuration time       | 2017-04-03 16:02:00.342281
  

         Start simulation
     Date and time (Linux)  : Mon Apr  3 16:02:36 2017
     Host name              : pablo-AU                                
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    6-31G Aux=cc-pVDZ-RI                    
                                            
                                            
    Atomtypes=2 Nosymmetry Angstrom Charge=0                                                                                
    Charge=8 Atoms=1                                                                                                        
    O   -2.904830    1.255230    0.000000                                                                                   
    Charge=1 Atoms=2                                                                                                        
    H   -1.934830    1.255230    0.000000                                                                                   
    H   -3.228160    0.442870   -0.420050                                                                                   
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RH
    .DIIS
    .START
    ATOMS
    .LCM
    .MAXIT
    1000
    **CC
    .CPSD3
    .CANONICAL
    .MEMORY
    1.0
    *CCRESPONSE
    .NEXCIT
    2
    *END OF INPUT
 
 
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
 WARNING This basis format violate the old dalton basis format
 Which consist of 1 Exponent F16.9 and up to 6 contraction
 coefficients on the first line followed by a up to 7 contraction coefficients
 on the following lines until the full number of contraction coefficients are given
 We will try to this basis set, but this code is not very well testet. TK
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 6-31G                     22        9 [10s4p|3s2p]                                 
          2 H      1.000 6-31G                      4        2 [4s|2s]                                      
          3 H      1.000 6-31G                      4        2 [4s|2s]                                      
    ---------------------------------------------------------------------
    total         10                               30       13
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 cc-pVDZ-RI                56       56 [7s5p4d2f|7s5p4d2f]                          
          2 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
          3 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
    ---------------------------------------------------------------------
    total         10                               84       84
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       13
      Auxiliary basisfunctions           :       84
      Primitive Regular basisfunctions   :       30
      Primitive Auxiliary basisfunctions :       84
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Due to the presence of the keyword (default for correlation)
    .NOGCINTEGRALTRANSFORM
    We transform the input basis to the Grand Canonical
    basis and perform integral evaluation using this basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on 6-31G Charge   8
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -71.4946859143    0.00000000000    0.00      0.00000    0.00    0.0000000    4.531E+00 ###
      2    -73.8341599123   -2.33947399805    0.00      0.00000    0.00    0.0000000    2.663E+00 ###
      3    -74.2580432558   -0.42388334348   -1.00      0.00000    0.00    0.0000000    1.607E-01 ###
      4    -74.2598387062   -0.00179545041   -1.00      0.00000    0.00    0.0000000    3.161E-02 ###
      5    -74.2598920070   -0.00005330082   -1.00      0.00000    0.00    0.0000000    2.600E-03 ###
      6    -74.2598924085   -0.00000040150   -1.00      0.00000    0.00    0.0000000    1.479E-06 ###

    Level 1 atomic calculation on 6-31G Charge   1
    ================================================
  
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1     -0.3410549973    0.00000000000    0.00      0.00000    0.00    0.0000000    1.982E-01 ###
      2     -0.3488197504   -0.00776475307    0.00      0.00000    0.00    0.0000000    1.270E-02 ###
      3     -0.3488518065   -0.00003205613   -1.00      0.00000    0.00    0.0000000    3.937E-05 ###
 
    Matrix type: mtype_dense

    First density: Atoms in molecule guess

    Iteration 0 energy:      -75.944138361131
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1    -75.9265805613    0.00000000000    0.00      0.00000    0.00    0.0000000    1.718E+00 ###
      2    -75.9685132390   -0.04193267771    0.00      0.00000    0.00    0.0000000    1.001E+00 ###
      3    -75.9843275074   -0.01581426836   -1.00      0.00000    0.00    0.0000000    6.887E-02 ###
      4    -75.9845163177   -0.00018881031   -1.00      0.00000    0.00    0.0000000    1.176E-02 ###
      5    -75.9845230822   -0.00000676444   -1.00      0.00000    0.00    0.0000000    1.694E-03 ###
      6    -75.9845231992   -0.00000011701   -1.00      0.00000    0.00    0.0000000    1.099E-04 ###
      7    -75.9845231996   -0.00000000046   -1.00      0.00000    0.00    0.0000000    1.980E-05 ###
    SCF converged in      7 iterations
    >>>  CPU Time used in SCF iterations is   0.40 seconds
    >>> wall Time used in SCF iterations is   0.40 seconds

    Total no. of matmuls in SCF optimization:         59

    Number of occupied orbitals:       5
    Number of virtual orbitals:        8

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     5 iterations!

    Calculation of virtual orbital energies converged in     6 iterations!

     E(LUMO):                         0.201605 au
    -E(HOMO):                        -0.498125 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.699730 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.04193267771    0.0000    0.0000    0.0000000
      3   -0.01581426836   -1.0000    0.0000    0.0000000
      4   -0.00018881031   -1.0000    0.0000    0.0000000
      5   -0.00000676444   -1.0000    0.0000    0.0000000
      6   -0.00000011701   -1.0000    0.0000    0.0000000
      7   -0.00000000046   -1.0000    0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1           -75.92658056132371768854      0.171788818626525D+01
        2           -75.96851323903459274334      0.100068010643628D+01
        3           -75.98432750739429764053      0.688657971245424D-01
        4           -75.98451631770838332613      0.117552000494058D-01
        5           -75.98452308215087214194      0.169385283069236D-02
        6           -75.98452319916466990435      0.109915107778845D-03
        7           -75.98452319962056833447      0.198017815317274D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                       -75.984523199621
          Nuclear repulsion:                       9.062741299348
          Electronic energy:                     -85.047264498969

      %LOC%
      %LOC% Localized orbitals written to lcm_orbitals.u
      %LOC%

    Number of electrons  in molecule =       10
    Number of occ. orb.  in molecule =        5
    Number of virt. orb. in molecule =        8




    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :     10
    FULL: Number of atoms               :      3
    FULL: Number of basis func.         :     13
    FULL: Number of aux. basis func.    :     84
    FULL: Number of core orbitals       :      1
    FULL: Number of valence orbitals    :      4
    FULL: Number of occ. orbitals       :      5
    FULL: Number of occ. alpha orbitals :     -1
    FULL: Number of occ. beta  orbitals :     -1
    FULL: Number of virt. orbitals      :      8
    FULL: Local memory use type full    :  0.32E-05
    FULL: Distribute matrices           : F
    FULL: Using frozen-core approx.     : F

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
    Memory set in input to be:    1.000     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 


--------------------------
  Coupled-cluster energy  
--------------------------

Wave function    = CPSD3   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting CC iterations
 ### ----------------------
 ###  Iteration     Residual norm          CC energy
 ###      1         0.548144430E-02         0.00000000    
 ###      2         0.129603417E-02         0.00000000    
 ###      3         0.207318021E-03         0.00000000    
 ###      4         0.608208326E-04         0.00000000    
 ###      5         0.797346346E-05         0.00000000    
 ###      6         0.539373934E-06         0.00000000    
 ###      7         0.458206399E-07         0.00000000    
 ###      8         0.614759376E-08         0.00000000    
 ###      9         0.317465669E-09         0.00000000    
 CCSOL: MAIN LOOP      : 0.387    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Hooray! CC equation is solved!
Singles amplitudes norm  =  0.1880411E-01
Total amplitudes norm    =  0.1880411E-01
Corr. energy             =      0.0000000000
CCSOL: Total cpu time    =  0.388     s
CCSOL: Total wall time   =  0.389     s
Number of CC iterations  =    9
 CCSOLVER - tot time (min.)            0.00648

 JAC: Starting guess from orbital energies


 JAC ********************************************************
 JAC        Information for Jacobian eigenvalue solver       
 JAC        ------------------------------------------       
 JAC 
 JAC Number of eigenvalues                 2
 JAC Initial subspace dimension            2
 JAC Maximum subspace dimension           40
 JAC 
 JAC Start guess for eigenvalues
 JAC ---------------------------
 JAC       1    0.6997303087    
 JAC       2    0.7508053349    
 JAC 
 JAC ********************************************************
 JAC
 JAC Jacobian eigenvalue solver
 JAC
 JAC Which   Subspace     Eigenvalue           Residual       Conv?  
 JAC     1       2        0.35404606         0.11547526        F
 JAC     2       3        0.44183831         0.15245171        F
 JAC     1       4        0.33992779         0.11452932E-01    F
 JAC     2       5        0.42309994         0.45492748E-01    F
 JAC     1       6        0.33981122         0.16106039E-02    F
 JAC     2       7        0.42120408         0.74135979E-02    F
 JAC     1       8        0.33981063         0.36723022E-03    F
 JAC     2       9        0.42112790         0.22039612E-02    F
 JAC     1      10        0.33981062         0.70960011E-05    T
 JAC     2      10        0.42112597         0.85451679E-03    F
 JAC     1      11        0.33981062         0.70960011E-05    T
 JAC     2      11        0.42112590         0.91234277E-04    T


      ************************************************************
      *               CCS      excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.3398106       9.246722       74579.81    
              2      0.4211259       11.45942       92426.45    

 CPSD2: total wall time :       0.00 min.

--------------------------
  Coupled-cluster energy  
--------------------------

Wave function    = CPSD2   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting CC iterations
 ### ----------------------
 ###  Iteration     Residual norm          CC energy
 ###      1         0.245393079E-01
 ###      2         0.118543366E-01
 ###      3         0.129383513E-02
 ###      4         0.950366335E-04
 ###      5         0.452000936E-05
 ###      6         0.397717598E-06
 ###      7         0.205941814E-06
 ###      8         0.145626128E-06
 ###      9         0.276203435E-08
 ###     10         0.460246542E-09
 CCSOL: MAIN LOOP      :  2.03    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
Singles multiplier norm  =  0.5745857E-01
Total multiplier norm    =  0.5745857E-01
CCSOL: Total cpu time    =  0.332     s
CCSOL: Total wall time   =   2.04     s
Number of CC iterations  =   10
 CCSOLVER - tot time (min.)            0.03393

 CPSD2: total wall time :       0.00 min.

--------------------------
  Coupled-cluster energy  
--------------------------

Wave function    = CPSD2   
MaxIter          =  100
Num. b.f.        =   13
Num. occ. orb.   =    5
Num. unocc. orb. =    8
Convergence      =  0.1E-08
Debug mode       =    F
Print level      =    2
Use CROP         =    T
CROP subspace    =    3
Preconditioner   =    T
Precond. B       =    T
Singles          =    T

 ### Starting CC iterations
 ### ----------------------
 ###  Iteration     Residual norm          CC energy
 ###      1         0.244953004E-01
 ###      2         0.126276844E-01
 ###      3         0.290242254E-02
 ###      4         0.889474335E-03
 ###      5         0.278648312E-03
 ###      6         0.594085067E-04
 ###      7         0.654776018E-05
 ###      8         0.109941814E-05
 ###      9         0.183200272E-06
 ###     10         0.674415876E-07
 ###     11         0.524611653E-07
 ###     12         0.309144910E-07
 ###     13         0.461574202E-08
 ###     14         0.251170293E-08
 ###     15         0.155729085E-08
 ###     16         0.427324397E-09
 CCSOL: MAIN LOOP      : 0.679    s


-------------------------------
  Coupled-cluster job summary  
-------------------------------

Yeeehaw! left-transformations converged!
Singles multiplier norm  =  0.6537599E-01
Total multiplier norm    =  0.6537599E-01
CCSOL: Total cpu time    =  0.680     s
CCSOL: Total wall time   =  0.680     s
Number of CC iterations  =   16
 CCSOLVER - tot time (min.)            0.01133
 

 CPSD3: initialization   :       0.00 min.
 CPSD3: integrals + fock :       0.00 min.
 
 2nd-order doubles: loop info:
    Max. Vir batch dimension   =            8
    Tot. number of Vir batches =            1
    Max. Occ batch dimension   =            5
    Tot. number of Occ batches =            1
    Tot. Memory required       =       0.0003 GB
    Job type                   =            2
 CPSD3: Bvv int to disk :       0.00 min.
 CPSD3: main loop 1     :       0.00 min.
 CPSD3: bar integrals   :       0.00 min.
 CPSD3: main loop 2     :       0.00 min.
 CPSD3: total wall time :       0.00 min.

 CPSD3: Analysis for state no.    1
 CPSD3: ----------------------------
 CPSD3:    Zero-th order energy, CIS/CCS         =       9.2467224 eV
 CPSD3:    Second order correction, CIS(D)/CPSD2 =      -1.0336789 eV
 CPSD3:    Third order correction, CPSD3         =       0.0657291 eV
 CPSD3:    Total third order excitation energy   =       8.2787726 eV
 CPSD3:
 CPSD3: Diagnostics, (the smaller the better):
 CPSD3:    < R_1^(2)   | R_1^(2)   >  =       0.0033015
 CPSD3:    < R_2^(1)   | R_2^(1)   >  =       0.0311866
 CPSD3:    < R_2^(1,2) | R_2^(1,2) >  =       0.0341602

 CPSD3: initialization   :       0.00 min.
 CPSD3: integrals + fock :       0.00 min.
 
 2nd-order doubles: loop info:
    Max. Vir batch dimension   =            8
    Tot. number of Vir batches =            1
    Max. Occ batch dimension   =            5
    Tot. number of Occ batches =            1
    Tot. Memory required       =       0.0003 GB
    Job type                   =            2
 CPSD3: Bvv int to disk :       0.00 min.
 CPSD3: main loop 1     :       0.00 min.
 CPSD3: bar integrals   :       0.00 min.
 CPSD3: main loop 2     :       0.00 min.
 CPSD3: total wall time :       0.00 min.

 CPSD3: Analysis for state no.    2
 CPSD3: ----------------------------
 CPSD3:    Zero-th order energy, CIS/CCS         =      11.4594249 eV
 CPSD3:    Second order correction, CIS(D)/CPSD2 =      -0.9897088 eV
 CPSD3:    Third order correction, CPSD3         =       0.1345266 eV
 CPSD3:    Total third order excitation energy   =      10.6042427 eV
 CPSD3:
 CPSD3: Diagnostics, (the smaller the better):
 CPSD3:    < R_1^(2)   | R_1^(2)   >  =       0.0042740
 CPSD3:    < R_2^(1)   | R_2^(1)   >  =       0.0284559
 CPSD3:    < R_2^(1,2) | R_2^(1,2) >  =       0.0302577


      ************************************************************
      *               CPSD3    excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.3042391       8.278773       66772.77    
              2      0.3896985       10.60424       85528.94    






    ******************************************************************************
    *                      Full CPSD3    calculation is done !                   *
    ******************************************************************************





    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :      -75.9845231996
     E: Correlation energy                             :        0.0000000000



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :         2.80400     s
    Total Wall time used in CC           :         4.50600     s
    ------------------------------------------------------


    Hostname       : pablo-AU                                          
    Job finished   : Date: 03/04/2017   Time: 16:02:41



    =============================================================================
                              -- end of CC program --
    =============================================================================



    Total no. of matmuls used:                        96
    Total no. of Fock/KS matrix evaluations:           8
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          8.799 MB
      Max allocated memory, type(matrix)                 128.432 kB
      Max allocated memory, real(realk)                    8.695 MB
      Max allocated memory, integer                      103.508 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.640 kB
      Max allocated memory, AOBATCH                       77.824 kB
      Max allocated memory, BATCHTOORB                     0.112 kB
      Max allocated memory, ARRAY                          4.944 kB
      Max allocated memory, ODBATCH                        3.520 kB
      Max allocated memory, LSAOTENSOR                     5.472 kB
      Max allocated memory, SLSAOTENSOR                    6.624 kB
      Max allocated memory, ATOMTYPEITEM                 229.584 kB
      Max allocated memory, ATOMITEM                       2.112 kB
      Max allocated memory, LSMATRIX                      11.536 kB
      Max allocated memory, OverlapT                      97.696 kB
      Max allocated memory, linkshell                      0.432 kB
      Max allocated memory, integrand                    716.800 kB
      Max allocated memory, integralitem                   1.280 MB
      Max allocated memory, IntWork                      161.608 kB
      Max allocated memory, Overlap                        6.459 MB
      Max allocated memory, ODitem                         2.560 kB
      Max allocated memory, LStensor                      66.556 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   3.44 seconds
    >>> wall Time used in LSDALTON is   5.15 seconds

    End simulation
     Date and time (Linux)  : Mon Apr  3 16:02:41 2017
     Host name              : pablo-AU                                
