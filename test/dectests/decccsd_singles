#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > decccsd_singles.info <<'%EOF%'
   DEC-CCSD energy using fragment calculations and long-range t1
   -------------------------------------------------------------
   Molecule:         3 BH molecules with stretched bonds
   Wave Function:    CCSD / STO-3G
   Test Purpose:     Test DEC fragmentation for CCSD using 
                     long-range t1 correction.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > decccsd_singles.mol <<'%EOF%'
BASIS                                                   
STO-3G
                                                        
                                                        
Atomtypes=2 Nosymmetry Angstrom
Charge=5.0 Atoms=3
B          0.000000      0.0000000    0.0000000
B          3.000000      0.0000000    1.4500000
B          6.000000      0.0000000    0.0000000
Charge=1.0 Atoms=3
H          0.000000      1.4500000    0.0000000
H          3.000000      0.0000000    0.0000000
H          6.000000      0.0000000    1.4500000
%EOF%
#######################################################################
#  DALTON INPUT
#######################################################################
cat > decccsd_singles.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
TRILEVEL
.CONVTHR
1.0d-5
**DEC
.CCSD
.FOT
1.0e-4
.SinglesPolari
.SinglesThr
0.1
.MEMORY
2.0
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >decccsd_singles.check
cat >>decccsd_singles.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# HF ENERGY 
CRIT1=`$GREP "Final HF energy: * \-74\.1900454" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=2
ERROR[1]="HF ENERGY NOT CORRECT -"

# CCSD ENERGY
CRIT1=`$GREP "Correlation energy  : * \-0\.199" $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="CCSD correlation energy is not correct"

# Memory test for total memory                                             
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`         
TEST[3]=`expr  $CRIT1`                                                    
CTRL[3]=1                                                                 
ERROR[3]="Memory leak -"
                                                                          
# Memory test for array4                                                  
CRIT1=`$GREP "Memory in use for array4 * \: * 0.000 * GB" $log | wc -l`   
TEST[4]=`expr  $CRIT1`                                                    
CTRL[4]=1                                                                 
ERROR[4]="Memory leak for array4 -" 

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%




