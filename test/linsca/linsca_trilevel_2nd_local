#!/bin/sh 
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_trilevel_2nd_local.info <<'%EOF%'
   linsca_trilevel_2nd_local
   -------------------------
   Molecule:         H2O
   Wave Function:    DFT GGAKey slater=1 becke=1 vwn5=1 p86c=1
   Test Purpose:     Check trilevel code with second order optimization in local region
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_trilevel_2nd_local.mol <<'%EOF%'
BASIS
6-31G Aux=Ahlrichs-Coulomb-Fit
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         A
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_trilevel_2nd_local.dal <<'%EOF%'
**INTEGRAL
.DENSFIT
**WAVE FUNCTIONS
.HF
*DENSOPT
.DIIS
.START
 TRILEVEL
.CONVDYN
 STANDARD
.2ND_LOC
**INFO
.INFO_LINEQ
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_trilevel_2nd_local.check
cat >> linsca_trilevel_2nd_local.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


# ENERGY test
CRIT1=`$GREP "Final HF energy: * -75.831126" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="LEVEL 2 ENERGY NOT CORRECT -"

CRIT1=`$GREP "Final HF energy: * -75.985303" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="LEVEL 3 ENERGY NOT CORRECT -"

CRIT1=`$GREP "HOMO-LUMO Gap \(iteratively\): * 0.759724" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="HOMO-LUMO GAP NOT CORRECT -"

# Test individual iterations, level 2
CRIT1=`$GREP "1    -75.7953369" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="1st L2 energy not correct -"

# Test individual iterations, level 3
CRIT1=`$GREP "1    -75.8311261" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=1
ERROR[5]="1st L3 energy not correct -"

# Test second order convergence
CRIT1=`$GREP "0.0000000    1.589E-04" $log | wc -l`
CRIT2=`$GREP "0.0000000    8.072E-08" $log | wc -l`
TEST[6]=`expr $CRIT1 + $CRIT2`
CTRL[6]=2
ERROR[6]="Not second order convergence -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[7]=`expr  $CRIT1`
CTRL[7]=1
ERROR[7]="Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6 7
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
