#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_uhf_stability.info <<'%EOF%'
   linsca_uhf_stability
   --------------------
   Molecule:         HF
   Wave Function:    HF
   Test Purpose:     Check unrestricted Hessian eigenvalue
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_uhf_stability.mol <<'%EOF%'
ATOMBASIS
HF
===
Atomtypes=2  Generators=0 Bohr
Charge=1 Atoms=1 Basis=6-31G
H    0.000000000000  0.000000000000 -4.306871942321
Charge=9 Atoms=1 Basis=6-31G
F    0.000000000000  0.000000000000  0.228470430269
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_uhf_stability.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.CONVDYN
TIGHT
.STABILITY
.UNREST
**INFO
.INFO_STABILITY
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
#
# Warning it turns out that this testcase can converge to 2 different solutions
# both seem to be valid. 
# 1. Energy -99.68318182787002967871  Eigenvalue  0.354702
# 2. Energy -99.68318199157896231100  Eigenvalue  0.354499
#
# Someone who knows more about the crop solver than me could maybe have
# a look. 
# Best Regards
# Thomas Kjærgaard
#
echo $CHECK_SHELL >linsca_uhf_stability.check
cat >> linsca_uhf_stability.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT1=`$GREP "Hessian eigenvalue no.  1:     0.354[4-7]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HESSIAN EIGENVALUE NOT CORRECT -"

CRIT1=`$GREP "Final HF energy: * -99.683181[8-9]" $log | wc -l`
TEST[2]=`expr	$CRIT1`
CTRL[2]=1
ERROR[2]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
