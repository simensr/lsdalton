#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_arh_ls_davidson.info <<'%EOF%'
   linsca_arh_ls_davidson
   ---------------
   Molecule:         He14
   Wave Function:    BLYP/6-31G
   Test Purpose:     Check scf opt using davidson solver and linesearch
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_arh_ls_davidson.mol <<'%EOF%'
BASIS
6-31G


AtomTypes=1 Angstrom Nosymmetry
Charge=2.0 Atoms=42
He        0.000000     0.00000     0.00000
He        1.500000     0.00000     0.00000
He        3.000000     0.00000     0.00000
He        4.500000     0.00000     0.00000
He        6.000000     0.00000     0.00000
He        7.500000     0.00000     0.00000
He        9.000000     0.00000     0.00000
He        0.000000     1.50000    10.00000
He        1.500000     1.50000    10.00000
He        3.000000     1.50000    10.00000
He        4.500000     1.50000    10.00000
He        6.000000     1.50000    10.00000
He        7.500000     1.50000    10.00000
He        9.000000     1.50000    10.00000
He        0.000000     3.00000     0.00000
He        1.500000     3.00000     0.00000
He        3.000000     3.00000     0.00000
He        4.500000     3.00000     0.00000
He        6.000000     3.00000     0.00000
He        7.500000     3.00000     0.00000
He        9.000000     3.00000     0.00000
He        0.000000     4.50000    10.00000
He        1.500000     4.50000    10.00000
He        3.000000     4.50000    10.00000
He        4.500000     4.50000    10.00000
He        6.000000     4.50000    10.00000
He        7.500000     4.50000    10.00000
He        9.000000     4.50000    10.00000
He        0.000000     6.00000     0.00000
He        1.500000     6.00000     0.00000
He        3.000000     6.00000     0.00000
He        4.500000     6.00000     0.00000
He        6.000000     6.00000     0.00000
He        7.500000     6.00000     0.00000
He        9.000000     6.00000     0.00000
He        0.000000     7.50000    10.00000
He        1.500000     7.50000    10.00000
He        3.000000     7.50000    10.00000
He        4.500000     7.50000    10.00000
He        6.000000     7.50000    10.00000
He        7.500000     7.50000    10.00000
He        9.000000     7.50000    10.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_arh_ls_davidson.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
BLYP
*DENSOPT
.ARH(LS) DAVID
.ARH DEBUG
.START
H1DIAG
.CONVDYN
STANDARD
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_arh_ls_davidson.check
cat >> linsca_arh_ls_davidson.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# test energy
CRIT1=`$GREP "Final DFT energy: * -121.025773" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"


# Test number of iterations
CRIT1=`$GREP "SCF converged in * 8" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="number of iterations wrong -"


# Test individual iterations
CRIT1=`$GREP " 2 * \-97\.69[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="2nd energy shift not correct -"

# Test step length 
CRIT1=`$GREP "Alpha Value * 3\.000" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=2
ERROR[4]="Alpha Value 3.00 incorrect - "

# Test step length 
CRIT1=`$GREP "Alpha Value * 7\.750" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=1
ERROR[5]="Alpha Value incorrect -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[6]=`expr  $CRIT1`
CTRL[6]=1
ERROR[6]="Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
