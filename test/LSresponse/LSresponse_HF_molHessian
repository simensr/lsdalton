#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_HF_molHessian.info <<'%EOF%'
   LSresponse_HF_molHessian
   --------------------
   Molecule:         H2CO modified to remove symmetries
   Wave Function:    HF / 6-31G*
   Test Purpose:     Test molecular geometrical Hessian in LSDALTON (Patrick Merlot)
                    
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_HF_molHessian.mol <<'%EOF%'
BASIS
6-31G*
H2CO non-symmetric

Atomtypes=3 Angstrom
Charge=6.0 Atoms=1
C           .000000     .000000    -.536614
Charge=8.0 Atoms=1
O           .200000     .100000     .683501
Charge=1.0 Atoms=2
H           .000000     .934390   -1.124164
H           .000000    -1.00000   -1.324164
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_HF_molHessian.dal <<'%EOF%'
**GENERAL
.TIME
**INTEGRALS
**WAVE FUNCTIONS
.HF
*DENSOPT
**GEOHESSIAN
.TEST
.INTPRINT
99
*END OF INPUT
%EOF%

#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_HF_molHessian.check
cat >> LSresponse_HF_molHessian.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="grep -i"
else
   GREP="grep -i"
fi

# Energy
CRIT1=`$GREP "Final HF energy: * -113.83693" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory (TOTAL): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MOLHESSIAN: Cumul. norm of the first derivative of the reference density matrix
CRIT1=`$GREP "Cumul. norm of Da:.*0.504445" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Error in the first derivative of the reference density matrix in the geometrical Hessian"

# MOLHESSIAN: Cumul. norm of the first derivative of the Coulomb term
CRIT1=`$GREP "Cumul. norm of Ja:.*606.417079" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Error in the first derivative of the Coulomb term"

# MOLHESSIAN: Cumul. norm of the first derivative of the exchange term
CRIT1=`$GREP "Cumul. norm of Ka:.*44.416565" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=1
ERROR[5]="Error in the first derivative of the exchange term"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM IN GEOMETRICAL HESSIAN CONTRIBUTIONS
  exit 1
fi

%EOF%
#######################################################################
