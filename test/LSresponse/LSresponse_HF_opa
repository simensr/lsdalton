#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_HF_opa.info <<'%EOF%'
   LSresponse_HF_opa
   -----------------
   Molecule:         Hydrogen fluoride
   Wave Function:    HF / 6-31G*
   Test Purpose:     Test excitation energy and transition moment (Kasper K)
                    
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_HF_opa.mol <<'%EOF%'
BASIS
6-31G*
-----------test----------

Atomtypes=2 Generators=0
Charge=1.0 Atoms=1 Basis=6-31G*
H 0.000000000 0.0000000000   0.000000000000000
Charge=9.0 Atoms=1 Basis=6-31G*
F 1.96000000  0.0000000000   0.000000000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_HF_opa.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH DAVID                   
.CONVDYN
TIGHT
**RESPONS
.NEXCIT
5
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_HF_opa.check
cat >> LSresponse_HF_opa.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-99\.98634" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Excitation energy and transition moment
CRIT1=`$GREP "0\.55349[0-9][0-9][0-9] * \-?1\.06696[0-9][0-9]" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Error in excitation energy /transition moment"


PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
