#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_mcd3.info <<'%EOF%'
   LSresponse_mcd3 
   -------------
   Molecule:         HF
   Wave Function:    DFT (B3LYP) / Huckel
   Test Purpose:     Test MCD: Aterm, Bterm and Damped MCD. 
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_mcd3.mol <<'%EOF%'
BASIS
Huckel
H3+ molecule with
a Huckel basis
Atomtypes=1  Charge=1  Nosymmetry Angstrom
Charge=1.0  Atoms=3
H  0.000000     0.346410     0.000000
H  0.300000    -0.173205     0.000000
H -0.300000    -0.173205     0.000000
%EOF%

#######################################################################
#  DALTON INPUT
#  WARNING DO NOT USE THIS INPUT WITHOUT MODIFICATION
#######################################################################
cat > LSresponse_mcd3.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.THRESH
1.d-14
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.RH
.DIIS
.NVEC
8
.START
H1DIAG
.CONVTHR
1.0D-8
**RESPONSE
*QUASIMCD
.DEGENERATE
#.MCDEXCIT
#2
#.GAUSSIAN
#.LINESHAPEPARAM
#0.002D0
#.NVECFORPEAK
#3
#.NSTEPS
#1000
#CLOSE TO A PEAK (unlike mcd2 testcase)
.DAMPEDXCOOR
1
2.3497425912d0
*END QUASIMCD
*SOLVER
.NOPREC
#.TWOSTART
.CONVTHR
1.0D-8
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSresponse_mcd3.check
cat >>LSresponse_mcd3.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY                                         
CRIT1=`$GREP "This is a DFT calculation of type: B3LYP" $log | wc -l`  
CRIT2=`$GREP "Final DFT energy\: *\-1\.001861447" $log | wc -l`       
TEST[1]=`expr   $CRIT1 + $CRIT2`                        
CTRL[1]=2                                                           
TEST[1]=`expr   $CRIT2`                        
CTRL[1]=1                                                           
ERROR[1]="DFT ENERGY NOT CORRECT -"  

# DAMPED 
CRIT2=`$GREP "2\.34974259[0-9][0-9]E\+00 * \-9\.678[3-4][0-9][0-9][0-9][0-9][0-9][0-9]E\+03 * \-3\.1084[0-9][0-9][0-9][0-9][0-9][0-9]E\+03" $log | wc -l` 
CRIT9=`$GREP "5\.15708592[0-9][0-9]E\+05 * \-1\.91063[0-9][0-9][0-9][0-9][0-9]E\+01 * \-6\.1364[0-9][0-9][0-9][0-9][0-9][0-9]E\+00" $log | wc -l` 
TEST[2]=`expr   $CRIT2 + $CRIT9`     
CTRL[2]=2
ERROR[2]="Damped MCD spectra close to peak not correct -"     

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
