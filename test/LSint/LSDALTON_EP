#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_EP.info <<'%EOF%'
   LSDALTON_EP
   -------------
   Molecule:         water/6-31G
   Wave Function:    LDA
   Test Purpose:     Check electrostatic potential integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_EP.mol <<'%EOF%'
ATOMBASIS
This file have been generated using the WRITE_MOLECULE_OUTPUT subroutine
Output is in Bohr
Atomtypes=2    Charge=0   Nosymmetry
Charge=1.00   Atoms=8     Basis=6-31G
H          8.67500000      0.00000000    -16.64000000
H          9.57993600      0.00000000    -17.92045500
H          7.20321300      0.03775700    -17.67333600
H          8.10814900      0.03775700    -18.95379100
H          9.24133000      0.57150100     -2.49360100
H         10.14626600      0.57150100     -3.77405500
H          7.13448500      0.65989700     -2.98249600
H          8.03942100      0.65989700     -4.26295100
Charge=8.00   Atoms=4     Basis=6-31G
O          8.67500000      0.00000000    -17.60000000
O          7.20321300      0.03775700    -18.63333600
O          9.24133000      0.57150100     -3.45360100
O          7.13448500      0.65989700     -3.94249600
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_EP.dal <<'%EOF%'
**INTEGRAL
.DEBUGEP
**WAVE FUNCTIONS
.DFT
 LDA
*DENSOPT
.RH
.DIIS
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_EP.check
cat >> LSDALTON_EP.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "EP integrals calculated successfully" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
