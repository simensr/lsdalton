#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_magderiv3.info <<'%EOF%'
   LSDALTON_magderiv3
   -------------
   Molecule:         water/(cc-pVDZ,cc-pVTZ)
   Wave Function:    DFT/B3LYP
   Test Purpose:     Check magnetic derivative 2e int integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_magderiv3.mol <<'%EOF%'
ATOMBASIS
Water
Output is in Bohr
Atomtypes=2    Charge=0   Nosymmetry
Charge=8.00   Atoms=1     Basis=cc-pVDZ
O     0.0000000000000000   -0.2249058930  0.00000000
Charge=1.00   Atoms=2     Basis=cc-pVTZ
H     1.45235              0.899623      0.00000000 
H    -1.45235              0.899623      0.00000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_magderiv3.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRAL
.DEBUGMAGDERIV
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.RH
.DIIS
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_magderiv3.check
cat >> LSDALTON_magderiv3.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
# the others should be zero
CRIT1=`$GREP "QQQ FxFx magderiv * \-667.55492" $log | wc -l`
CRIT2=`$GREP "QQQ FyFy magderiv * \-1583.7428" $log | wc -l`
CRIT3=`$GREP "QQQ FzFz magderiv * \-3067.1427" $log | wc -l`
TEST[1]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[1]=3
ERROR[1]="MAGDERIV 2INT TEST1 NOT CORRECT -"

CRIT1=`$GREP "QQQ KxKx magderiv * \-0?.17888" $log | wc -l`
CRIT2=`$GREP "QQQ KyKy magderiv * \-0?.32538" $log | wc -l`
CRIT3=`$GREP "QQQ KzKz magderiv * \-0?.63854" $log | wc -l`
TEST[2]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[2]=3
ERROR[2]="MAGDERIV 2INT TEST2 NOT CORRECT -"

CRIT1=`$GREP "QQQ JxJx magderiv * \-680.10255" $log | wc -l`
CRIT2=`$GREP "QQQ JyJy magderiv * \-1612.0820" $log | wc -l`
CRIT3=`$GREP "QQQ JzJz magderiv * \-3106.1679" $log | wc -l`
TEST[3]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[3]=3
ERROR[3]="MAGDERIV 2INT TEST1 NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
