#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_ethane_df_lda_jengine.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Ethane
   Wave Function:    DFT (LDA) / STO-3G 
   Test Purpose:     Test energy as obtained from FCK3 in Hermite basis
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_ethane_df_lda_jengine.mol <<'%EOF%'
BASIS
6-31G  Aux=df-def2
LDA molecular hessian without symmetry
Ethane 
    2   0
        6.    2
C      1.4496747688     -0.0000010117     0.0000000000
C     -1.4496747688      0.0000010117     0.0000000000
        1.    6
H      2.1888645654     -0.9728762646     1.6850699397
H      2.1888645654     -0.9728762646    -1.6850699397
H     -2.1888645654      0.9728762646     1.6850699397
H     -2.1888645654      0.9728762646    -1.6850699397
H      2.1888617992      1.9457507963     0.0000000000
H     -2.1888617992     -1.9457507963     0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_ethane_df_lda_jengine.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**INTEGRALS
.DENSFIT
**WAVE FUNCTIONS
.DFT
 LDA
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.START
H1DIAG
.RH
.DIIS
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_ethane_df_lda_jengine.check
cat >>LSDALTON_ethane_df_lda_jengine.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-79\.01723272" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
