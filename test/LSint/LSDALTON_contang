#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_contang.info <<'%EOF%'
   LSDALTON_contang
   -------------
   Molecule:         H2S
   Wave Function:    HF/cc-pVTZ
   Test Purpose:     Test .CONTANG keyword: contracted, angular integral ordering
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_contang.mol <<'%EOF%'
ATOMBASIS
H2S molecule
For testing the .CONTANG keyword: contracted, angular integral ordering
Atomtypes=2
Charge=16.0 Atoms=1 Basis=cc-pVTZ
S     0.00000000    0.07624135   0.00000000
Charge=1.0 Atoms=2 Basis=cc-pVTZ
H    -1.53208889   -1.20933387   0.00000000
H     1.53208889   -1.20933387   0.00000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_contang.dal <<'%EOF%'
**INTEGRALS
.CONTANG
**WAVE FUNCTIONS
.HF
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_contang.check
cat >> LSDALTON_contang.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-398\.5827271[1-2]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
