#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_ethane_dft_molgrad.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Ethane
   Wave Function:    DFT/ B3LYP / 6-31G
   Test Purpose:     Test energy and molecular gradient contributions
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_ethane_dft_molgrad.mol <<'%EOF%'
BASIS
6-31G
LDA molecular without symmetry
Ethane 
    2   0
        6.    2
C1      1.4496747688     -0.0000010117     0.0000000000
C2     -1.4496747688      0.0000010117     0.0000000000
        1.    6
H1      2.1888645654     -0.9728762646     1.6850699397
H2      2.1888645654     -0.9728762646    -1.6850699397
H3     -2.1888645654      0.9728762646     1.6850699397
H4     -2.1888645654      0.9728762646    -1.6850699397
H5      2.1888617992      1.9457507963     0.0000000000
H6     -2.1888617992     -1.9457507963     0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_ethane_dft_molgrad.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
**INFO
.DEBUG_MPI_MEM
**RESPONS
*MOLGRA
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSDALTON_ethane_dft_molgrad.check
cat >> LSDALTON_ethane_dft_molgrad.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-79\.747380" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Gradient tests
CRIT1=`$GREP "C[1] * \-0?\.00173[0-9][0-9][0-9][0-9][0-9] * \-0?\.0000063... * \-?0?\.0000000000" $log | wc -l`
CRIT2=`$GREP "C[2] * 0?\.00173[0-9][0-9][0-9][0-9][0-9] * 0?\.0000063... * \-?0?\.0000000000" $log | wc -l`
CRIT3=`$GREP "H[1] * \-0?\.00023[0-9][0-9][0-9][0-9][0-9] * \-0?\.00171[0-9][0-9][0-9][0-9][0-9] * ?0\.00297[0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
CRIT4=`$GREP "H[2] * \-0?\.00023[0-9][0-9][0-9][0-9][0-9] * \-0?\.00171[0-9][0-9][0-9][0-9][0-9] * \-0?\.00297[0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
CRIT5=`$GREP "H[3] * 0?\.00023[0-9][0-9][0-9][0-9][0-9] * 0?\.00171[0-9][0-9][0-9][0-9][0-9] * ?0\.00297[0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
CRIT6=`$GREP "H[4] * 0?\.00023[0-9][0-9][0-9][0-9][0-9] * 0?\.00171[0-9][0-9][0-9][0-9][0-9] * \-0?\.00297[0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
CRIT7=`$GREP "H[5] * \-0?\.00023[0-9][0-9][0-9][0-9][0-9] * 0?\.00343[0-9][0-9][0-9][0-9][0-9] * \-?0\.0000000000" $log | wc -l`
CRIT8=`$GREP "H[6] * 0?\.00023[0-9][0-9][0-9][0-9][0-9] * \-0?\.00343[0-9][0-9][0-9][0-9][0-9] * \-?0?\.0000000000" $log | wc -l`

TEST[3]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ $CRIT7 \+ $CRIT8 `
CTRL[3]=8
ERROR[3]="Error in molecular gradient"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
