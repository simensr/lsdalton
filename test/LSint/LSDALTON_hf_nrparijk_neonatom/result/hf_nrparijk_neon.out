     THIS IS A DEBUG BUILD

     ******************************************************************
     **********  LSDalton - An electronic structure program  **********
     ******************************************************************


    This is output from LSDalton 1.0


     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:

     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)



    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)


     NOTE:

     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.

     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.

     See the home page "http://daltonprogram.org"
     for further information.


     Who compiled             | wirz
     Host                     | debianProV256
     System                   | Linux-3.16.0-4-amd64
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Debian 4.9.2-10) 4.9.2
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Debian 4.9.2-10) 4.9.2
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Debian 4.9.2-10) 4.9.2
     BLAS                     | /usr/lib/atlas-base/libf77blas.so;/usr/lib/atlas-b
                              | ase/libcblas.so;/usr/lib/atlas-base/libatlas.so
     LAPACK                   | /usr/lib/atlas-base/libatlas.so;/usr/lib/atlas-bas
                              | e/atlas/liblapack.so
     Static linking           | OFF
     Last Git revision        | 43330936b8cebdff3ba597b5fc4782fc2fad07c6
     Git branch               | lukas/non-robust-PARI
     Configuration time       | 2016-11-18 14:04:17.888545


         Start simulation
     Date and time (Linux)  : Fri Nov 18 15:16:57 2016
     Host name              : debianProV256

    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE
    ---------------------------------------------------

    BASIS
    cc-pVDZ


    Atomtypes=1 Nosymmetry Angstrom
    Charge=10 Atoms=1
    O  0 0 0

    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE
    ---------------------------------------------------

    **GENERAL
    .TIME
    **INTEGRALS
    .NR2-PARI-J
    .PARI-UNCONSTRAINED
    .THR_PCHOL_NRPARI1
    1.d-9
    .THR_PCHOL_NRPARI2
    1.d-7
    .NRPARI_AUGM_SCHEME
    6
    .NRPARI_EXP
    0.7
    .NRPARI_SPHERICAL
    .false.
    .NRPARI_CONTR
    .true.
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RH
    .DIIS
    *END OF INPUT



    Atoms and basis sets
      Total number of atoms        :      1
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O     10.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]
    ---------------------------------------------------------------------
    total         10                               26       14
    ---------------------------------------------------------------------


    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       14
      Primitive Regular basisfunctions   :       26
    --------------------------------------------------------------------

    >>>  CPU Time used in product basis is   0.03 seconds
    >>> wall Time used in product basis is   0.03 seconds
    >>>  CPU Time used in truncate is   0.00 seconds
    >>> wall Time used in truncate is   0.00 seconds
    >>>  CPU Time used in indices is   0.00 seconds
    >>> wall Time used in indices is   0.00 seconds
    >>>  CPU Time used in pivots is   0.01 seconds
    >>> wall Time used in pivots is   0.01 seconds
    >>>  CPU Time used in basis is   0.00 seconds
    >>> wall Time used in basis is   0.00 seconds
    >>>  CPU Time used in NR-PARI basis is   0.04 seconds
    >>> wall Time used in NR-PARI basis is   0.04 seconds
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    Density subspace min. method    : DIIS
    Density optimization            : Diagonalization

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
    We have detected a Dunnings Basis set so we deactivate the
    use of the Grand Canonical basis, which is normally default.
    The use of Grand Canonical basis can be enforced using the FORCEGCBASIS keyword
    We perform the calculation in the standard input basis

    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis

    End of configuration!

    >>>  CPU Time used in *INPUT is   0.04 seconds
    >>> wall Time used in *INPUT is   0.05 seconds
    >>>  CPU Time used in CS15_INPUT-Mole is   0.00 seconds
    >>> wall Time used in CS15_INPUT-Mole is   0.00 seconds
    >>>  CPU Time used in II_precalc_ScreenMat is   0.02 seconds
    >>> wall Time used in II_precalc_ScreenMat is   0.02 seconds
    >>>  CPU Time used in OVERLAP is   0.00 seconds
    >>> wall Time used in OVERLAP is   0.00 seconds
    >>>  CPU Time used in *S is   0.00 seconds
    >>> wall Time used in *S is   0.00 seconds
    >>>  CPU Time used in NucElec is   0.00 seconds
    >>> wall Time used in NucElec is   0.00 seconds
    >>>  CPU Time used in Kinetic is   0.00 seconds
    >>> wall Time used in Kinetic is   0.00 seconds
    >>>  CPU Time used in *H1 is   0.00 seconds
    >>> wall Time used in *H1 is   0.00 seconds

    First density: Atoms in molecule guess

    >>>  CPU Time used in coeff-block: 3-centre-integrals is   0.03 seconds
    >>> wall Time used in coeff-block: 3-centre-integrals is   0.03 seconds
    >>>  CPU Time used in coeff-block: 2-centre-integrals is   0.02 seconds
    >>> wall Time used in coeff-block: 2-centre-integrals is   0.02 seconds
 minEigV=    1.1771580067730332E-008
 maxEigV=    1187.1761072723768
condition nb.=  0.10085E+12
 minEigV=    1.1771631677211662E-008
 maxEigV=    1187.1761072723766
condition nb.=  0.10085E+12
    >>>  CPU Time used in coeff-block: solve LSE is   0.00 seconds
    >>> wall Time used in coeff-block: solve LSE is   0.00 seconds
    >>>  CPU Time used in coeff-block is   0.05 seconds
    >>> wall Time used in coeff-block is   0.05 seconds
    >>>  CPU Time used in coeffs is   0.05 seconds
    >>> wall Time used in coeffs is   0.05 seconds
    >>>  CPU Time used in c-beta is   0.00 seconds
    >>> wall Time used in c-beta is   0.00 seconds
    >>>  CPU Time used in J-1 is   0.05 seconds
    >>> wall Time used in J-1 is   0.05 seconds
    >>>  CPU Time used in Jfull is   0.00 seconds
    >>> wall Time used in Jfull is   0.00 seconds
    >>>  CPU Time used in PARI-J is   0.11 seconds
    >>> wall Time used in PARI-J is   0.11 seconds
    >>>  CPU Time used in reg-Jengine is   0.11 seconds
    >>> wall Time used in reg-Jengine is   0.11 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.01 seconds
    >>> wall Time used in LINK-Kbuild is   0.01 seconds
    Iteration 0 energy:     -128.488775551710

    >>>  CPU Time used in *START is   0.12 seconds
    >>> wall Time used in *START is   0.12 seconds
    Preparing to do S^1/2 decomposition...
    >>>  CPU Time used in LWDIAG is   0.00 seconds
    >>> wall Time used in LWDIAG is   0.00 seconds

    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    >>>  CPU Time used in INIT SCF is   0.00 seconds
    >>> wall Time used in INIT SCF is   0.00 seconds
    >>>  CPU Time used in coeffs is   0.00 seconds
    >>> wall Time used in coeffs is   0.00 seconds
    >>>  CPU Time used in c-beta is   0.00 seconds
    >>> wall Time used in c-beta is   0.00 seconds
    >>>  CPU Time used in J-1 is   0.06 seconds
    >>> wall Time used in J-1 is   0.05 seconds
    >>>  CPU Time used in Jfull is   0.00 seconds
    >>> wall Time used in Jfull is   0.00 seconds
    >>>  CPU Time used in PARI-J is   0.06 seconds
    >>> wall Time used in PARI-J is   0.05 seconds
    >>>  CPU Time used in reg-Jengine is   0.06 seconds
    >>> wall Time used in reg-Jengine is   0.05 seconds
    >>>  CPU Time used in LINK-Kbuild is   0.01 seconds
    >>> wall Time used in LINK-Kbuild is   0.01 seconds
    >>>  CPU Time used in FCK_FO is   0.06 seconds
    >>> wall Time used in FCK_FO is   0.06 seconds
    >>>  CPU Time used in G_GRAD is   0.00 seconds
    >>> wall Time used in G_GRAD is   0.00 seconds
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1   -128.4887755517    0.00000000000    0.00      0.00000    0.00    0.0000000    3.957E-06 ###
    SCF converged in      1 iterations
    >>>  CPU Time used in SCF iterations is   0.06 seconds
    >>> wall Time used in SCF iterations is   0.06 seconds

    Total no. of matmuls in SCF optimization:          8

    Number of occupied orbitals:       5
    Number of virtual orbitals:        9

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     2 iterations!

    Calculation of virtual orbital energies converged in     1 iterations!

     E(LUMO):                         5.196711 au
    -E(HOMO):                        -1.918799 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     7.115510 au

    >>>  CPU Time used in HL GAP is   0.12 seconds
    >>> wall Time used in HL GAP is   0.12 seconds

    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1          -128.48877555173993414428      0.395732568343761D-05

          SCF converged !!!!
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                      -128.488775551740
          Nuclear repulsion:                       0.000000000000
          Electronic energy:                    -128.488775551740

    Total no. of matmuls used:                        16
    Total no. of Fock/KS matrix evaluations:           2
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present

      Max allocated memory, TOTAL                         16.246 MB
      Max allocated memory, type(matrix)                  92.880 kB
      Max allocated memory, real(realk)                   16.162 MB
      Max allocated memory, integer                       83.881 kB
      Max allocated memory, logical                        1.092 kB
      Max allocated memory, character                      2.496 kB
      Max allocated memory, AOBATCH                       21.360 kB
      Max allocated memory, ODBATCH                        1.320 kB
      Max allocated memory, LSAOTENSOR                     0.608 kB
      Max allocated memory, SLSAOTENSOR                    1.288 kB
      Max allocated memory, ATOMTYPEITEM                   6.841 MB
      Max allocated memory, ATOMITEM                       0.864 kB
      Max allocated memory, LSMATRIX                       3.024 kB
      Max allocated memory, OverlapT                      28.224 kB
      Max allocated memory, linkshell                      0.204 kB
      Max allocated memory, integrand                    580.608 kB
      Max allocated memory, integralitem                   1.037 MB
      Max allocated memory, IntWork                      106.280 kB
      Max allocated memory, Overlap                       14.272 MB
      Max allocated memory, ODitem                         0.960 kB
      Max allocated memory, LStensor                     308.693 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   0.37 seconds
    >>> wall Time used in LSDALTON is   0.37 seconds

    End simulation
     Date and time (Linux)  : Fri Nov 18 15:16:57 2016
     Host name              : debianProV256
