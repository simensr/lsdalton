#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_FMM_csr.info <<'%EOF%'
   LSDALTON_FMM_csr
   -------------
   Molecule:         5 HCN molecules placed 20 atomic units apart
   Wave Function:    B3LYP/STO-2G
   Test Purpose:     Check FMM within new framework
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_FMM_csr.mol <<'%EOF%'
BASIS
3-21G
Two nearby H2O
-----------------------------
Atomtypes=2 Angstrom Nosymmetry
Charge=1. Atoms=4
H                  9.241330    0.571501   -2.493601
H                 10.146266    0.571501   -3.774055
H                  7.134485    0.659897   -2.982496
H                  8.039421    0.659897   -4.262951
Charge=8. Atoms=2
O                  9.241330    0.571501   -3.453601
O                  7.134485    0.659897   -3.942496
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_FMM_csr.dal <<'%EOF%'
**GENERAL
.CSR
.ZERO
1.d-10
**INTEGRALS
.RUNMM
*FMM
.PRINT
 5
.SCREEN
 1e-10
.LMAX
 10
.TLMAX
 24
**WAVE FUNCTION
.DFT
 B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.ARH
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_FMM_csr.check
cat >> LSDALTON_FMM_csr.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-151\.857832[1-2]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
