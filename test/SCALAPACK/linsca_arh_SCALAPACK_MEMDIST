#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_arh_SCALAPACK_MEMDIST.info <<'%EOF%'
   linsca_arh_SCALAPACK_MEMDIST
   ---------------
   Molecule:         Methane with -OH, -F, and Cl
   Wave Function:    B3LYP/3-21G
   Test Purpose:     Memory distributed SCALAPACK functionality
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_arh_SCALAPACK_MEMDIST.mol <<'%EOF%'
ATOMBASIS
Methane with -OH, -F, and Cl
b3lyp/6-31+g(d,p) optimized geo.
Atomtypes=5 Generators=0 Angstrom
        1.    2      Bas=3-21G
H     0.520821    0.080551    1.451675
H     1.965224   -1.097271    0.053015
        6.    1      Bas=3-21G
C     0.430423    0.005605    0.364687
        8.    1      Bas=3-21G
O     1.023374   -1.101579   -0.177807
        9.    1      Bas=3-21G
F     0.989971    1.157250   -0.160167
       17.    1      Bas=3-21G
Cl   -1.303842   -0.036443   -0.048756
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_arh_SCALAPACK_MEMDIST.dal <<'%EOF%'
**GENERAL
.SCALAPACK
.SCALAPACKBLOCKSIZE
22
**INTEGRALS
.MEMDIST
**WAVE FUNCTIONS
.DFT
B3LYP
*DFT INPUT
.GRID TYPE
BLOCK LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.ARH FULL
.NVEC
 8
.CONVDYN
 STANDARD
**INFO
.INFO_LINEQ
.INFO_STABILITY
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_arh_SCALAPACK_MEMDIST.check
cat >> linsca_arh_SCALAPACK_MEMDIST.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# test energy
CRIT1=`$GREP "Final DFT energy: * -671.003017" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# test HOMO-LUMO gap
CRIT1=`$GREP "HOMO-LUMO Gap \(iteratively\):     \0\.3385" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="HOMO-LUMO GAP NOT CORRECT -"

# Test individual iterations
CRIT1=`$GREP "1   -670.934753" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="1st energy not correct -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Memory leak -"

# SCALAPACK test
CRIT1=`$GREP "SCALAPACK for memory distribution and parallelization" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=1
ERROR[5]="Not using SCALAPACK -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[6]=`expr  $CRIT1`
CTRL[6]=0
ERROR[6]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
