#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_atoms_SCALAPACK_MEMDIST.info <<'%EOF%'
   linsca_atoms_SCALAPACK_MEMDIST
   -------------
   Molecule:         H2O
   Wave Function:    HF/6-31G,Ahlrichs-Coulomb-Fit
   Test Purpose:     Check ATOMS starting guess and SCALAPACK functionality
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_atoms_SCALAPACK_MEMDIST.mol <<'%EOF%'
BASIS
6-31G Aux=Ahlrichs-Coulomb-Fit
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         A
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_atoms_SCALAPACK_MEMDIST.dal <<'%EOF%'
**GENERAL
.SCALAPACK
.SCALAPACKBLOCKSIZE
10
**INTEGRAL
.MEMDIST
.DENSFIT
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
 ATOMS
.CONVTHR
 1.0d-6
**INFO
.INFO_LINEQ
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_atoms_SCALAPACK_MEMDIST.check
cat >> linsca_atoms_SCALAPACK_MEMDIST.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final HF energy: * -75.985303" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"


CRIT1=`$GREP "HOMO-LUMO Gap \(iteratively\): * 0.75972[3-4]" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="HOMO-LUMO GAP NOT CORRECT -"

# Test individual iterations
CRIT1=`$GREP "1 * -75.92740771[0-5]" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=2
ERROR[3]="1st energy not correct -"

# Test individual iterations
CRIT1=`$GREP "2 * -75.96925697[0-5]" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=2
ERROR[4]="2nd energy not correct -"

# Test individual iterations
CRIT1=`$GREP "7 * -75.98530302[0-9]" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=2
ERROR[5]="Last energy not correct -"

# Test Newton convergence
CRIT1=`$GREP "Newton equations converged in  4 iterations!" $log | wc -l`
TEST[6]=`expr  $CRIT1`
CTRL[6]=3
ERROR[6]="Convergence of micro iterations is broken -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[7]=`expr  $CRIT1`
CTRL[7]=1
ERROR[7]="Memory leak -"

# SCALAPACK test
CRIT1=`$GREP "SCALAPACK for memory distribution and parallelization" $log | wc -l` 
TEST[8]=`expr  $CRIT1`                                                             
CTRL[8]=1                                                                          
ERROR[8]="Not using SCALAPACK -"        

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[9]=`expr  $CRIT1`
CTRL[9]=0
ERROR[9]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5 6 7 8 9
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
