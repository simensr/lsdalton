#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_DFT_beta_SCALAPACK.info <<'%EOF%'
   LSresponse_DFT_beta_SCALAPACK
   -------------------
   Molecule:         H2O2
   Wave Function:    DFT-B3LYP / STO-3G
   Test Purpose:     Test 1st hyperpolarizability and SCALAPACK func.
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_DFT_beta_SCALAPACK.mol <<'%EOF%'
BASIS
STO-3G
Hydrogen Peroxide

AtomTypes=2 Generators=0
Charge=8.0 Atoms=2
O    -0.00000000  1.40784586 -0.09885600
O     0.00000000 -1.40784586 -0.09885600
Charge=1.0 Atoms=2
H     0.69081489  1.72614891  1.56891868
H    -0.69081489 -1.72614891  1.56891868
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_DFT_beta_SCALAPACK.dal <<'%EOF%'
**GENERAL
.SCALAPACK
.SCALAPACKBLOCKSIZE
14
**WAVE FUNCTIONS
.DFT
B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.ARH                              
.NVEC
8
.CONVTHR
1.0D-6
**INFO
.DEBUG_MPI_MEM
**RESPONS
*SOLVER
.CONVTHR
1.0D-6
*BETA
.BFREQ
1
0.05
.CFREQ
1
0.05
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_DFT_beta_SCALAPACK.check
cat >> LSresponse_DFT_beta_SCALAPACK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-149\.3649[0-1]" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# 1 hyperpolarizability tests
CRIT1=`$GREP "XXZ * \-2.238" $log | wc -l`
CRIT2=`$GREP "ZYY * \-4.384" $log | wc -l`
CRIT3=`$GREP "ZZZ * \-9.891" $log | wc -l`

TEST[3]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3 `
CTRL[3]=3
ERROR[3]="Error in 1st hyperpolarizability"

# SCALAPACK test     
CRIT1=`$GREP "SCALAPACK for memory distribution and parallelization" $log | wc -l`   
TEST[4]=`expr  $CRIT1`        
CTRL[4]=1  
ERROR[4]="Not using SCALAPACK -" 

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=0
ERROR[5]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
