bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
export LM_LICENSE_FILE=/opt/pgi/license.dat
source /opt/pgi/linux86-64/15.7/pgi.sh
export LD_LIBRARY_PATH=/opt/pgi/15.7/share_objects/lib64:$LD_LIBRARY_PATH
export OMP_NUM_THREADS=6
export CUDA_HOME=/usr/local/cuda-7.5
export LD_LIBRARY_PATH=${CUDA_HOME}/lib64
export PATH=${CUDA_HOME}/bin:${PATH}
export DALTON_NUM_MPI_PROCS=1
export LSDALTON_DEVELOPER=1
export DALTON_TMPDIR=$(pwd)/tmp
#
wrk=$1
lib=lsdalton_$bname
git submodule update --force --init --recursive
#
if [ ! -d $bname ]
then
   ./setup --fc=pgf90 --cc=pgcc --cxx=pgcpp --omp --gpu --cublas --type=debug --check --extra-fc-flags="-ta=tesla:cc35 -Mcuda=7.0 -lcuda -lcublas -L/usr/lib -lblas -llapack" --blas=none --lapack=none -DENABLE_EFS=OFF -DENABLE_VPOTDAMP=OFF -DENABLE_XCFUN=OFF -DENABLE_RSP=OFF -DENABLE_DEC=ON -DENABLE_TENSORS=ON -DBUILDNAME="$bname" $bname
fi
if [ ! -d $DALTON_TMPDIR ]
then
   mkdir $DALTON_TMPDIR
fi
#
cd $bname
#
ctest -D Nightly --track GitLabCI -L ContinuousIntegration
exit $?
