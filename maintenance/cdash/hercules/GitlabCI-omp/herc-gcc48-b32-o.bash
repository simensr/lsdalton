bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
echo "running GitlabCI-omp/herc-gcc48-b32-o.bash"
export OMP_NUM_THREADS=6
export DALTON_TMPDIR=$(pwd)/tmp
export LSDALTON_DEVELOPER=1
#
wrk=$1
lib=lsdalton_$bname
git submodule update --force --init --recursive
#
if [ ! -d $bname ]
then
   ./setup --fc=gfortran --cc=gcc --cxx=g++ --omp --type=debug --check -DENABLE_DEC=ON -DENABLE_TENSORS=ON -DENABLE_RSP=OFF -DENABLE_XCFUN=OFF -DENABLE_PCMSOLVER=OFF -DBUILDNAME="$bname" $bname
fi
if [ ! -d $DALTON_TMPDIR ]
then
   mkdir $DALTON_TMPDIR
fi
#
cd $bname
#
ctest -D Nightly --track GitLabCI -L ContinuousIntegration
exit $?
